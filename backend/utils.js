module.exports = {
    decode: function(decoded_data) {
        var timetable = []; // atkoduoti laikai
        var weekdays = []; // atkoduotos savaites dienos
        var valid_from = []; // isigaliojimo datos
        var valid_to   = []; // pasibaigimo datos

        var w; // laiku lenteles plotis, gausim atkoduodami reisu pradzias
        var h; // laiku lenteles aukstis;

        var times = decoded_data.split(",");
        var i, prev_t;
        var i_max = times.length;

        var zero_ground=[], plus = "+", minus = "-";

        for (i = -1, w = 0, h=0, prev_t = 0; ++i < i_max;) { // atkoduojam reisu pradzios laikus
            var t = times[i];

            if (t == "") { //pasibaige reisu pradzios
                break;
            }

            var tag = t.charAt(0);
            if (tag == plus || (tag == minus && t.charAt(1)=="0")) {
                zero_ground[i] = "1";
            }

            prev_t += +(t);
            timetable[w++] = prev_t;
        }

        for (var j = zero_ground.length; --j >= 0;) {
            if (!zero_ground[j]) {
                zero_ground[j] = "0";
            }
        }

        // atkoduojame isigaliojimo datas
        for (var j = 0; ++i < i_max;) {
            var day = +(times[i]); // skaitom isigaliojimo data
            var k = times[++i]; // kiek kartu pakartoti

            if (k == "") { // pabaiga
                k = w - j; // irasysim reiksme i likusius elementus
                i_max = 0; // kad iseitu is atkodavimo ciklo
            }
            else {
                k = +(k);
            }

            while (k-- > 0) {
                valid_from[j++] = day;
            }
        }

        // atkoduojame pasibaigimo datas
        --i;

        for (var j = 0, i_max = times.length; ++i < i_max;) {
            var day = +(times[i]); // pasibaigimo data
            var k = times[++i]; // kiek kartu pakartoti

            if (k == "") { // pabaiga
                k = w - j; // irasysim reiksme i likusius elementus
                i_max = 0; // kad iseitu is atkodavimo ciklo
            }
            else {
                k = +(k);
            }

            while (k-- > 0) {
                valid_to[j++] = day;
            }
        }

        // atkoduojame savaites dienas
        --i;

        for (var j = 0, i_max = times.length; ++i < i_max;) {
            var weekday = times[i]; // skaitom savaites dienas
            var k = times[++i]; // kiek kartu pakartoti

            if (k == "") { // pabaiga
                k = w - j; // irasysim savaites dienas i likusius elementus
                i_max = 0; // kad iseitu is savaites dienu atkodavimo cikla
            }
            else {
                k = +(k);
            }

            while (k-- > 0) {
                weekdays[j++] = weekday;
            }
        }

        // atkoduojame vaziavimo laikus iki sekancios stoteles kiekviename reise
        --i;
        h = 1;

        for (var j = w, w_left = w, dt = 5, i_max = times.length; ++i < i_max;) {
            dt += +(times[i]) -5; // vaziavimo laikas nuo ankstesnes stoteles
            var k = times[++i]; // kiek reisu tinka tas pats vaziavimo laikas

            if (k != "") { // ne visiems likusiems reisams
                k = +(k);
                w_left -= k; // kiek liko neuzpildytu lenteles elementu eiluteje
            }
            else {
                k = w_left; // vaziavimo laikas tinka visiems likusiems reisams
                w_left = 0;
            }

            while (k-- > 0) {
                timetable[j] = dt + timetable[j-w]; // jei lentele vienmatis masyvas
                ++j;
            }

            if (w_left <= 0) {
                w_left = w; // vel nustatome, kad neuzpildytas visas lenteles plotis sekancioje eiluteje
                dt = 5; // pradinis laiko postumis +5 sekanciai eilutei
                ++h;
            }
        }

        return {workdays:weekdays, times:timetable, tag: zero_ground.join(""), valid_from: valid_from, valid_to: valid_to};
    },

    parseRouteData: function(arg, direction_id, r) {
        var direction_schedule = {};
        if(arg.direction_schedule_list) { var direction_schedule_list = []; }; // [] <- nes buna kad stotele marsrute kartojasi ir {} jau netinka
        var data = r._times_data;
        var w = data.workdays.length;
        var h = data.times.length / w;

        var route_data = r;
        var thisday = Math.floor((new Date).getTime() / (1000 * 60 * 60 * 24));//	var thisday = toDay() + 14;

        if(arg.valid_from == 1 || (arg.valid_from && data.valid_from.indexOf(arg.valid_from) == -1)) {
            arg.valid_from == 1;
        }

        for (var i=0; i<h; i++) {
            var stop_id = route_data.stops[i];

            if (!(stop_id in direction_schedule)) { direction_schedule[stop_id] = []; }
            if(arg.direction_schedule_list) { var direction_schedule_list_item = []; }

            var index = 0;
            for(var j=i*w; j<i*w+w; j++) {

                if(data.valid_to && data.valid_to[index] !=0 && data.valid_to[index]<thisday) { // pasibaiges reisas index
                    index += 1;
                    continue;
                }

                if(arg.valid_from) {
                    if(arg.valid_from == 2) {
                        if (data.valid_from[index] > thisday + 14) {
                            index += 1;
                            continue;
                        }
                    }
                    else if(arg.valid_from != 1 && arg.valid_from == route_data.valid_from0 && data.valid_from[index]<thisday) {

                    }
                    else if((arg.valid_from != 1 && data.valid_from[index] != arg.valid_from) ||
                        (arg.valid_from == 1 && data.valid_from[index] > thisday)) {
                        index += 1;
                        //console.log("filter");
                        continue;
                    }
                }

                var time = this.formatTime(data.times[j]);

                var cfg = {i:index, time:time, workdays:data.workdays[index]};

                if(arg.valid_from == 2) {
                    cfg.valid_from = data.valid_from[index];
                    cfg.valid_to = data.valid_to[index];

                    if(cfg.valid_from && cfg.valid_from < thisday) cfg.valid_from = 0;
                    if(cfg.valid_to && cfg.valid_to > (thisday + 14)) cfg.valid_to = 0;
                }

                if(data.tag.charAt(index) == "1") { cfg.ground = true; }

                if(i == (h-1)) { // paskutine stotele
                    cfg.end = true;
                }
                else if(data.times[(h-1)*w+index]==-1 && data.times[(i+1)*w+index]==-1) {
                    cfg.end = true;
                }

                index += 1;

                direction_schedule[stop_id].push(cfg);

                if(arg.direction_schedule_list) { direction_schedule_list_item.push(cfg); }
            }
            direction_schedule_list.push(direction_schedule_list_item);
        }


        if(arg.valid_from) {
            var new_workdays = [];
            for(var i=0; i<data.valid_from.length;i++) {
                if(arg.valid_from == 2) {

                } else if((arg.valid_from != 1 && data.valid_from[i] == arg.valid_from) ||
                    (arg.valid_from != 1 && arg.valid_from == route_data.valid_from0 && data.valid_from[i] < thisday) ||
                    (arg.valid_from == 1 && data.valid_from[i] <= thisday)) {
                    new_workdays.push(data.workdays[i]);
                }
            }
            arg.workdays[direction_id] = new_workdays;
        }
        else {
            arg.workdays[direction_id] = data.workdays;
        }

        arg.direction_schedule[direction_id] = direction_schedule;

        /*1*/if(arg.direction_schedule_list) { arg.direction_schedule_list[direction_id] = direction_schedule_list; }

        if ("stop_id" in arg) {

            var stop_schedule = direction_schedule[arg.stop_id];

            for(var i=0; i<stop_schedule.length; i++) {
                var wd = stop_schedule[i].workdays;
                var hour = stop_schedule[i].time.h;
                var minute = stop_schedule[i].time.m;

                var cfg = {direction_id: direction_id, stop_id:arg.stop_id, race_index:stop_schedule[i].race_index, minute: minute, days:wd};
                if(arg.valid_from == 2) {
                    cfg.valid_from = stop_schedule[i].valid_from;
                    cfg.valid_to =  stop_schedule[i].valid_to;
                }

                if("ground" in stop_schedule[i]) {
                    cfg.ground = true;
                }

                if("end" in stop_schedule[i]) {
                    cfg.end = true;
                }

                for (var days in arg.stop_schedule_byhour) {
                    if (wd.indexOf(days)!=-1) {
                        if (!(hour in arg.stop_schedule_byhour[days])) { arg.stop_schedule_byhour[days][hour]=[]; }
                        arg.stop_schedule_byhour[days][hour].push(cfg);
                    }
                }

                if (!(wd in arg.stop_schedule_byhour_all)) { arg.stop_schedule_byhour_all[wd]={}; }
                if (!(hour in arg.stop_schedule_byhour_all[wd])) { arg.stop_schedule_byhour_all[wd][hour]=[]; }
                arg.stop_schedule_byhour_all[wd][hour].push(cfg);
            }
        }

        return direction_schedule;
    },

    formatTime: function (time) {
        var minutes = time % 60;
        var hours = Math.floor(time/60);
        return {h: hours, m: minutes};
    }
};