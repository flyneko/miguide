var http = require('http');
var utils = require('./utils');
var fs = require('fs');

function Application() {
    var self = this;
    this.APPLICATION_DIR = __dirname + '/';
    this.responseData = {
        stops: {},
        routes: {},
        times: {}
    }
    this.doneProgress = 3;

    this.refreshAPIFiles(function () { self.executeStops(); })

    setInterval(function () {
        self.responseData = {
            stops: {},
            routes: {},
            times: {}
        }
       self.refreshAPIFiles(function () { self.executeStops(); });
    }, 12 * 60 * 60 * 1000);
}
Application.prototype.updateDoneProgress = function () {
    this.doneProgress--;
    if (this.doneProgress == 0) {
        console.log('Server started successfully');
    }
}

Application.prototype.refreshAPIFiles = function (onsuccess) {
    var successCount = 3;
    var self = this;
    function parse(name) {
        http.get('http://www.minsktrans.by/city/minsk/' + name + '.txt', function(response) {
            var body = '';
            response.on('data', function(chunk) {
                body += chunk;
            });
            response.on('end', function() {
                successCount--;
                if (successCount == 0 && onsuccess) onsuccess();
                fs.writeFile(self.APPLICATION_DIR + 'api/' + name + '.txt', body, {encoding: 'utf-8'});
                console.log(name.charAt(0).toUpperCase() + name.slice(1) + ' is successfully updated')
            })
        });
    }

    parse('stops');
    parse('routes');
    parse('times');
}

Application.prototype.routeSearch = function(query, onsuccess) {
    http.get('http://p.testsite.webtm.ru/diplom/minsktrans/?route_search&from[lat]=' + query.from.lat + '&from[lng]=' + query.from.lng + '&to[lat]=' + query.to.lat + '&to[lng]=' + query.to.lng + '', function(response) {
        var body = '';
        response.on('data', function(chunk) { body += chunk; });
        response.on('end', function() {
            onsuccess(JSON.parse(body));
        });
    });
}

Application.prototype.trackRouteById = function (route_id, onsuccess) {
    if (!this.responseData.routes[route_id]) {
        onsuccess([]);
        return false;
    }
    var route = this.responseData.routes[route_id];
    var endStop = this.responseData.stops[route.stops[route.stops.length - 1]];
    http.get('http://p.testsite.webtm.ru/diplom/minsktrans/?track&v=' + (266373 ^ route.route_nmb) +'&tt=' + route.transport + '&r=' + route.route_nmb, function(response) {
        var body = '';
        response.on('data', function(chunk) { body += chunk; });
        response.on('end', function() {
            var vehicles = (JSON.parse(body)).Vehicles;
            var out = {};
            for (var i in vehicles) {
                var vehicle = vehicles[i];
                if ( (new RegExp(vehicle.EndStop.replace(/[^\d\wа-яА-Я]/g, ''), 'gi')).test(endStop.name.replace(/[^\d\wа-яА-Я]/g, '')) ) {
                    out[vehicle.Id] = {lat: vehicle.Latitude, lng: vehicle.Longitude}
                }
            }
            onsuccess(out);
        });
    });
}


Application.prototype.getData = function (response, data) {
    var out = [];
    for (var i in this.responseData[data])
        out.push(this.responseData[data][i]);
    this.generateJSONResponse(response, out);
}

Application.prototype.generateJSONResponse = function(r, res) {
    r.header('Access-Control-Allow-Origin', '*');
    r.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    r.header('Access-Control-Allow-Headers', 'Content-Type');

    r.header('Content-Type', "application/json; charset=utf-8");
    r.end(JSON.stringify(res));
}

Application.prototype.getRouteById = function (route_id) {
    var route = this.responseData.routes[route_id];
    var self = this;
    return {
        id: route.id,
        name: route.direction_name,
        type: route.transport,
        number: route.route_nmb,
        stops: route.stops,
        schedule: self.getScheduleByRoute(route.id)
    }
};

Application.prototype.searchByRequest = function (request) {
    var out = [];
    var limit = 10;
    for (var i in this.responseData.routes) {
        if (out.length >= limit) break;
        var route = this.responseData.routes[i];

        if (request == route.route_nmb && !/[a-zA-Z]+/gi.test(route.direction_name)) {
            var direction = route.direction_name.split(' - ')[1].trim()
            out.push({type: 'route', transport: route.transport, name: route.route_nmb, direction: direction, id: i});
        }
    }

    for (var i in this.responseData.stops) {
        if (out.length >= limit) break;
        var stop = this.responseData.stops[i];

        if (new RegExp(request, 'ig').test(stop.name))
            out.push({type: 'stop', name: stop.name, id: i});
    }
    return out;
};

Application.prototype.getScheduleByRoute = function (direction_id, valid_from) {
    var arg = {
        workdays:{},
        direction_schedule:{},
        direction_schedule_list:{},
        valid_from:valid_from
    }

    var route = this.responseData.routes[direction_id];
    var oldSchedule = utils.parseRouteData(arg, direction_id, route);
    var newSchedule = {};

    for (var stop_id in oldSchedule) {
        var sch = oldSchedule[stop_id];
        for (var j in sch) {
            var s = sch[j];
            var workdays = s.workdays.split('');
            for (var p in workdays) {
                var workday = (workdays[p] != 7 && workdays[p] != 6) ? 'workdays' : workdays[p];
                if (/[1-5]+/g.test(p) && workday == 'workdays') continue;
                if (!newSchedule[stop_id]) newSchedule[stop_id] = {};
                if (!newSchedule[stop_id][workday]) newSchedule[stop_id][workday] = {};
                if (s.time.h > 0) {
                    if (!newSchedule[stop_id][workday][s.time.h]) newSchedule[stop_id][workday][s.time.h] = [];
                    newSchedule[stop_id][workday][s.time.h].push(s.time.m);
                }
            }
        }
    }
    return newSchedule;
};

Application.prototype.getRoutesByStopId = function (stop_id) {
    var out = [];
    var stop = this.responseData.stops[stop_id];
    for (var i in stop.routes) {
        var route = this.getRouteById(stop.routes[i]);
        route.schedule = route.schedule[stop_id];
        delete route.stops;
        out.push(route);
    }
    return out;
};


Application.prototype.executeTime = function () {
    var self = this;
    fs.readFile(this.APPLICATION_DIR + 'api/times.txt', 'utf8', function (err, data) {
        if (err)
            return console.log(err);

        var blocks = data.split("\n");
        var routes_data = self.responseData.routes;

        for (var i=0; i<blocks.length; i++) {
            var datablock = blocks[i];
            if (datablock.length == 0) continue;

            var index = datablock.indexOf(",");
            var direction_id = datablock.substr(0, index);
            var encoded_data = datablock.substr(index + 1);
            var data = utils.decode(encoded_data);

            var route = routes_data[direction_id];
            if (route) {
                route._times_data = data;
            }
        }
        self.updateDoneProgress();
    });
}

Application.prototype.executeStops = function() {
    var self = this;
    fs.readFile(this.APPLICATION_DIR + 'api/stops.txt', 'utf8', function (err, data) {
        if (err)
            return console.log(err);

        var lines = data.split("\n");
        var si = {};
        var area = "0";
        var street = "0";
        var name = "";

        var key_list = (lines[0] + ";ascii;timediff;indir2").toLowerCase().split(';');
        //ID;Area;Street;Name;Lng;Lat

        for (var i = key_list.length; --i >= 0;)
            si["fld_" + key_list[i].trim()] = i;

        var alias_ready = false;

        for (var i=1, imax = lines.length; i < imax; i++) {
            if (lines[i].length > 1) {
                var parts = lines[i].split(';');

                var id = parts[si["fld_id"]];
                var n  = parts[si["fld_name"]];

                if (n != "" && typeof n == 'string')
                    name = n.replace(/~/g, ' ');

                if ((n = parts[si["fld_area"]]) != "")
                    area = n;

                if ((n = parts[si["fld_street"]]) != "")
                    street = n;

                var neighbours = (si["fld_stops"]) ? parts[si["fld_stops"]].trim() : null;

                var cfg = {
                    id: id,
                    name: name,
                    area: area,
                    street: street,
                    routes: [],
                    neighbours: [],
                    position: {lat: +parts[si["fld_lat"]] / 100000, lng: +parts[si["fld_lng"]] / 100000},
                    indir2: []
                };

                if(neighbours) {cfg.neighbours = neighbours.split(",")};

                if (cfg.position.lat && cfg.position.lng)
                    self.responseData.stops[id] = cfg;

                if(!alias_ready && (id.charAt(0) == "A"))
                    self.responseData.stops[id].is_alias = true;
                else if (!alias_ready)
                    alias_ready = true;
            }
        }
        self.updateDoneProgress();
        self.executeRoutes();
    });
};

Application.prototype.executeRoutes = function () {
    var self = this;
    fs.readFile(this.APPLICATION_DIR + 'api/routes.txt', 'utf8', function (err, data) {
        if (err)
            return console.log(err);
        var si = {
            _routes_data: {},
            _transport_data: {},
            _special_dates: {},
            setStopData: function (cfg) {
                if(!self.responseData.stops[cfg.id]) {
                    self.responseData.stops[cfg.id] = cfg;
                }
                else {
                    for(var key in cfg) {
                        self.responseData.stops[cfg.id][key] = cfg[key];
                    }
                }
            }
        };
        var today = Math.floor((new Date()).getTime() / (1000 * 60 * 60 * 24));

        var lines = data.split("\n");

        var stops_data = self.responseData.stops;

        var city = "";
        var transport = "";
        var operator = "";
        var route_nmb = "";
        var commercial = "";
        var authority = "";
        var direction_id = ""
        var datestart = ""
        var special_dates = [];
        var comments = false;

        var key_list = lines[0].split(';');
        var key_index_map = {};

        for (var i=0; i<key_list.length; i++)
            key_index_map[key_list[i].trim()] = i;

        for (var i=1; i<lines.length; i++) {
            if (lines[i].charAt(0) == "#") {
                var parts = lines[i].split('#');
                var date_from = null;
                var date_to = null;
                var curr_date = new Date();

                if (parts[1] != "")
                    date_from = new Date(parts[1]);

                if (parts[2] != "")
                    date_to = new Date(parts[2]);

                if ((!date_from || date_from <= curr_date)&& (!date_to || date_to >= curr_date) ) {
                    var route = self.responseData.routes[direction_id];
                    if (!route.comments)
                        route.comments = [];
                    route.comments.push({comment: parts[3]});
                    comments = route.comments;
                }
            } else if (lines[i].length > 1) {
                var parts = lines[i].split(';');

                authority = parts[key_index_map["Authority"]] || authority;
                route_nmb = parts[key_index_map["RouteNum"]] || route_nmb;
                datestart = parts[key_index_map["Datestart"]] || datestart;

                if (authority == "SpecialDates") {
                    var dates_collection = {};

                    var dates = parts[key_index_map["ValidityPeriods"]].split(",");
                    var diff_d = 0, day = 0;

                    for (var i_dates = -1, dates_length = dates.length; ++i_dates < dates_length;) {
                        if (dates[i_dates])
                            diff_d = +(dates[i_dates]);

                        day += diff_d;
                        dates_collection[day] = true;
                    }

                    si._special_dates[parts[key_index_map["RouteNum"]]] = dates_collection;
                    continue;
                }

                city = parts[key_index_map["City"]].toLowerCase() || city;
                var cities = city.split(",");

                transport = parts[key_index_map["Transport"]] || transport;

                operator = parts[key_index_map["Operator"]] || operator;
                var route_type = parts[key_index_map["RouteType"]];

                commercial = parts[key_index_map["Commercial"]] || "K"; //commercial;

                direction_id = parts[key_index_map["RouteID"]];
                var direction_name = parts[key_index_map["RouteName"]];
                var weekdays = parts[key_index_map["Weekdays"]];
                var route_tag = parts[key_index_map["RouteTag"]];

                var stops = parts[key_index_map["RouteStops"]].trim().split(",");

                var entry = false;

                for (var j = stops.length; --j >= 0;) {
                    var stop_id = stops[j];
                    if (stops_data[stop_id])
                        stops_data[stop_id].routes.push(direction_id);

                    if (stop_id.charAt(0) == "e") {
                        if (!entry) entry = [];
                        entry[j] = "1";
                        stop_id = stop_id.substring(1);
                        stops[j] = stop_id;
                    }
                    else if (stop_id.charAt(0) == "x") {
                        if (!entry) entry = [];
                        entry[j] = "2";
                        stop_id = stop_id.substring(1);
                        stops[j] = stop_id;
                    } else if (entry)
                        entry[j] = "0";

                    // Странный кусок
                    /*var stop;
                     if ((stop = stops_data[stop_id])) {
                     stop.indir2.push(direction_id);
                     stop.indir2.push(j);
                     } else
                     si.setStopData({name:stops[j], area:"0", street:"0", ascii:stops[j], position: {lat:0, lon:0}, id:stop_id, indir2:[], neighbours:[]});*/
                }

                var parts_special_dates = parts[key_index_map["SpecialDates"]];

                if (parts_special_dates) {
                    special_dates = (parts_special_dates == "0") ? [] : parts_special_dates.split(",");

                    for (var i_special_dates = 0; i_special_dates < special_dates.length; i_special_dates += 2)
                        special_dates[i_special_dates] = si._special_dates[special_dates[i_special_dates]];
                }

                self.responseData.routes[direction_id] = {
                    id            : direction_id,
                    authority     : authority,
                    commercial    : commercial,
                    cities        : cities,
                    city          : city,
                    transport     : transport != 'trol' ? transport : 'trolley-bus',
                    operator      : operator,
                    weekdays      : weekdays,
                    route_type    : route_type,
                    route_tag     : route_tag,
                    route_nmb     : route_nmb,
                    direction_name: direction_name,
                    stops         : stops,
                    entry         : (entry) ? entry.join("") : "",
                    special_dates : special_dates,
                    datestart : datestart
                };

                if (comments) {
                    if (parts[key_index_map["RouteNum"]] || parts[key_index_map["Transport"]] || parts[key_index_map["Authority"]])
                        comments = false; // new route - current comments should be reset
                    else
                        self.responseData.routes[direction_id].comments = comments;
                }

                if (transport == "distantbus" && route_nmb == "17")
                    k = k;

                var valid_to = parts[key_index_map["ValidityPeriods"]];

                if (valid_to) {
                    var route = self.responseData.routes[direction_id];
                    valid_to = valid_to.split(",");

                    for (var k = 0, days = 0; k < valid_to.length; k++) {
                        if (valid_to[k]) {
                            days += +(valid_to[k]);
                            valid_to[k] = days;
                        }
                    }

                    for (var k = 1, kk=0; k < valid_to.length; k+=2) {

                        var valid_from = valid_to[k-1] || 0;

                        if (valid_from > today + 14 && kk > 0)
                            break;

                        if (valid_from <= today && (!valid_to[k] || valid_to[k] > valid_from + 14))
                            kk = 0;

                        if (valid_to[k]) {
                            if (valid_to[k] < today)
                                continue;

                            if (valid_to[k] <= today + 14)
                                route["valid_to" + kk] = valid_to[k];
                        }

                        route["valid_from" + kk] = valid_from;
                        ++kk;
                    }
                }

            }
        }
        self.updateDoneProgress();
        self.executeTime();
    })
}

module.exports = new Application();