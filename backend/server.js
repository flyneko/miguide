var app = require('express')();
var Application = require('./application');

app.get('/search/:request', function(req, response) {
    Application.generateJSONResponse(response, Application.searchByRequest(req.params.request));
});

app.get('/route/:id', function(req, response){
    Application.generateJSONResponse(response, Application.getRouteById(req.params.id) );
});

app.get('/stop', function(req, response) {
    Application.getData(response, 'stops');
});

app.get('/stop/routes/:id', function(req, response) {
    Application.generateJSONResponse(response, Application.getRoutesByStopId(req.params.id));
});

app.get('/schedule/:route_id', function(req, response) {
    Application.generateJSONResponse(
        response,
        Application.getScheduleByRoute(req.params.route_id, 1)
    );
});

app.get('/track/:route_id', function(req, response) {
    Application.trackRouteById(req.params.route_id, function(answer) {
        Application.generateJSONResponse(response, answer);
    });
});

app.get('/route_search', function(req, response) {
    Application.routeSearch(req.query, function(answer) {
        Application.generateJSONResponse(response, answer);
    });
});

app.listen(3000, function(){
    console.log('listening on *:3000');
});