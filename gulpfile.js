var gulp = require('gulp');
var coffee = require('gulp-coffee');
var concat = require('gulp-concat');
var uglify  = require('gulp-uglify');
var livereload = require('gulp-livereload');

gulp.task('libs', function () {
    gulp.src([
        'libs/jquery.min.js',
        'libs/materialize.min.js',
        'libs/modernizr.custom.js',
        'libs/lockr.min.js',

        'libs/json2.js',
        'libs/underscore-min.js',
        'libs/backbone-min.js',
        'libs/richmarker.js',
        'libs/markerclusterer.js',
        'libs/jquery.actual.min.js'
    ])
        .pipe(uglify())
        .pipe(concat('libs.js'))
        .pipe(gulp.dest('public/js'));
})

gulp.task('coffee', function () {
    gulp.src([
        'app/constants.coffee',
        'app/models/MapModel.coffee',
        'app/models/RouteModel.coffee',
        'app/models/StopModel.coffee',
        'app/models/UserModel.coffee',
        'app/views/ApplicationView.coffee',
        'app/views/StopInfoView.coffee',
        'app/views/RouteInfoView.coffee',
        'app/views/RouteSearchView.coffee',
        'app/views/StopView.coffee',
        'app/views/UserView.coffee',
        'app/collections/RouteCollection.coffee',
        'app/collections/StopCollection.coffee',
        'app/Application.coffee',
        'app/general.coffee',
    ])
        .pipe(concat('a.coffee'))
        .pipe(coffee())
        .pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(gulp.dest('public/js'))
        .pipe(livereload());
})

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('app/**/*.coffee', ['coffee']);
});