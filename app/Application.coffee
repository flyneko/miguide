class @Application
  transportLabels: { 'bus': 'автобус', 'trolley-bus': 'троллейбус', 'tram': 'трамвай', 'metro': 'метро' }

  defaultOptions:
    tie: 0
    schedule_limit: 30

  getOption: (name) ->
    Lockr.get('options')[name]

  setOption: (name, value) ->
    options = Lockr.get('options')
    options[name] = value
    Lockr.set('options', options)

  constructor:() ->
    self = @
    if !Modernizr.geolocation
      Materialize.toast('Ваше устройство не поддерживается', 5000)

    Lockr.prefix = 'miguide_'

    if !Lockr.get('options', null)
      Lockr.set('options', @defaultOptions)
    Backbone.Model.prototype.app = Backbone.Collection.prototype.app = Backbone.View.prototype.app = @

    if !navigator.onLine
      Materialize.toast('Отсутствует соединение с интернетом', 5000)

    google.maps.event.addDomListener(window, 'load', (() ->
      self.map = new Map('map-canvas')
      self.user = new User()
      self.view = new ApplicationView()
      self.stops = new StopCollection()
      self.routeInfoView = new RouteInfoView()
      self.stopInfoView = new StopInfoView()
      self.routeSearchView = new RouteSearchView()
    ))
    Number.prototype.zero = () ->
      (if @valueOf() < 10 then '0' else '') + @valueOf().toString()
    Number.prototype.hourConvert = () ->
      if @valueOf() >= 24 then @valueOf() - 24 else @valueOf()