class RouteSearchView extends Backbone.View
  stopTemplate: _.template('<div class="route-search-stop"><span class="route-search-stop-time"><%= time %></span><span class="route-search-stop-name"><%= name %></span></div>')
  actionTemplate: _.template('<div style="color: <%= color %>" class="route-search-action"><span class="route-search-action-duration"><%= duration %></span><div class="route-search-action-self"><%= self %></div></div>')

  activeMarkerSelect: null
  locations: {from: {}, to: {}}

  el: '#route-search'
  events:
    'click .route-search-select-location': 'selectLocation'
    'submit form': 'search'

  initialize: () ->
    self = @
    $('.route-search-modal-trigger').click(() ->
      $('<a href="#route-search">').leanModal({
        complete: () ->
          self.app.user.view.marker.setMap self.app.map.object
        ready: () ->
          self.renderLayout()
          self.app.view.windowsStack.push (() -> self.$el.find('.modal-close-button').click())
      }).click()
      false
    )

  search: () ->
    self = @
    $wrapper = self.$el.find('.route-search-actions-wrapper')
    getColor = (type, transport = '') ->
      switch type
        when 'riding'
          switch transport
            when 'bus' then return '#075E00'
            when 'trolleybus' then return '#D38100'
            when 'tram' then return '#0076C9'
            when 'metro' then return '#930000'
            when 'routetaxi' then return 'yellow'
            else return 'black'
        when 'walking' then return '#303030'
        else return 'grey'

    drawArrow = (path, color) ->
      h = []
      h.push new google.maps.LatLng(p.lat, p.lon) for p in path
      self.map.drawPolyline({ path: h, geodesic: !0, strokeColor: color, strokeOpacity: 0.7, strokeWeight: 4, zIndex: 2 })

    $.ajax({
      beforeSend: () -> self.$el.find('[type=submit]').attr('disabled', true)
      complete: () -> self.$el.find('[type=submit]').attr('disabled', false)
      url: SERVER_URL + '/route_search?' + decodeURIComponent($.param({from: @locations.from.coordinates, to: @locations.to.coordinates}))
      success: (response) ->
        time = new Date
        duration = 0
        movements = response.pathes[0]

        self.map.clearMap()

        if _.isEmpty(response.pathes)
          $wrapper.html('<span class="route-search-tooltip">Ничего не найдено</span>')
          return false

        for s, index in response.stops
          if index == 0 or (index == response.stops.length - 1) then continue
          stop = new Stop({name: s.name, position: {lat: s.lat, lng: s.lon}})
          stop.view.$el.off('click')
          stop.view.marker.setMap self.map.object
          self.map.markers.push stop.view.marker

        resultsHtml = ''
        for action, index in movements.actions
          duration += action.dur
          stop = response.stops[action.si]
          route = if response.routes[action.ri] then response.routes[action.ri] else null
          if action.type == 'riding'
            drawArrow(movements.track.slice(action.ti, action.ti + action.tl), getColor(action.type, route.type))
          if action.type != 'riding' and action.type != 'waiting' and index != 0
            drawArrow([response.stops[movements.actions[index - 1].si], stop], getColor(action.type))

          actionTime = new Date(time.getTime() + duration * 60000)

          if index != 0
            if action.type == 'riding' then i = '<span class="transport-item ' + (if route.type == 'trolleybus' then 'trolley-bus' else route.type) + '">' + route.num + '</span>'
            if action.type == 'waiting' then i = '<i class="mdi-action-schedule"></i> Ожидание'
            if action.type == 'walking' then i = '<i class="mdi-maps-navigation"></i> Пешком'
            color = getColor(action.type, if route then route.type else null)
            resultsHtml += self.actionTemplate({color: color, duration: action.dur, self: i})

          resultsHtml += self.stopTemplate({time: actionTime.getHours().zero() + ':' + actionTime.getMinutes().zero(), name: stop.name})

        self.map.centerizeViewToPolyline self.map.polylines
        $wrapper.html '<div id="route-search-actions">' + resultsHtml + '</div>'
        self.app.view.recalculateModalContentHeight(self.$el)
    })

    false

  selectLocation: (e) ->
    $this = $(e.target)
    $this.attr('disabled', true)
    @activeMarkerSelect = $this.data('location')

  renderLayout: () ->
    self = @
    if @RENDERED then return false
    if !@map then @map = new Map('route-search-map', {scrollwheel: false, zoomControl: true})
    @app.user.view.marker.setMap @map.object
    geocoder = new google.maps.Geocoder()
    @$el.find('input[data-location]').each(() ->
      $input = $(@)
      autocomplete = new google.maps.places.Autocomplete(@, { types: ['address'], componentRestrictions: {country: 'by'} })
      autocomplete.addListener('place_changed', () ->
        place = autocomplete.getPlace()
        if place.length == 0 then return
        self.locations[$input.attr('data-location')].marker.setPosition(place.geometry.location)
        self.locations[$input.attr('data-location')].coordinates = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()}
      )
    )

    @locations.from.marker = new google.maps.Marker({ label: 'A', map: @map.object, draggable: false, animation: google.maps.Animation.DROP})
    @locations.to.marker = new google.maps.Marker({ label: 'B', map: @map.object, draggable: false, animation: google.maps.Animation.DROP})

    @map.object.addListener('click', (e) ->
      if (!self.activeMarkerSelect) then return false
      self.$el.find('button[data-location=' + self.activeMarkerSelect + ']').attr('disabled', false)
      self.locations[self.activeMarkerSelect].marker.setPosition(e.latLng)
      self.locations[self.activeMarkerSelect].coordinates = {lat: e.latLng.lat(), lng: e.latLng.lng()}
      geocoder.geocode({'location': {lat: e.latLng.lat(), lng: e.latLng.lng() }}, (results, status) ->
        if status == google.maps.GeocoderStatus.OK
          if (results[1])
            self.$el.find('input[data-location=' + self.activeMarkerSelect + ']').val(results[1].formatted_address)
          else
            window.alert('No results found')
        else
          window.alert('Geocoder failed due to: ' + status)
        self.activeMarkerSelect = null
      )
    )
    @app.view.recalculateModalContentHeight(@$el)
    @$el.find('.modal-content').scrollTop(0)
    @RENDERED = true
