class StopInfoView extends Backbone.View
  el: '#stop-info-view'
  events:
    'click .stop-info-view-close': () -> @$el.hide(); @$el.find('[data-tooltip]').tooltip('remove')
  $scheduleModal: $('#stop-schedule')
  $nearScheduleList: $('#schedule-list')
  nearScheduleInterval: null

  initialize: () ->
    self = @
    @$scheduleModal.find('.tabs li:visible a').first().click()
    @$el.find('.stop-info-schedule-button').click(() ->
      $('<a href="#stop-schedule">').leanModal({
          ready: () ->
            self.app.view.windowsStack.push (() -> self.$scheduleModal.find('.modal-close-button').click())
            self.$scheduleModal.find('.tabs li:visible a').first().click()
      }).click()
      false
    )

  renderLayout: () ->
    self = @
    stopRouteGroupItem = _.template("<div class='stop-routes-group' data-position='top' data-tooltip='<%= type_label %>'><span class='transport-item <%= type %>'></span><%= routes %></div>")
    nearScheduleListItem = _.template('<li data-position="right" data-tooltip="<%= tooltip %>">
              <span class="schedule-item-delay"><%= difference %></span>
              <span class="transport-item <%= type %>"><%= number %></span>
          </li>')
    scheduleTableItem = _.template('<tr>
              <td><span class="transport-item <%= type %>"><%= number %></span></td>
              <td><%= schedule %></td>
          </tr>')
    scheduleGroupItem = _.template('<div class="stop-schedule-group"><span><%= hour %></span> <%= minutes %></div>')
    scheduleListLimit = @app.getOption('schedule_limit')
    scheduleTableObject = {}

    generateNearSchedule = () ->
      now = new Date
      scheduleArray = []
      _.each(self.model.get('routes').models, (route) ->
        scheduleKey = if /[1-5]+/g.test(now.getDay()) then 'workdays' else now.getDay()
        if scheduleKey == 0 then scheduleKey = 7 # 0 is a sunday
        daySchedule = route.get('schedule')[scheduleKey]
        count = 0

        for hour, minutes of daySchedule
          hour = (+hour).hourConvert()
          if (hour < now.getHours()) then continue
          if count >= scheduleListLimit then break
          count += minutes.length
          for m in minutes
            if hour == now.getHours() and m < now.getMinutes() then continue
            scheduleArray.push {id: route.get('id'), num: route.get('number'), type: route.get('type'), h: hour, m: m}
      )
      scheduleArray = _.sortBy(_.sortBy(scheduleArray, 'm'), 'h')
      self.$nearScheduleList.find('[data-tooltip]').tooltip('remove')
      self.$nearScheduleList.empty()

      for s in scheduleArray.slice(0, scheduleListLimit)
        q = new Date; q.setHours(s.h); q.setMinutes(s.m)
        difference = Math.round ( (q - now) / 1000) / 60
        if difference > 60
          difference = '+' + (Math.round(difference / 60)) + ' ч. ' + (difference % 60) + ' м.'
        else if difference == 0
          difference = 'сейчас'
        else
          difference = '+' + difference + ' минут'
        $nearScheduleItemList = $( nearScheduleListItem({type: s.type, number: s.num, difference: difference, tooltip: self.app.transportLabels[s.type] + ' в ' + s.h.zero() + ':' + s.m.zero() }) )
        $nearScheduleItemList.tooltip({delay: 50})
        $nearScheduleItemList.find('.transport-item').data('id', s.id).click(() -> v = self.app.routeInfoView; v.$block.hide(); v.$schedule.show(); v.render( new Route({id: $(@).data('id')}), () -> v.showStopSchedule( self.model.get('id') ) ))
        self.$nearScheduleList.append $nearScheduleItemList

    _.each(@model.get('routes').models, (route) ->
      for day, schedule of route.get('schedule')
        if !scheduleTableObject[day] then scheduleTableObject[day] = {}
        scheduleTableObject[day][route.get('number')] = { type: route.get('type'), schedule: schedule }
    )

    $('.stop-schedule-tab').empty()
    $('#stop-schedule .tabs li').hide()

    generateNearSchedule()
    # update near schedule
    if @nearScheduleInterval then clearInterval @nearScheduleInterval
    @nearScheduleInterval = setInterval(generateNearSchedule, 1000 * 60)

    for day, tableRoutes of scheduleTableObject
      $scheduleTable = $('<table class="striped"><tbody></tbody></table>')
      scheduleTableHtml = ''
      $dayTab = self.$el.find("[data-day='#{day}']")
      for routeNumber, routeInfo of tableRoutes
        q = ''
        for hour, minutes of routeInfo.schedule
          q += scheduleGroupItem({ hour: (+hour).hourConvert(), minutes: _.map(minutes, (v) -> v.zero()).join(' ') })
        if !_.isEmpty(q) then scheduleTableHtml += scheduleTableItem({ type: routeInfo.type, number: routeNumber, schedule: q })
      $scheduleTable.find('tbody').html scheduleTableHtml
      @$el.find('[href="#' + $dayTab.attr('id') + '"]').parent().show()
      $dayTab.append($scheduleTable)

    $('.stop-info-title').text @model.get('name')
    stopRoutes = _.groupBy(@model.get('routes').models, (r) -> r.get('type'))
    stopRoutesHtml = ''

    # stop routes
    for type, typeRoutes of stopRoutes
      stopRoutesNumbers = []
      stopRoutesNumbers.push r.get('number') for r in typeRoutes
      stopRoutesHtml += stopRouteGroupItem({type: type, type_label: @app.transportLabels[type], routes: _.uniq(stopRoutesNumbers).join(' ') })

    @$el.find('#stop-info-routes').html stopRoutesHtml
    @$el.find('.stop-routes-group').tooltip {delay: 50}
    @$el.show()
    @app.view.windowsStack.push (() -> self.$el.hide())
    @$nearScheduleList.scrollTop(0)
    @app.view.recalculateModalContentHeight(@$el)

  render: (@model) ->
    self = @
    @app.map.object.panTo(new google.maps.LatLng(@model.get('position').lat, @model.get('position').lng))
    model = @model
    @$el.find('[data-tooltip]').tooltip('remove')
    if @model.FETCHED
      @renderLayout()
    else
      $.ajax({
        url: model.collection.url + '/routes/' + model.get('id')
        dataType: 'json'
        success: (routes) ->
          if _.isEmpty(routes) then Materialize.toast('Информации по остановке не найдено', 3000); return false
          self.model.FETCHED = true
          collection = new RouteCollection(routes)
          model.set('routes', collection)
          self.renderLayout()
      })