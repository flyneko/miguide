class ApplicationView extends Backbone.View
  el: '#application'
  windowsStack: []
  $searchForm: $('#search-form')
  $searchResults: $('#search-results')
  events:
    'click *': (e) ->
      $this = $(e.target)
      # Hide search results
      if $this.id != 'search-trigger' and !$this.parents('#search-trigger').length and $this.id != 'search-form' and !$this.parents('#search-form').length then @$searchResults.hide(); @$searchForm.fadeOut()
    'click #search-trigger': () -> @$searchForm.fadeIn(); $('#search-input').focus(); false
    'click #search-close, .search-result-item': () -> @$searchForm.fadeOut()
    'click #nav-mobile a': () -> $('#nav-mobile').sideNav('hide')
    'click .go-to-user': () -> if @app.user.view.marker then p = @app.user.get('position'); @app.map.object.panTo(new google.maps.LatLng(p.lat, p.lng)) else Materialize.toast('Невозможно определить местоположение', 3000)
    'click .modal-close-button': (e) -> $('.lean-overlay').click();
    'focus #search-input' : () -> @$searchResults.show()
    'input #search-input': (e) -> s = @; clearTimeout(@searchTimer); @searchTimer = setTimeout((() -> s.search e.target), 600)
    'submit #search-form': () -> false

  search: (input) ->
    value = input.value
    if _.isEmpty(value) then @$searchResults.hide(); return false
    self = @
    $.ajax({
      url: SERVER_URL + '/search/' + value
      dataType: 'json'
      success: (response) ->
        if _.isEmpty(response) then self.$searchResults.hide() else self.$searchResults.show()
        out = ''
        if !self.$searchResults.length
          self.$searchResults = $('<div id="search-results">')
          self.$searchResults.appendTo self.$searchForm.find('.input-field')

        for r in response
          out += '<div data-id="' + r.id + '" data-type="' + r.type + '" class="search-result-item">' +
            '<div>' + (if r.type == 'route' then '<span class="transport-item ' + r.transport + '"></span>' else '<img width="25" src="public/images/icons/stop-icon.png">') + '</div>' +
            '<div>' + r.name +
            (if r.type == 'route' then '<span class="search-result-item-direction">' + r.direction + '</span>' else '') +
            '<span class="search-result-item-type">' + (if r.type == 'route' then self.app.transportLabels[r.transport] else 'остановка') + '</span>' +
            '</div>' +
            '</div>'
        self.$searchResults.html out
        $('.search-result-item').click(() ->
          $this = $(@)

          if $this.data('type') == 'stop'
            stop = self.app.stops.get $this.data('id')
            self.app.map.object.setZoom 17
            stop.view.$el.click()

          if $this.data('type') == 'route'
            self.app.routeInfoView.render new Route({id: $this.data('id')})
          self.$searchForm.fadeOut()
        )
    })

  recalculateModalContentHeight: ($parent) ->
    $modalContent = $parent.find('.modal-content')
    $modalContent.css('max-height', 'none')
    $modalContent.css('max-height', ($modalContent.parents('.modal').first().actual('height') - $parent.find('.modal-header').actual('height')))

  initialize: () ->
    self = @
    $('.modal-trigger').leanModal()
    $("#side-nav-button").sideNav()
    $('select').material_select()

    document.addEventListener("backbutton", (() ->
      if !$('#settings').is(':visible') && !self.app.routeSearchView.$el.is(':visible') && !self.app.stopInfoView.$scheduleModal.is(':visible') && !$('#route-info').is(':visible') && !self.app.stopInfoView.$el.is(':visible')
        navigator.app.exitApp()
        return false
      $('#settings').closeModal()
      (self.windowsStack.pop())()
    ), false)

    $.ajaxSetup({
      beforeSend: () -> $('#loader').stop(true).fadeIn()
      complete: () -> $('#loader').stop(true).fadeOut()
      error: () -> Materialize.toast('Ошибка выполнения запроса. Повторите позже', 5000)
    })

    self = @
    $('[data-option="schedule_limit"]').change(() -> self.app.setOption('schedule_limit', @value)).val @app.getOption('schedule_limit')
    $('[data-option="marker"]').change(() -> self.app.user.set('marker', @value)).val(@app.user.get('marker'))
    $('[data-swtich-option]').change(() ->
      self.app.setOption($(@).attr('data-swtich-option'), +@checked)
    ).each(() ->
      $el = $(@)
      $el.prop('checked', self.app.getOption($el.attr('data-swtich-option')))
    )
