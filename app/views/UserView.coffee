class UserView extends Backbone.View
  watcherID: null
  marker: null

  initialize: () ->
    @createMarker()

  createMarker: () ->
    self = @
    watcherOptions = {
      enableHighAccuracy: true
      timeout: 15000
      maximumAge: 1000
    }

    if @marker
      @marker.setMap null
    if @watcherID
      navigator.geolocation.clearWatch(@watcherID)

    navigator.geolocation.getCurrentPosition(((p) ->
      position = self.model.get('position')
      if _.isEmpty(position) then position = { lat: p.coords.latitude, lng: p.coords.longitude }
      self.marker = new RichMarker({ content: '<div style="background-image: url(public/images/markers/' + self.model.get('marker') + '.png)" class="map-marker"></div>', map: self.app.map.object, draggable: false, zIndex: 99, shadow: 'none', position: new google.maps.LatLng(position.lat, position.lng) })

      self.watcherID = navigator.geolocation.watchPosition(((position) -> self.trackPosition(position)), ((e) -> self.trackPositionError(e)), watcherOptions)
    ), (() -> Materialize.toast('Ошибка определения местоположения', 3000)), watcherOptions)

  trackPositionError: (err) ->
    Materialize.toast('Ошибка отслеживания(' + err.code + '): ' + err.message, 3000);

  trackPosition: (p) ->
    position = { lat: p.coords.latitude, lng: p.coords.longitude }
    oldPosition = @model.get('position')
    @model.set('position', position)
    if oldPosition != position
      if @app.getOption('tie') then @app.map.object.panTo(new google.maps.LatLng(position.lat, position.lng))


