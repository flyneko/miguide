class StopView extends Backbone.View
  template: _.template('<div data-position="top" data-tooltip="<%= name %>" class="map-marker stop">'),
  render: () ->
    @$el = $(@template({name: @model.get('name')}))
    @$el.data('model-id', @model.get('id'))
    @
  initialize: () ->
    self = @
    @render()
    position = @model.get('position')
    @$el.hover(() ->
      if !self.$el.data('tooltipped')
        self.$el.data('tooltipped', true).tooltip({delay: 50}).mouseenter()
    ).click(() ->
      if self.app.routeInfoView.$el.is(':visible')
        self.$el.trigger('click.modalHandler')
      else
        self.app.stopInfoView.render self.model
    )

    @marker = new RichMarker({ position: new google.maps.LatLng(position.lat, position.lng), draggable: false, zIndex: 1, shadow: 'none',  content: self.$el[0] })
