class RouteInfoView extends Backbone.View
  el: '#route-info'
  $block: $('#route-info-block')
  $schedule: $('#route-info-schedule')
  events:
    'click .route-info-back-wrapper': () -> @$block.show(); @$schedule.hide(); @showMap()

  trackVehicles: () ->
    self = @
    if !@map then return false
    $.get(SERVER_URL + '/track/' + @model.get('id'), (response) ->
      for id, vehicle of response
        if !self.markers[id]
          self.markers[id] = new RichMarker({ content: '<div  style="background-image: url(public/images/icons/' + self.model.get('type') + '-icon.png)" class="map-marker"></div>', map: self.map.object, draggable: false, zIndex: 99, shadow: 'none', position: new google.maps.LatLng(vehicle.lat, vehicle.lng) })
        else
          self.markers[id].setPosition(new google.maps.LatLng(vehicle.lat, vehicle.lng))
      for id, marker of self.markers
        if !response[id] then marker.setMap null; delete self.markers[id]

      self.trackTimeout = setTimeout(self.trackVehicles, 15000)
    )

  showStopSchedule: (id) ->
    self = @
    schedule = @model.get('schedule')[id]
    @$el.find('.route-info-stop').html @app.stops.get(id).get('name')
    scheduleTableItem = _.template('<tr><td class="route-info-schedule-hour"><%= hour %></td><td><%= minutes %></td></tr>')
    @$schedule.show()
    @$block.hide()
    for day, tableRoutes of schedule
      $scheduleTable = $('<table class="striped"><tbody></tbody></table>')
      scheduleTableHtml = ''
      $dayTab = @$el.find("[data-day='#{day}']")
      for hour, minutes of tableRoutes
        scheduleTableHtml += scheduleTableItem({ hour: (+hour).hourConvert(), minutes: _.map(minutes, (v) -> v.zero()).join(' ') })
      $scheduleTable.find('tbody').html scheduleTableHtml
      @$el.find('[href="#i' + $dayTab.attr('id') + '"]').parent().show()
      $dayTab.empty().append($scheduleTable)
    @$el.find('.tabs li:visible a').first().click()
    @app.view.recalculateModalContentHeight(@$schedule)
    @$schedule.find('.modal-content').scrollTop(0)
    @app.view.windowsStack.push (() -> self.$el.find('.modal-back-button').click())


  render: (@model, onfetch = null) ->
    self = @
    @$schedule.hide()
    @$block.show()
    model = @model
    @markers = {}
    @model.fetch({
      success: () ->
        transportLabel = self.app.transportLabels[model.get('type')]
        self.$el.find('.route-info-title').html transportLabel.charAt(0).toUpperCase() + transportLabel.slice(1) + ' №' + model.get('number')
        self.$el.find('.route-info-back-transport').html '<span class="transport-item ' + model.get('type') + '">' + model.get('number') + '</span>'
        $routeStopsWrapper = self.$el.find('#route-info-stops')
        routeStopTemplate = _.template('<div data-id="<%= id %>" class="route-info-stop-item"> <span><%= name %></span> </div>')
        self.$el.find('.route-info-number').html model.get('number')
        self.$el.find('.route-info-direction b').html model.get('name')
        $routeStopsWrapper.empty()
        for i in model.get('stops')
          s = self.app.stops.get(i)
          model.stops.push s
          $routeStop = $( routeStopTemplate({id: s.get('id'), name: s.get('name')}) )
          $routeStop.click(() -> self.showStopSchedule( $(@).data('id') ))
          $routeStopsWrapper.append $routeStop
        self.app.view.recalculateModalContentHeight(self.$block)
        $('<a href="#route-info">').leanModal({
          complete: () ->
            self.map = null
            s.view.marker.setMap null for s in self.model.stops
            clearTimeout self.trackTimeout
          ready: () ->
            self.app.view.windowsStack.push (() -> self.$el.find('.modal-close-button').click())
            self.$block.find('.modal-content').scrollTop(0)
            if onfetch then onfetch()
            self.showMap()
        }).click()
    })
    @

  showMap: () ->
    self = @
    if !@$block.is(':visible') then return false
    if !@map then @map = new Map('route-info-map', {scrollwheel: false, zoomControl: true})
    stopCoordinates = []
    for s in @model.stops
      stopCoordinates.push s.get('position')
      s.view.marker.setMap self.map.object
      s.view.$el.bind('click.modalHandler', () -> self.showStopSchedule( $(@).data('model-id') ))
    @map.drawPolyline({ path: stopCoordinates, strokeWeight: 4, strokeOpacity: 0.7, strokeColor: '#258E7C'}, true)
    if @model.get('type') != 'metro'
      @trackVehicles()