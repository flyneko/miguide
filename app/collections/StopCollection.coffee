class StopCollection extends Backbone.Collection
  model: Stop
  url: SERVER_URL + '/stop'
  initialize: () ->
    self = @
    @markers = []
    @listenTo(@, 'add', (stop) ->
      self.markers.push(stop.view.marker)
    )
    @fetch({
      success: () -> new MarkerClusterer(self.app.map.object, self.markers)
    })
