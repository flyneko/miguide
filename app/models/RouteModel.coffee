class Route extends Backbone.Model
  urlRoot: SERVER_URL + '/route'
  idAttribute: 'id'

  initialize: () ->
    @stops = []
