class User extends Backbone.Model
  defaults:
    position: {}
    marker: 1

  initialize: () ->
    localUser = Lockr.get('user', null)
    if (!localUser)
      localUser = { marker: Math.floor(Math.random() * 14) + 1 }
      Lockr.set('user', localUser)
    @set(localUser)

    @view = new UserView({ model: @ })
    @bind('change:marker', (user, marker) -> u = Lockr.get('user'); u.marker = marker; Lockr.set('user', u); user.view.createMarker() )
    @bind('change:position', ((user, p) -> user.view.marker.setPosition(new google.maps.LatLng(p.lat, p.lng)) ))