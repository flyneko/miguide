class Map extends Backbone.Model
  API_KEY: 'AIzaSyADgOdY3bLje-BaCx-NdoiazWLtZn9n1TU'
  polylines: []
  markers: []

  clearMap: () ->
    a.setMap null for a in @polylines
    a.setMap null for a in @markers

  drawPolyline: (polylineOptions, centerize = false) ->
    path = new google.maps.Polyline(polylineOptions)
    @polylines.push path
    path.setMap @object
    if centerize then @centerizeViewToPolyline(path)

  centerizeViewToPolyline: (polylines) ->
    bounds = new google.maps.LatLngBounds
    if _.isArray(polylines)
      for polyline in polylines
        points = polyline.getPath().getArray()
        bounds.extend i for i in points
    else
      points = polylines.getPath().getArray()
      bounds.extend i for i in points
    @object.fitBounds(bounds)

  initialize: (selector, options = {}) ->
    self = @
    defaultLatLng = new google.maps.LatLng(53.903069, 27.556481); # Add the coordinates
    mapOptions =
      center: defaultLatLng,
      zoom: 11, # The initial zoom level when your map loads (0-20)
      maxZoom: 18, # Maximum soom level allowed (0-20)
      zoomControl:false, # Set to true if using zoomControlOptions below, or false to remove all zoom controls.
      mapTypeId: google.maps.MapTypeId.ROADMAP, # Set the type of Map
      scrollwheel: true, # Enable Mouse Scroll zooming
      panControl:false, # Set to false to disable
      mapTypeControl:false, # Disable Map/Satellite switch
      scaleControl:false, # Set to false to hide scale
      streetViewControl:false, # Set to disable to hide street view
      overviewMapControl:false, # Set to false to remove overview control
      rotateControl:false # Set to false to disable rotate control

    @object = new google.maps.Map(document.getElementById(selector), _.extend(mapOptions, options));
    noPoi = [{ featureType: "poi", stylers: [{visibility: "off" }]}]
    @object.setOptions({styles: noPoi});