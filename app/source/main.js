
/* */

function StopsInfo(city, content) {

    this.city = city;
    this.stops_file = "stops.txt";
    this.routes_file = "routes.txt";
    this.times_file = "times.txt";

    this.ready = false;
    this._temp = {routes:{}, routes_new:{}};

}

StopsInfo.prototype.execute = function(arg, fnc) {
    //console.log();

    if(this.ready) {
        //fnc(this, arg);
        fnc(arg);
    }
    else {
        if(this.load_started) return;
        this.load_started = true;
        this.load(arg, fnc);
    }
}

/*
 var arg = {
 directions_stack:[args[1]],
 stop_id:args[2],
 workdays:{},
 direction_schedule:{},
 stop_schedule_byhour:{}
 }
 */


StopsInfo.prototype.executeOnPolylinesAll = function(arg, fnc) {
    if(arg.directions.length == 0) {
        fnc(arg);
    }
    else {
        var directions_id = arg.directions.pop();

        CDownloadUrl("get", pc.content[pc.city].data + directions_id + ".txt", function (doc) {
            var lines = doc.split("\n");
            arg.data[directions_id] = {levels:lines[0], line:lines[1]};

            pc.getSI().executeOnPolylinesAll(arg, fnc);
        });
    }
}



StopsInfo.prototype.executeOnPolylines = function(arg, fnc) {

    CDownloadUrl("get", pc.content[pc.city].data + arg.directions.main + ".txt", function (doc) {
        var lines = doc.split("\n");
        arg.polylines = {main: {levels:lines[0], line:lines[1]}};

        if(arg.directions.opposite) {
            CDownloadUrl("get", pc.content[pc.city].data + arg.directions.opposite + ".txt", function (doc) {
                var lines = doc.split("\n");
                arg.polylines.opposite = {levels:lines[0], line:lines[1]};
                fnc(arg);
            });
        }
        else {
            fnc(arg);
        }
    });
}

StopsInfo.prototype.isCached = function(direction_id) {
    return (direction_id in this._temp.routes_new);
}

StopsInfo.prototype.executeOnScheduleList = function(arg, fnc) {

    var si = this;

    if (!si._times_data)
    {
        // prepare_times_data here!
        CDownloadUrl("get", si.times_file, function (doc) {
            var blocks = doc.split("\n"); // split the document into lines

            si._times_data = {};
            var routes_data = si._routes_data;

            for (var i=0; i<blocks.length; i++) {
                var datablock = blocks[i];
                if (datablock.length == 0) continue;

                var index = datablock.indexOf(",");
                var direction_id = datablock.substr(0, index);
                var encoded_data = datablock.substr(index + 1);
                var data = si.decode2(encoded_data);

                si._temp.routes_new[direction_id] = data; //si.decode2(encoded_data);
                //console.log(data);

                var route = routes_data[direction_id];
                if (route) {
                    route._times_data = data;
                }
            }

            if(!arg.download) { // jeigu arg.download = true... tai tik nuskaitome
                si.executeOnScheduleList(arg, fnc);
            }
            else {
                fnc(arg);
            }

        });
    }
    else if(arg.download) { // jau atsisiunteme times.txt anksciau, bet nenorime nieko parsinti dabar...
        fnc(arg);
    }
    else {
        for(var i=0; i<arg.directions_stack.length; i++) {
            var direction_id = arg.directions_stack[i];

            if (!(direction_id in si._temp.routes_new)) {
                var coredata = si.decode2(direction_id); // sena decode versija
                si._temp.routes_new[direction_id] = coredata;
            }
            else { var coredata = si._temp.routes_new[direction_id]; }

            si.parseRouteData2(arg, direction_id, coredata);
        }

        if (!arg.light) { // jeigu duotas argumentas light - tai nedarome dienu susinkimo...
            var all_workdays = [];
            for(var direction_id in arg.workdays) {
                all_workdays = all_workdays.concat(arg.workdays[direction_id]);
            }
            arg.all_workdays = split_days(all_workdays.unique());
        }
        fnc(arg);
    }
}

function arr_debug(arr) {
    var debug = [];
    for(var i=0; i<arr.length; i++) {
        debug[i] = i + ":" + arr[i];
    }
    return debug;
}

function classic_time_item(time) {
    var minutes = time % 60;
    var hours = Math.floor(time/60);
    return {hour:hours, minute:minutes};
//   (minutes<10)?("0"+minutes):minutes].join(""));
}


function classic_time_list(minute_list) {
    var classic_arr = [];

    for(var i=0; i<minute_list.length; i++) {
        classic_arr.push(toStringTime(classic_time_item(minute_list[i])));
    }
    return classic_arr;
}


StopsInfo.prototype.decode_to_classic = function(direction_id, decoded_data) {
    //var decoded_data = this.decode(direction_id);
    var data = [";"+decoded_data.workdays.join(",")];

    var stops = this._routes_data[direction_id].stops;
    for(var i=0; i<stops.length; i++) {
        var classic_format = classic_time_list(decoded_data.times[i]);
        data.push(stops[i] + ";" + classic_format.join(","));
    }
    return data.join("\r\n");
}


StopsInfo.prototype.decode2 = function(decoded_data) {
//StopsInfo.prototype.decode2 = function(direction_id) {
    //   var decoded_data = this._times_data[direction_id];

    //decoded_data = "495,600,-540,,12345,1,123457,1,6,,1,,3,,11,,11,,3,,1,,"; //

    var timetable = []; // atkoduoti laikai
    var weekdays = []; // atkoduotos savaites dienos
    var valid_from = []; // isigaliojimo datos
    var valid_to   = []; // pasibaigimo datos

    var w; // laiku lenteles plotis, gausim atkoduodami reisu pradzias
    var h; // laiku lenteles aukstis;

    var times = decoded_data.split(",");
    var i, prev_t;
    var i_max = times.length;

    var zero_ground=[], plus = "+", minus = "-";

    for (i = -1, w = 0, h=0, prev_t = 0; ++i < i_max;) { // atkoduojam reisu pradzios laikus
        var t = times[i];

        if (t == "") { //pasibaige reisu pradzios
            break;
        }

        var tag = t.charAt(0);
        if (tag == plus || (tag == minus && t.charAt(1)=="0")) {
            zero_ground[i] = "1";
        }

        prev_t += +(t);
        timetable[w++] = prev_t;
    }

    for (var j = zero_ground.length; --j >= 0;) {
        if (!zero_ground[j]) {
            zero_ground[j] = "0";
        }
    }

    // atkoduojame isigaliojimo datas
    for (var j = 0; ++i < i_max;) {
        var day = +(times[i]); // skaitom isigaliojimo data
        var k = times[++i]; // kiek kartu pakartoti

        if (k == "") { // pabaiga
            k = w - j; // irasysim reiksme i likusius elementus
            i_max = 0; // kad iseitu is atkodavimo ciklo
        }
        else {
            k = +(k);
        }

        while (k-- > 0) {
            valid_from[j++] = day;
        }
    }

    // atkoduojame pasibaigimo datas
    --i;

    for (var j = 0, i_max = times.length; ++i < i_max;) {
        var day = +(times[i]); // pasibaigimo data
        var k = times[++i]; // kiek kartu pakartoti

        if (k == "") { // pabaiga
            k = w - j; // irasysim reiksme i likusius elementus
            i_max = 0; // kad iseitu is atkodavimo ciklo
        }
        else {
            k = +(k);
        }

        while (k-- > 0) {
            valid_to[j++] = day;
        }
    }

    // atkoduojame savaites dienas
    --i;

    for (var j = 0, i_max = times.length; ++i < i_max;) {
        var weekday = times[i]; // skaitom savaites dienas
        var k = times[++i]; // kiek kartu pakartoti

        if (k == "") { // pabaiga
            k = w - j; // irasysim savaites dienas i likusius elementus
            i_max = 0; // kad iseitu is savaites dienu atkodavimo cikla
        }
        else {
            k = +(k);
        }

        while (k-- > 0) {
            weekdays[j++] = weekday;
        }
    }

    // atkoduojame vaziavimo laikus iki sekancios stoteles kiekviename reise
    --i;
    h = 1;

    for (var j = w, w_left = w, dt = 5, i_max = times.length; ++i < i_max;) {
        dt += +(times[i]) -5; // vaziavimo laikas nuo ankstesnes stoteles
        var k = times[++i]; // kiek reisu tinka tas pats vaziavimo laikas

        if (k != "") { // ne visiems likusiems reisams
            k = +(k);
            w_left -= k; // kiek liko neuzpildytu lenteles elementu eiluteje
        }
        else {
            k = w_left; // vaziavimo laikas tinka visiems likusiems reisams
            w_left = 0;
        }

        while (k-- > 0) {
            timetable[j] = dt + timetable[j-w]; // jei lentele vienmatis masyvas
            ++j;
        }

        if (w_left <= 0) {
            w_left = w; // vel nustatome, kad neuzpildytas visas lenteles plotis sekancioje eiluteje
            dt = 5; // pradinis laiko postumis +5 sekanciai eilutei
            ++h;
        }
    }

    final_data = {workdays:weekdays, times:timetable, tag: zero_ground.join(""), valid_from: valid_from, valid_to: valid_to};

    return final_data;
}

StopsInfo.prototype.getScheduleObj = function(direction_id, valid_from) {
    //console.log("getScheduleObj", direction_id, valid_from);

    var arg = {
        workdays:{},
        direction_schedule:{},
        direction_schedule_list:{},
        valid_from:valid_from
        //current_only: !valid_from
    }


    if ((!(direction_id in this._temp.routes_new)) && this._times_data) {
        var coredata = this.decode(direction_id); // sena versija
        this._temp.routes_new[direction_id] = coredata;
        //delete this._times_data[direction_id];
    }
    else { var coredata = this._temp.routes_new[direction_id]; }

    //var coredata = this.decode(direction_id);

    this.parseRouteData2(arg, direction_id, coredata);
    return arg;
}


StopsInfo.prototype.parseRouteData2 = function(arg, direction_id, data) {
    //if(direction_id == 23477) console.log(data);

    var direction_schedule = {};

    /*1*/ if(arg.direction_schedule_list) { var direction_schedule_list = []; }; // [] <- nes buna kad stotele marsrute kartojasi ir {} jau netinka

    var w = data.workdays.length;
    var h = data.times.length / w;

    var route_data = this.getRouteData(direction_id);
    //var stops = this._routes_data[direction_id].stops;
    var thisday = toDay();//	var thisday = toDay() + 14;

    // arg.valid_from == 1 - reiskia reikia grazinti tik einamaji

    //console.log(direction_id, " arg.valid_from: ", arg.valid_from);

    if(arg.valid_from == 1 || (arg.valid_from && data.valid_from.indexOf(arg.valid_from) == -1)) {
        arg.valid_from == 1;
    }

    //console.log("vv: ", arg.valid_from);


    for (var i=0; i<h; i++) {
        var stop_id = route_data.stops[i];

        if (!(stop_id in direction_schedule)) { direction_schedule[stop_id] = []; }
        /*1*/ if(arg.direction_schedule_list) { var direction_schedule_list_item = []; }

        var index = 0;
        for(var j=i*w; j<i*w+w; j++) {

            if(data.valid_to && data.valid_to[index] !=0 && data.valid_to[index]<thisday) { // pasibaiges reisas index
                index += 1;
                continue;
            }

            if(arg.valid_from) {
                if(arg.valid_from == 2) {
                    if (data.valid_from[index] > thisday + 14) {
                        index += 1;
                        continue;
                    }
                }
                else if(arg.valid_from != 1 && arg.valid_from == route_data.valid_from0 && data.valid_from[index]<thisday) {
                    // jeigu shalutinis tvarkarastis jau isigaliojo anksciau uzh dabartini valid_from0 - tai ji reikia rodyti
                }
                else if((arg.valid_from != 1 && data.valid_from[index] != arg.valid_from) ||
                    (arg.valid_from == 1 && data.valid_from[index] > thisday)) {
                    index += 1;
                    //console.log("filter");
                    continue;
                }
            }

            var time = classic_time_item(data.times[j]);

            var cfg = {race_index:index, time:time, workdays:data.workdays[index]};

            if(arg.valid_from == 2) {
                cfg.valid_from = data.valid_from[index];
                cfg.valid_to = data.valid_to[index];
                //console.log("setting: ", cfg.valid_from, cfg.valid_to);
                if(cfg.valid_from && cfg.valid_from < thisday) cfg.valid_from = 0;
                if(cfg.valid_to && cfg.valid_to > (thisday + 14)) cfg.valid_to = 0;
            }
            //else {
            //   console.log("wtf!");
            //}

            if(data.tag.charAt(index) == "1") { cfg.ground = true; }

            if(i == (h-1)) { // paskutine stotele
                cfg.end = true;
            }
            else if(data.times[(h-1)*w+index]==-1 && data.times[(i+1)*w+index]==-1) {
                // jei sekancioje ir paskutineje stotele nera stojama - nusprendziame kad si yra paskutine (sutrumpintas marsrutas)
                cfg.end = true;
            }

            index += 1;

            direction_schedule[stop_id].push(cfg);

            /*1*/if(arg.direction_schedule_list) { direction_schedule_list_item.push(cfg); }
        }
        direction_schedule_list.push(direction_schedule_list_item);
    }


    if(arg.valid_from) {
        var new_workdays = [];
        for(var i=0; i<data.valid_from.length;i++) {
            //if(arg.valid_from != 1 && arg.valid_from == route_data.valid_from0 && data.valid_from[index]<thisday) {
            // jeigu shalutinis tvarkarastis jau isigaliojo anksciau uzh dabartini valid_from0
            //}
            if(arg.valid_from == 2) {

            }
            else if((arg.valid_from != 1 && data.valid_from[i] == arg.valid_from) ||
                (arg.valid_from != 1 && arg.valid_from == route_data.valid_from0 && data.valid_from[i] < thisday) ||
                (arg.valid_from == 1 && data.valid_from[i] <= thisday)) {
                new_workdays.push(data.workdays[i]);
            }
        }
        arg.workdays[direction_id] = new_workdays;
    }
    else {
        arg.workdays[direction_id] = data.workdays;
    }

    arg.direction_schedule[direction_id] = direction_schedule;

    /*1*/if(arg.direction_schedule_list) { arg.direction_schedule_list[direction_id] = direction_schedule_list; }

    // TODO problema tame kad ash dabar negaliu atskirti ar laikas stoteleje kurioje stoja du kartus yra ankstesnis ar velesnis - t.y. ar tai paskutine stotele ar ne


    if ("stop_id" in arg) {
        // schedules = {"1-5": { 23:[ {race_index:7, min: 15},  {race_index:5, min: 23}]}, }
        var stop_schedule = direction_schedule[arg.stop_id];

        for(var i=0; i<stop_schedule.length; i++) {
            var wd = stop_schedule[i].workdays;
            var hour = stop_schedule[i].time.hour;
            var minute = stop_schedule[i].time.minute;

            var cfg = {direction_id: direction_id, stop_id:arg.stop_id, race_index:stop_schedule[i].race_index, minute: minute, days:wd};
            if(arg.valid_from == 2) {
                cfg.valid_from = stop_schedule[i].valid_from;
                cfg.valid_to =  stop_schedule[i].valid_to;
            }

            if("ground" in stop_schedule[i]) {
                cfg.ground = true;
            }

            if("end" in stop_schedule[i]) {
                cfg.end = true;
            }

            // dabar reikia patikrinti kokioms dienoms is [12345,6,7] tinka shitas tvarkarashtis
            // uzhpildome {1,2,3,4,5,6,7} masyva
            for (var days in arg.stop_schedule_byhour) {
                if (wd.indexOf(days)!=-1) {
                    if (!(hour in arg.stop_schedule_byhour[days])) { arg.stop_schedule_byhour[days][hour]=[]; }
                    arg.stop_schedule_byhour[days][hour].push(cfg);
                }
            }

            // uzhpildome {12345, 6,7, 67 ir pan}

            if (!(wd in arg.stop_schedule_byhour_all)) { arg.stop_schedule_byhour_all[wd]={}; }
            if (!(hour in arg.stop_schedule_byhour_all[wd])) { arg.stop_schedule_byhour_all[wd][hour]=[]; }
            arg.stop_schedule_byhour_all[wd][hour].push(cfg);
        }


        // --  Sitie SORTAI cia tik stabdo  - juos reikia atlikineti ten kur reikia! -- Estu versijoje reiketu pasalinti
        //for(var wd in arg.stop_schedule_byhour) {
        //     for(var hour in arg.stop_schedule_byhour[wd]) {
        //         arg.stop_schedule_byhour[wd][hour].sort(function(x,y) {return x.minute - y.minute});
        //     }
        //}

        //for(var wd in arg.stop_schedule_byhour_all) {
        //     for(var hour in arg.stop_schedule_byhour_all[wd]) {
        //         arg.stop_schedule_byhour_all[wd][hour].sort(function(x,y) {return x.minute - y.minute});
        //     }
        //}
    }
}


StopsInfo.prototype.getRouteData = function(direction_id) {
    return this._routes_data[direction_id];
}

StopsInfo.prototype.setStopData = function(cfg) {
    if(!(cfg.id in this._stops_data)) {
        this._stops_data[cfg.id] = cfg;
    }
    else {
        for(var key in cfg) {
            this._stops_data[cfg.id][key] = cfg[key];
        }
    }

    /* //storing stop data to string
     var s = this._stops_data[cfg.id];

     if (s) {
     s = s.split(";");

     if (cfg.neighbours) {
     s[this.fld_stops] = cfg.neighbours.join(",");
     }
     if (cfg.name) {
     s[this.fld_name] = cfg.name;
     }
     if (cfg.ascii) {
     s[this.fld_ascii] = cfg.ascii;
     }
     this._stops_data[cfg.id] = s.join(";")
     }
     else {
     this._stops_data[cfg.id] = [cfg.id, "", cfg.area, cfg.street, cfg.name, cfg.lng*100000, cfg.lat*100000, cfg.neighbours.join(","), cfg.ascii, "", ""].join(";");
     }
     */
}


StopsInfo.prototype.getStopData = function(stop_id) {
    if(stop_id in this._stops_data) return this._stops_data[stop_id];
    else if(stop_id.charAt(0) == "@") {
        var c = stop_id.substr(1).split(":").map(function (x) {return +x});
        this.createStopFromPoint(c[0], c[1]);
    }
    return this._stops_data[stop_id];

    var s = this._stops_data[stop_id];

    if (!s) {
        return null;
    }
    s = s.split(";");

    var stop_data = {
        id: stop_id
        ,       area: s[this.fld_area]
        ,     street: s[this.fld_street]
        ,       name: s[this.fld_name]
        ,      ascii: s[this.fld_ascii]
        , neighbours: (s[this.fld_stops]) ? s[this.fld_stops].split(",") : []
        ,        lat: (+s[this.fld_lat]) / 100000
        ,        lng: (+s[this.fld_lng]) / 100000
    };

    var timediff;
    if (timediff = s[this.fld_timediff]) {
        stop_data.timediff = +timediff;
    }

    var indir2;
    if (indir2 = s[this.fld_indir2].split(",")) {
        indir2.pop(); // dedant routes buvo galutinis kablelis

        for (var i=1, imax = indir2.length; i < imax; i+=2) {
            indir2[i] = +(indir2[i]);
        }
        stop_data.indir2 = indir2;
    }
    else {
        stop_data.indir2 = [];
    }

    if (("" + stop_id).charAt(0) == "A") {
        stop_data.is_alias = true;
    }

    return stop_data;
}

// not used anymore
StopsInfo.prototype.getStopTransport = function(stop_id, transport_type, route_nmb) {
    var stops_data = this.getStopData(stop_id);
    var directions = [];
    //for(var direction_id in stops_data.indir2) {
    for (var i=0, imax = stops_data.indir2.length; i < imax; i+=2) {
        var direction_id = stops_data.indir2[i];
        var route_data = this.getRouteData(direction_id);

        if((route_data.transport == transport_type) && (route_data.route_nmb == route_nmb)) {
            directions.push(direction_id);
        }
    }
    return directions;
}

StopsInfo.prototype.getPointNeighbours = function(lat, lng) {
    var maxwalk = 5.0;
    if(maxwalk_el = document.getElementById("search_maxwalk_input")) {
        maxwalk = parseFloat(maxwalk_el.value, 10);
    }
    //maxwalk = maxwalk/1000.0;

    var stops = [];
    for(var id in this._stops_data) {
        var stop_data = this.getStopData(id);
        var name = stop_data.name;
        var fchar = id.charAt(0);
        if(fchar == "@" || fchar == "A") continue;

        //console.log(typeof point1.lat, typeof point2.lat);
        var distance = distPythagor(lat, lng, stop_data.lat, stop_data.lng);
        //var distance = point1.distanceFrom(point2);
        //console.log("distance: ", distance);
        if(distance > maxwalk) continue;
        stops.push(id);
    }


    return stops;
}

StopsInfo.prototype.load = function(arg, onload) {
    var si = this;

    CDownloadUrl("get", this.stops_file, function (doc) {
        //var d1 = new Date();

        var lines = doc.split("\n"); // split the document into lines
        si._stops_data = {};

        var city = "";
        var area = "0";
        var street = "0";
        var name = "";
        var ascii = "";

        var key_list = (lines[0] + ";ascii;timediff;indir2").toLowerCase().split(';');
        //ID;Area;Street;Name;Lng;Lat

        for (var i = key_list.length; --i >= 0;) {
            si["fld_" + key_list[i].trim()] = i;
        }

        var alias_ready = false; // nuskaiteme visus aliasus

//		for (var i=lines.length; --i >= 0;) {
        for (var i=1, imax = lines.length; i < imax; i++) {
            if (lines[i].length > 1) {
                var parts = lines[i].split(';'); // supjaustome eilute i reiksmes

                var id = parts[si["fld_id"]]; //parts[0];
                var n  = parts[si["fld_name"]];

                if (n != "") {
                    name = n.replace(/~/g, ' ');
                    ascii = toascii(n);
                }

                if ((n = parts[si["fld_area"]]) != "") {
                    area = n;
                }
                if ((n = parts[si["fld_street"]]) != "") {
                    street = n;
                }

                //si._stops_data[id] = [id, "", area, street, name, parts[si["fld_lng"]], parts[si["fld_lat"]], parts[si["fld_stops"]], ascii, "", ""].join(";")
                //continue;

                var neighbours = (si["fld_stops"]) ? parts[si["fld_stops"]].trim() : null;

                var cfg = {
                    id: id
                    ,       name: name
                    ,      ascii: ascii
                    ,       area: area
                    ,     street: street
                    , neighbours: []
                    ,        lat: +parts[si["fld_lat"]] / 100000
                    ,        lng: +parts[si["fld_lng"]] / 100000
                    ,     indir2: []
                };

                if(neighbours) {cfg.neighbours = neighbours.split(",")};
                si._stops_data[id] = cfg;


                if(!alias_ready && (id.charAt(0) == "A")) {
                    si._stops_data[id].is_alias = true;

                }
                else if(!alias_ready) {
                    alias_ready = true;
                }
            }
        }
        console.log(si._stops_data);
        for (var i=-12; i<=12; i++) {
            var tz = si.getStopData("TimeZone" + i);

            if (tz) {
                if (tz.neighbours) {
                    for (var j = tz.neighbours.length; --j >= 0;) {
                        var s = si._stops_data[tz.neighbours[j]];

                        if (s) {
                            s.timediff = i;
                            //s = s.split(";")
                            //s[si.fld_timediff] = i;
                            //si._stops_data[tz.neighbours[j]] = s.join(";");
                        }
                    }
                }
                delete si._stops_data["TimeZone" + i];
            }
        }

        //console.log(si._stops_data);

        //var d2 = new Date();
        //alert(d2.getTime() - d1.getTime());


        CDownloadUrl("get", si.routes_file, function (doc) {
            //console.log("toay!", doc);
            var today = toDay();//            var today = toDay()+14;

            var lines = doc.split("\n"); // split the document into lines
            si._routes_data = {};
            si._transport_data = {};
            si._special_dates = {};
            var stops_data = si._stops_data;

            var city = "";
            var transport = "";
            var operator = "";
            var route_nmb = "";
            var commercial = "";
            var authority = "";
            var direction_id = ""
            var datestart = ""
            var special_dates = [];
            var comments = false;

            var key_list = lines[0].split(';');
            var key_index_map = {};
            for (var i=0; i<key_list.length; i++) {
                key_index_map[key_list[i].trim()] = i;
            }

            //console.log("lines: ", lines)

            for (var i=1; i<lines.length; i++) {
                if (lines[i].charAt(0) == "#") {
                    var parts = lines[i].split('#');
                    var date_from = null;
                    var date_to = null;
                    var curr_date = new Date();

                    if (parts[1] != "") {
                        date_from = new Date(parts[1]);
                    }
                    if (parts[2] != "") {
                        date_to = new Date(parts[2]);
                    }

                    if ((!date_from || date_from <= curr_date)&& (!date_to || date_to >= curr_date) )
                    {

                        var route = si._routes_data[direction_id];

                        if (!route.comments) {
                            route.comments = [];
                        }
                        route.comments.push({comment: parts[3]});
                        comments = route.comments;
                    }


                }
                else if (lines[i].length > 1) {
                    var parts = lines[i].split(';'); // supjaustome eilutę

                    authority = parts[key_index_map["Authority"]] || authority;
                    route_nmb = parts[key_index_map["RouteNum"]] || route_nmb;
                    datestart = parts[key_index_map["Datestart"]] || datestart;


                    if (authority == "SpecialDates") {
                        var dates_collection = {};

                        var dates = parts[key_index_map["ValidityPeriods"]].split(",");
                        var diff_d = 0, day = 0;

                        for (var i_dates = -1, dates_length = dates.length; ++i_dates < dates_length;) {
                            if (dates[i_dates]) {
                                diff_d = +(dates[i_dates]);
                            }

                            day += diff_d;
                            dates_collection[day] = true;
                        }

                        si._special_dates[parts[key_index_map["RouteNum"]]] = dates_collection;
                        continue;
                    }

                    //console.log("toay2!");


                    city = parts[key_index_map["City"]].toLowerCase() || city;
                    var cities = city.split(",");

                    if((cities.length == 1) && (!(cities[0] in pc.content))) continue; // pvz:. MKM

                    transport = parts[key_index_map["Transport"]].toLowerCase() || transport;

                    if(!transport) transport = "regionalbus";

                    // Miestuose rodom tik mietu transporta, regionuose tik regionu
                    //var is_region_transport = _transport_data[transport].report;
                    var is_region_transport = _transport_data[transport].region;

                    for(var j=0;j<cities.length;j++) {
                        var jcity = cities[j];
                        //var is_region = (jcity in _menu_list);
                        var is_region = (jcity in _menu_list); //(jcity in pc.content) ? pc.content[jcity].region : false;

                        if((is_region_transport && is_region) || (!is_region_transport && !is_region)) {
                            if(!(jcity in pc.content)) pc.content[jcity] = {};
                            if(!("transport" in pc.content[jcity])) pc.content[jcity].transport = {};
                            pc.content[jcity].transport[transport] = true;
                        }
                    }

                    operator = parts[key_index_map["Operator"]] || operator;
                    route_type = parts[key_index_map["RouteType"]];

                    commercial = parts[key_index_map["Commercial"]] || "K"; //commercial;

                    //if(commercial != "A") console.log("commercial: ", commercial);
                    //console.log("commercial: ", commercial);

                    direction_id = parts[key_index_map["RouteID"]];
                    var direction_name = format_dirname(parts[key_index_map["RouteName"]]);
                    var weekdays = parts[key_index_map["Weekdays"]];
                    var route_tag = parts[key_index_map["RouteTag"]];

                    var stops = parts[key_index_map["RouteStops"]].trim().split(",");

                    //console.log(authority, cities[0]);

                    /* surenkame duomenis stotelems */
                    var entry = false;

                    for (var j = stops.length; --j >= 0;) {
                        var stop_id = stops[j];

                        if (stop_id.charAt(0) == "e") {
                            if (!entry) entry = [];
                            entry[j] = "1";
                            stop_id = stop_id.substring(1);
                            stops[j] = stop_id;
                        }
                        else if (stop_id.charAt(0) == "x") {
                            if (!entry) entry = [];
                            entry[j] = "2";
                            stop_id = stop_id.substring(1);
                            stops[j] = stop_id;
                        }
                        else if (entry) {
                            entry[j] = "0";
                        }

                        var stop;
                        if ((stop = stops_data[stop_id])) {
                            stop.indir2.push(direction_id);
                            stop.indir2.push(j);
                        }
                        else { // stop used in route not found
                            // add stop for preventing crashing
                            si.setStopData({name:stops[j], area:"0", street:"0", ascii:stops[j], lat:0, lng:0, id:stop_id, indir2:[], neighbours:[]});
                        }
                        /*
                         var id = stops[j];

                         var routes_at_stop = si._stops_data[id].indir2;

                         routes_at_stop.push(direction_id);
                         routes_at_stop.push(j);
                         */

                        /*
                         if(route_tag == "0" || route_tag == "-") {
                         si._stops_data[id].temp[transport][route_nmb].hide = true; // naudosime vietoje transport_byroute_tag
                         // bet gal galimi variantai, kai vienam direction_id jis turi galioti o kitam jau ne?
                         }*/
                    }

                    var parts_special_dates = parts[key_index_map["SpecialDates"]];

                    if (parts_special_dates) {
                        special_dates = (parts_special_dates == "0") ? [] : parts_special_dates.split(",");

                        for (var i_special_dates = 0; i_special_dates < special_dates.length; i_special_dates += 2) {
                            special_dates[i_special_dates] = si._special_dates[special_dates[i_special_dates]];
                        }
                    }

                    si._routes_data[direction_id] = {
                        id            : direction_id
                        , authority     : authority
                        , commercial    : commercial
                        , cities        : cities
                        , city          : city  // TODO: naikinti city: atributa ir palikti tik cities
                        , transport     : transport
                        , operator      : operator
                        , weekdays      : weekdays
                        , route_type    : route_type
                        , route_tag     : route_tag
                        , route_nmb     : route_nmb
                        , direction_name: direction_name
                        , stops         : stops
                        , entry         : (entry) ? entry.join("") : ""
                        , special_dates : special_dates
                        , datestart : datestart
                    };

                    if (comments) {
                        if (parts[key_index_map["RouteNum"]] || parts[key_index_map["Transport"]] || parts[key_index_map["Authority"]]) {
                            comments = false; // new route - current comments should be reset
                        }
                        else {
                            si._routes_data[direction_id].comments = comments;
                        }
                    }

                    if (transport == "distantbus" && route_nmb == "17") {
                        k = k;
                    }

                    var valid_to = parts[key_index_map["ValidityPeriods"]];

                    if (valid_to) {
                        var route = si._routes_data[direction_id];
                        valid_to = valid_to.split(",");

                        for (var k = 0, days = 0; k < valid_to.length; k++) {
                            if (valid_to[k]) {
                                days += +(valid_to[k]);
                                valid_to[k] = days;
                            }
                        }

                        for (var k = 1, kk=0; k < valid_to.length; k+=2) {

                            var valid_from = valid_to[k-1] || 0;

                            if (valid_from > today + 14 && kk > 0) { // too distant schedules
                                break;
                            }

                            if (valid_from <= today && (!valid_to[k] || valid_to[k] > valid_from + 14))

                            { // validity period is already started and not short term
                                kk = 0;
                            }

                            if (valid_to[k]) {
                                if (valid_to[k] < today) { // validity period is expired
                                    continue;
                                }
                                if (valid_to[k] <= today + 14) {
                                    route["valid_to" + kk] = valid_to[k];
                                }
                            }

                            route["valid_from" + kk] = valid_from;
                            ++kk;
                        }
                    }

                    //console.log("???");
                    //fill si._transport_data START
                    if (!(transport in si._transport_data)) { si._transport_data[transport] = {}}
                    if (!(route_nmb in si._transport_data[transport])) {si._transport_data[transport][route_nmb] = []}

                    si._transport_data[transport][route_nmb].push(direction_id);
                    /*TODO
                     Pagal sita galima greiciau atvaizduoti marsrutus sarashuose #city ir #region! - ner prasmes nes as iskarto suformuoju visu transporto tipu rezultatus
                     */

                    // END
                }
            }

            //console.log("ssomething");

            si.ready = true;
            //onload(si, arg);
            //onload(arg);
            //console.log(pc._current_page_name);

            pages[pc._current_page_name].ondataload(arg);
            //pt_map_banner();
            //alert(pc._current_page_name);
        });
    });
}

//var results_html = "";
//function showResults() { pc.show_left_content(results_html); }
//var counter = 0;

StopsInfo.prototype.join_stops_data = function(stopsinfo, asciiname_long, data, indexOf) {

    if (!(asciiname_long in stopsinfo)) {
        stopsinfo[asciiname_long] = {area: data.is_alias ? "0" : data.area, street:data.street, ascii: data.ascii, name: data.name, indexOf: indexOf, lat: data.lat, lng: data.lng, id: data.id, stops:[]};
    }
    else if(stopsinfo[asciiname_long].street != data.street) {
        stopsinfo[asciiname_long].street = "0";
    }

    if(data.is_alias) {
        stopsinfo[asciiname_long].is_alias = true;
        stopsinfo[asciiname_long].stops = data.neighbours; //.split(",");
    }
    else {
        stopsinfo[asciiname_long].stops.push(data.id);
    }

}


StopsInfo.prototype.createStopFromPoint = function(lat, lng) {
    var id = "@" + lat + ":" + lng;
    var si = this;

    var neighbours = this.getPointNeighbours(lat, lng);
    if(!(id in this._stops_data)) {
        this.setStopData({name: pc.i18n("lngPointFromMap"), area:"0", street:"0", ascii:"", lat:lat, lng:lng, id:id, indir2:[], neighbours:neighbours});

        $.getJSON(_url_reversegeocode.replace("%lat", lat).replace("%lng", lng), function(data){
            if(data && data.length && data[0].lahiAadress) {

                var name = data[0].lahiAadress;
                var area = (data[0].tasemed && data[0].tasemed.omavalitsus) ? data[0].tasemed.omavalitsus : "0"

                si.setStopData({id:id, name:name, area:area, ascii:toascii(name)});
                pt_direct_routes_stops(); // atnaujinsime reiksmes ivedimo laukuose
            }
        });
    }
    else {
        this.setStopData({id:id, neighbours:neighbours});
        //this._stops_data[id].neighbours = neighbours;
    }


    for(var i=0;i<neighbours.length;i++) {
        var old_neighbours = this.getStopData(neighbours[i]).neighbours;
        old_neighbours.push(id);
        this.setStopData({id:neighbours[i], neighbours:old_neighbours});
    }
}


StopsInfo.prototype.searchStops = function(substr, stops_filtered, skip) {
    var stopsinfo = {};

    var substr_ascii = toascii(substr);
    var substr_lower = substr.toLowerCase();
    var separators = " -.\"()";

    if(!stops_filtered) stops_filtered = this._stops_data;
    //console.log(sizeOf(stops_data), sizeOf(stops_filtered));

    //alert(stops_filtered);

    for (var id in stops_filtered) {
        if (id.charAt(0) == "@") {
            continue;
        }

        var stop_data = this._stops_data[id];
        var name = stop_data.name;
        var asciiname = stop_data.ascii;

        var indexOf = asciiname.indexOf(substr_ascii);

        //if(skip) console.log(asciiname + ":" + indexOf + ":" + substr_ascii);

        if (skip || ((indexOf != -1) && ((indexOf == 0) || (separators.indexOf(asciiname.charAt(indexOf-1)) >= 0)) // radome substring
            && (substr_ascii == substr_lower || name.toLowerCase().indexOf(substr_lower) >= 0))) { // jei ivesta nesveplu raidziu, patikriname su originaliu pavadinimu

            var data = this.getStopData(id);
            var area = "," + data.area;
            var street = "," + data.street;
            var asciiname_long = asciiname + (area != ",0" ? area : ""); // + (street != ",0" ? street : "");

            if(data.is_alias) {
                asciiname_long = "alias_" + asciiname_long;
            }

            this.join_stops_data(stopsinfo, asciiname_long, data, indexOf);
        }
    }
    return stopsinfo;
}

/*
 Prioritetai poros parinkimiui A>B
 1. randame B>A
 2. randame B?>A
 3. randame B?>A?
 4. randame B?>
 5. randame >A?
 6. paliekame be poros
 */
StopsInfo.prototype.find_opposite = function(direction_id_arg, directions_types) {
    var main = directions_types[direction_id_arg].type;
    //console.log(main , directions_types, direction_id_arg);
    var main_split = main.split(">");
    var main_from = main_split[0];
    var main_to = main_split[main_split.length-1];

    var opposite = main_split.slice(0, main_split.length).reverse().join(">");

    //console.log("main: ", main, " opposite: ", opposite);

    var main_weekdays = this._routes_data[direction_id_arg].weekdays;
    //var main_city = this._routes_data[direction_id_arg].city;

    var found_1 = false;
    var found_2 = false;
    var found_3 = false;
    var found_4 = false;
    var found_5 = false;

    for(var direction_id in directions_types) {
        if (direction_id == direction_id_arg) continue;

        //var direction_city = this._routes_data[direction_id].city;
        //console.log(main_city, direction_city);
        //if (main_city != direction_city) continue;

        if(directions_types[direction_id].type == opposite) {
            found_1 = direction_id;
            break;
        }

        var other_split = directions_types[direction_id].type.split(">");
        var other_from = other_split[0];
        var other_to = other_split[other_split.length-1];
        var other_weekdays = this._routes_data[direction_id].weekdays;

        //console.log("good! ", other_weekdays, main_weekdays);

        if (other_weekdays!=main_weekdays) continue;


        if(!found_2 && (other_to == main_from) && (other_from.charAt(0) == main_to.charAt(0))) {
            found_2 = direction_id;
        }
        else if (!found_2 && (other_to.charAt(0) == main_from.charAt(0)) && (other_from.charAt(0) == main_to.charAt(0))) {
            found_3 = direction_id;
        }
        else if (!found_2 && !found_3 && (other_to.charAt(0) == main_from.charAt(0))) {
            found_4 = direction_id;
        }
        else if (!found_2 && !found_3 && !found_4 && (other_from.charAt(0) == main_to.charAt(0))) {
            found_5 = direction_id;
        }
    }

    return found_1 || found_2 || found_3 || found_4 || found_5;
}

// returns {main:direction_id, main_opposite:direction_id, similar:[], opposite:[]};
StopsInfo.prototype.get_directions = function(direction_id_arg) {
    //console.log(direction_id_arg);
    //var transport = this._routes_data[direction_id_arg].transport;
    //var route_nmb = this._routes_data[direction_id_arg].route_nmb;

    var directions_types = this.get_directiontypes(direction_id_arg);
    var main_direction_id = this.get_maindirection(directions_types) || direction_id_arg;
    var main_city = this._routes_data[main_direction_id].city;

    var rvalue = {main: main_direction_id};

    //console.log("get_directions: ", main_direction_id, directions_types);
    var found = this.find_opposite(main_direction_id, directions_types);

    //var two_directions = [main_direction_id]; // A>B
    if (found) {
        rvalue.main_opposite = found;
        //two_directions.push(found); // ir B>A
    }

    rvalue.similar = [];
    rvalue.opposite = [];

    // Jeigu pasirinkta url'e pagrindine B>A kryptis tai (similar ir opposite) rodome atsizhvelgiant i ja, o visais kitais atvejais naudojame A>B krypti.
    var main = directions_types[(found && (found == direction_id_arg))?found:main_direction_id];
    var main_split = main.type.split(">");

    for(var direction_id in directions_types) {
        if ((direction_id == main_direction_id) || (direction_id == found)) continue;
        if (found && (direction_id == found)) continue;

        var direction_city = this._routes_data[direction_id].city;
        if (main_city != direction_city) { // kryptis ish kitokio miesto (tame regione).
            rvalue.opposite.push(direction_id);
            continue;
        }

        var other_split = directions_types[direction_id].type.split(">");
        if (isSimilar(main_split, other_split)) {
            rvalue.similar.push(direction_id);
        }
        else {
            rvalue.opposite.push(direction_id);
        }
    }
    return rvalue;
}

//StopsInfo.prototype.get_directiontypes = function(transport, route_nmb_arg) {
StopsInfo.prototype.get_directiontypes = function(direction_id) {
    var transport = this._routes_data[direction_id].transport;
    var route_nmb = this._routes_data[direction_id].route_nmb;
    var cities = this._routes_data[direction_id].cities;

    //console.log(cities);

    var directions = this._transport_data[transport][route_nmb];
    //console.log("dir: ", directions);

    var directions_types = {};
    for(var i=0; i<directions.length;i++) {
        var rcities = this._routes_data[directions[i]].cities;
        var rname = this._routes_data[directions[i]].direction_name;
        var rtype = this._routes_data[directions[i]].route_type;


        //console.log(rcities);
        if(pc._current_page_name == "city" || pc._current_page_name == "region") {
            var different_cities = (cities[0] != rcities[0]);
            //var different_cities = (rcities.indexOf(pc.city_transport.city) != -1) ? false: true;
        }
        else {
            var different_cities = true;
            for(var j=0; j<cities.length; j++) { // sankirta
                for(var k=0; k<rcities.length; k++) {
                    if(cities[j] == rcities[k]) {
                        different_cities = false;
                        break;
                    }
                }
            }
        }


        //if(rcities.indexOf(pc.city) == -1) { // negerai, nes search atveju nera to pc.city teisingo
        if(different_cities) {
            //console.log("skip: ", rcities, ", type:", rtype, ", name:", rname);
            continue;
        }

        //console.log("cities: ", rcities, ", type:", rtype, ", name:", rname);
        directions_types[directions[i]] = {type:rtype, name:rname};
    }

    return directions_types;
}

StopsInfo.prototype.get_maindirections = function(direction_id) {
    //var transport = this._routes_data[direction_id].transport;
    //var route_nmb = this._routes_data[direction_id].route_nmb;

    var direction_types = this.get_directiontypes(direction_id);


    var main = this.get_maindirection(direction_types);
    var main_back = this.find_opposite(main, direction_types);

    var rvalue = {};
    //rvalue[main] = this._routes_data[main].route_type;
    rvalue[this._routes_data[main].route_type] = main;

    if(main_back) {
        rvalue[this._routes_data[main_back].route_type] = main_back;
    }

    return rvalue;
}

StopsInfo.prototype.getUrlConfig = function(cfg) {
    var transport = this._routes_data[cfg.direction_id].transport;
    var args = [transport, cfg.direction_id];
    if(cfg.stop_id) {
        args.push(cfg.stop_id);
    }
    return {city: this.city, page:"schedule", args:args};
}


StopsInfo.prototype.get_maindirection_by_id = function(direction_id) {
    //var transport = this._routes_data[direction_id].transport;
    //var route_nmb = this._routes_data[direction_id].route_nmb;
    var direction_types = this.get_directiontypes(direction_id);
    return this.get_maindirection(direction_types);
}


StopsInfo.prototype.get_maindirection = function(directions_types) {
    var found_1 = false;
    var found_2 = false;

    var first_direction_id = null; // tokiu atveju jei neradomk nei A>A, nei A>B ar pan - tai imame pirma pasitaikiusia.


    for (var direction_id in directions_types) {
        if (!first_direction_id) {first_direction_id = direction_id;}

        var type = directions_types[direction_id].type;
        if(type == "A>A" || type == "A>B") {
            found_1 = direction_id;
            break;
        }
        else {
            var type_split = type.split(">");
            var type_from = type_split[0];
            var type_to = type_split[type_split.length-1];
            if((type_from == "A" && type_to == "A") || (type_from == "A" && type_to == "B")) {
                found_2 = direction_id;
            }
        }
    }

    return found_1 || found_2 || first_direction_id;
}


function init_with_default_data(city) {
    CDownloadUrl("get", (_data_dir || (city?(pc.content[city].data):"harju/")) + "routes.txt", function (doc) {
        var lines = doc.split("\n"); // split the document into lines

        var city = "";
        var transport = "";

        var key_list = lines[0].split(';');
        var key_index_map = {};
        for (var i=0; i<key_list.length; i++) {
            key_index_map[key_list[i].trim()] = i;
        }

        var parts = lines[1].split(';'); // supjaustome eilutę
        //var city = parts[key_index_map["City"]].toLowerCase();
        //var cities = parts[key_index_map["City"]].toLowerCase().split(",");
        var transport = parts[key_index_map["Transport"]].toLowerCase();

        //pc.init(pc.defaults.city || city, pc.defaults.transport || transport);
        pc.init(pc.defaults.city, pc.defaults.transport || transport);
    });
}
