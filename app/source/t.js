
showCityMenu = function(obj,event) {
    var ils = ["<ul class='menu'>"];

    var show_transport = ((typeof(_menu_showcitytransport) == "undefined") || (_menu_showcitytransport == true));

    for(var city in _menu_list) {
        var first_transport = null;

        //console.log("city: ", city);
        trans = pc.content[city].transport; //pc.menu[city];

        var tils = [];

        //for(var transport in trans) {
        for(var transport in _transport_data) {
            if(!(transport in trans)) continue;

            if(!(first_transport)) {
                first_transport = {city:city, page:"schedule", args:[transport]};
                if(!show_transport) break;
            }
            var color = _transport_data[transport].color;
            var title = _transport_data[transport].title;
            // {city: "valga", page:"schedule", args:["regionalbus"]},
            var url = pc.url({city: city, page:"schedule", args:[transport]});

            tils.push(["<a style='color:", color, "' onclick=\"hidetip();\" href=\"", url, "\">", pc.i18n(title), "</a>"].join(""));
        }



        var bils = []
        for(var i=0; i<_menu_list[city].length; i++) {

            var ifirst_transport = null;
            var icity = _menu_list[city][i];

            //console.log(icity, pc.content[icity].transport);
            itrans = pc.content[icity].transport;

            if(!itrans) continue;

            //var itils = ["<ul>"];
            var itils = [];

            for(var transport in _transport_data) {
                if(!(transport in itrans)) continue;

                if(!(ifirst_transport)) {
                    ifirst_transport = {city:icity, page:"schedule", args:[transport]};
                    if(!show_transport) break;
                }
                var icolor = _transport_data[transport].color;
                var ititle = _transport_data[transport].title;
                // {city: "valga", page:"schedule", args:["regionalbus"]},
                var iurl = pc.url({city: icity, page:"schedule", args:[transport]});

                //itils.push("<li><a style='color:" + icolor + "' onclick=\"hidetip();\" href=\"" + pc.url(itrans[transport]) + "\">" + pc.i18n(_transport_types[transport].title) + "</a></li>");
                itils.push(["<a style='color:", icolor, "' onclick=\"hidetip();\" href=\"", iurl, "\">", pc.i18n(ititle), "</a>"].join(""));
            }

            //itils.push("</ul>");
            if(show_transport) bils.push("<li>");
            bils.push(["<span style='font-weight:bold;font-size:10pt;'><a onclick=\"hidetip();\" href=\"", pc.url(ifirst_transport), "\"><span class='simple_normal'>", pc.i18n(pc.content[_menu_list[city][i]].title), "</span></a>", ((show_transport)?": ":""), "<span class='simple'>", itils.join(", "), "</span></span>"].join(""));
            if(show_transport) bils.push("</li>");

        }

//		ils.push(["<li><span style='font-weight:bold;font-size:10pt;'><a onclick=\"hidetip();\" href=\"", pc.url(first_transport), "\"><span class='simple_bold'>", pc.i18n(pc.content[city].title), "</span></a>", ((!show_transport && !(_menu_list[city].length))?"":": "), "<span class='simple'>", tils.join(", "), "</span></span>"].join(""));
        ils.push(["<li><span style='font-weight:bold;font-size:10pt;'><a onclick=\"hidetip();\" href=\"", pc.url(first_transport), "\"><span class='simple_bold'>", pc.i18n(pc.content[city].title), "</span></a>", ((!show_transport && !(bils.length))?"":": "), "<span class='simple'>", tils.join(", "), "</span></span>"].join(""));

        //if (_menu_list[city].length && show_transport) ils.push("<ul>");
        if (bils.length && show_transport) ils.push("<ul>");
        ils.push(bils.join(!show_transport?", ":""));
        //if (_menu_list[city].length && show_transport) ils.push("</ul>");
        if (bils.length && show_transport) ils.push("</ul>");
        ils.push("</il>");

    }
    ils.push("</ul>");

    var html = ils.join("");
    fixedtooltip({width:300, html:html, obj:obj, event:event, offset:[0,1]}, 0);
}




showTransportMenu = function(obj,event) {
    var trans = pc.content[pc.city].transport;

    var ils = ["<span class='simple'><ul class='menu'>"];
    for(var transport in trans) {
        var color = trans[transport].color;
        var title = trans[transport].title;
        // {city: "valga", page:"schedule", args:["regionalbus"]},
        var url = pc.url({city: pc.city, page:"schedule", args:[transport]});
        ils.push(["<li><a style='font-family:arial;font-weight:bold;font-size:12pt;color:",color,"' onclick=\"hidetip();\" href=\"", url , "\">", pc.i18n(title), "</a></li>"].join(""));
    }
    //style='font-weight:bold;font-size:10pt;'

    ils.push("</ul></span>");

    var html = ils.join("");
    fixedtooltip({width:250, html:html, obj:obj, event:event, offset:[0,1]}, 0);
}

showDirectRouteInfo = function(cfg) {
    var html = ["<table>"];

    function stop_row(id, img) {
        var stop_data = pc.getSI().getStopData(id);
        var area_street = [];
        if (stop_data.area != "0") area_street.push(stop_data.area);
        if (stop_data.street != "0") area_street.push(stop_data.street);
        html.push(["<tr><td><img src='", img, "'/></td><td><b>", stop_data.name, "</b><br/>", (area_street.length?("<span style='font-size:10px;'>(" + area_street.join(", ") + ")</span><br/>"):""), "</td></tr>"].join(""));
    }

    if("start" in pc._temp.direct_routes) {
        html.push(stop_row(pc._temp.direct_routes.start[0], 'common/images/MarkerStartSmall.gif'));
    }
    if("finish" in pc._temp.direct_routes) {
        html.push(stop_row(pc._temp.direct_routes.finish[0], 'common/images/MarkerEndSmall.gif'));
    }
    html.push("</table>");

    fixedtooltip({width:250, html:html.join(""), obj:cfg.obj, event:cfg.event, offset:[0,1]}, 0);
}

//showStopInfo = function(stop_id, layer_name, obj, event, show_header) {
showStopInfo = function(cfg) {
    var stops_ids = [];

    if("search" in cfg) {
        if(cfg.search.type == "start") {
            var stop_data = pc.as_start.arr[cfg.search.index];
        }
        else if(cfg.search.type == "finish") {
            var stop_data = pc.as_end.arr[cfg.search.index];
        }
        else {
            var stop_data = pc.as_stopsearch.arr[cfg.search.index];
        }

        //var transport_in_stop = stop_data.transport;
        var transport_in_stop = OldStopsInfo.getTransportInStops(stop_data.stops);

        for(var k=0;k<stop_data.stops.length;k++) {
            stops_ids.push(stop_data.stops[k].id);
        }
    }
    else {
        var stop_id = cfg.stop_id; //or cfg.search_id
        stops_ids.push(stop_id);
        var stop_data = pc.getSI().getStopData(stop_id);
        //var transport_in_stop = stop_data.temp;
        var transport_in_stop = OldStopsInfo.getTransportInStops([stop_id]);
    }
    //console.log(stop_data);

    var transport_all_info_HTML = "";
    var transport_types = pc.getAllTransportTypes();

    for(var tt in transport_types) {
        if(!(tt in transport_in_stop)) continue; // search atveju neturime transport_byroute_tag
        if (stop_data.transport_byroute_tag && !(tt in stop_data.transport_byroute_tag)) continue; // pakeiteme ish .temp, kad prafiltruoti tuos kurie turi route_tag=0 arba -

        var title = pc.i18n(transport_types[tt].title);
        var color = transport_types[tt].color;

        var numspan_array = []; // [ [num, html] ] // for sorting

        var transport_obj = transport_in_stop[tt];

        for (var num in transport_obj) {
            if(stop_data.transport_byroute_tag && !(num in stop_data.transport_byroute_tag[tt])) continue;

            if("search" in cfg) {
                var first_direction_id = transport_obj[num].direction_id;
                var stop_id = transport_obj[num].stop_id;
            }
            else {
                var first_direction_id = transport_obj[num].directions[0];
            }
            var city = pc.chooseCity(pc.getSI().getRouteData(first_direction_id));

            numspan_array.push([parseInt("0"+num, 10), ["<a href=\"",
                pc.url({city:city, page:"schedule", args:[tt, first_direction_id, stop_id]}),
                "\" class=\"number_link\"><span class='number_white' style='color:", color,
                "' onclick=\"hidetip();\" onmouseover=\"showStopTransportX('", stop_data.id, "','",
                first_direction_id, "','", (cfg.layer_name == "left")?"left":cfg.layer_name, "',1,this,event);\">",
                num, "</span></a> "].join("")]);
        }

        if (numspan_array.length != 0) {
            numspan_array.sort(function(x,y) {return (x[0] - y[0]);});
            var num_html = numspan_array.map(function(x) {return x[1];}).join("");
            transport_all_info_HTML += "<span class='transport_spn'>" + title + ": </span> " + num_html + "<br/>";
        }
    }

    var direct_route_icons = ["<img title=\"", pc.i18n("lngClickForstart"), "\" onclick=\"hidetip();sf_click_stops('start',[", stops_ids.join(","),
        "]);\" style='cursor:pointer;margin-left:5px;margin-top:0px;' src='common/images/MarkerStartSmall.gif'>",
        "<img title=\"", pc.i18n("lngClickForfinish"), "\" onclick=\"hidetip();sf_click_stops('finish',[", stops_ids.join(","),
        "]);\" style='cursor:pointer;margin-left:5px;margin-top:0px;' src='common/images/MarkerEndSmall.gif'>"].join("");

    var content = [];

    if(cfg.show_header) {
        var area = stop_data.area;
        var street = stop_data.street;
        var area_street = [];
        if (area != "0") area_street.push(area);
        if (street != "0") area_street.push(street);
        var area_street_html = (area_street.length?("<br/><span style='font-weight:normal;font-size:10px;'>(" + area_street.join(", ") + ")</span>"):"");

        content.push(["<div class='IWCaption'>", stop_data.name,
            "<img title=\"", pc.i18n("lngClickForMap"), "\" src='", _sub_dir, "common/images/zoom.gif' border='0' onclick=\"gmap_loadstops([", stops_ids[0], "]); hidetip();\" style='margin-left:5px;cursor:pointer'/>",
            direct_route_icons, area_street_html, "</div>"].join(""));
    }

    //transport_all_info_HTML += ["<a href=\"#", stops_ids.join(";"), "\" style=\"font-size:8pt;\" class=\"command\">Subscribe to operative information</a>"].join("");
    content.push(["<div class='IWContent' style='width:315px'>",  transport_all_info_HTML, "</div>"].join(""));

    var html = content.join("");

    var scroll_div_id = "";
    if(cfg.layer_name == "left" || cfg.layer_name == "right") {
        var scroll_div_id = pc._current_page_name + "_" + cfg.layer_name;
    }
    else if(cfg.layer_name) {
        var scroll_div_id = cfg.layer_name;
    }

    fixedtooltip_static({width:330, html:html, scroll_div_id:scroll_div_id, obj:cfg.obj, event:cfg.event, offset:[0,-1], cancel:true}, 300);
}


showRouteInfo = function(direction_id, layer_name, obj, event) {
    var route_data = pc.getSI().getRouteData(direction_id);

    var color = pc.getTransportData(route_data.transport).color;

    var nmb_class = (route_data.route_nmb.length == 1) ? "number_small1" : ((route_data.route_nmb.length == 2) ? "number_small" : "number_small3");

    /*
     var innerHTML = ["<table><tr><td colspan=\"2\"><a class='number_link' href=\"", pc.url({city:pc.chooseCity(route_data) ,page:"schedule", args:[transport, direction_id]}),
     "\"><span class='", nmb_class, "' style='background-color:", color, "'>", route_data.route_nmb,
     "</span></a><span style=\"cursor:pointer;\" onclick=\"hidetip_static(); gmap_loadroute(",
     direction_id, ");\"><img title=\"", pc.i18n("lngClickForMap"), "\" src='", _sub_dir, "common/images/zoom.gif' border='0' style='margin-left:5px;cursor:pointer'/> <span class=\"command\">",
     , pc.i18n("lngShowRouteMap") , "</span></span></td></tr>",
     "<tr><td style=\"text-align:right;\">", pc.i18n("lngOperator"), ":</td><td>", route_data.operator, "</td></tr>",
     "<tr><td style=\"text-align:right;\">", pc.i18n("lngAddress"), ":</td><td></td></tr>",
     "<tr><td style=\"text-align:right;width:80px;\">", pc.i18n("lngTelephone"), ":</td><td></td></tr></table>"];


     //innerHTML.push(["<a href=\"#", route_data.operator, ";", route_data.route_nmb, "\" style=\"font-size:8pt;\" class=\"command\">Subscribe to operative information</a>"].join(""));
     */


//dp 2010-02-16
    var dstart =  route_data.datestart;
    dstart=dstart.replace(/d1/g,pc.i18n("lng1DayAbb"));
    dstart=dstart.replace(/d2/g,pc.i18n("lng2DayAbb"));
    dstart=dstart.replace(/d3/g,pc.i18n("lng3DayAbb"));
    dstart=dstart.replace(/d4/g,pc.i18n("lng4DayAbb"));
    dstart=dstart.replace(/d5/g,pc.i18n("lng5DayAbb"));
    dstart=dstart.replace(/d6/g,pc.i18n("lng6DayAbb"));
    dstart=dstart.replace(/d7/g,pc.i18n("lng7DayAbb"));
//--

    var innerHTML = ["<table><tr><td colspan=\"2\"><a class='number_link' href=\"", pc.url({city:pc.chooseCity(route_data) ,page:"schedule", args:[transport, direction_id]}),
        "\"><span class='", nmb_class, "' style='background-color:", color, "'>", route_data.route_nmb,
        "</span></a><span style=\"cursor:pointer;\" onclick=\"hidetip_static(); gmap_loadroute(",
        direction_id, ");\"><img title=\"", pc.i18n("lngClickForMap"), "\" src='", _sub_dir, "common/images/zoom.gif' border='0' style='margin-left:5px;cursor:pointer'/> <span class=\"command\">",
        , pc.i18n("lngShowRouteMap") , "</span></span></td></tr>",
        "<tr><td style=\"text-align:right;\">", pc.i18n("lngOperator"), ":</td><td>", route_data.operator, "</td></tr>",

//dp 2010-02-16
//					"<tr><td style=\"text-align:right;\">", pc.i18n("lngAddress"), ":</td><td></td></tr>", 
        "<tr><td style=\"text-align:right;\">", pc.i18n("lngDatestart"), ":</td><td>", dstart, "</td></tr>",
//					"<tr><td style=\"text-align:right;width:80px;\">", pc.i18n("lngTelephone"), ":</td><td></td></tr></table>"];
//--					

        "</table>"];






    var html = innerHTML.join("");

    var scroll_div_id = "";
    if(layer_name == "left" || layer_name == "right") {
        var scroll_div_id = pc._current_page_name + "_" + layer_name;
    }
    else if(layer_name) {
        var scroll_div_id = layer_name;
    }


    //var scroll_div_id = pc._current_page_name + "_" + layer_name;
    fixedtooltip_static({width:250, html:html, scroll_div_id:scroll_div_id, obj:obj, event:event, offset:[0,-1], cancel:true}, 300);
}



// show_all: 0 only direction_id, 1 all including *, 2 similar routes to direction_id
showStopTransportX = function(stop_id, direction_id, layer_name, show_all, obj, event, voffset) {
    // "stop_id" -> panaudosime stoteles pavadinimo gavimui
    // "direction_id" -> gausime transport_type -> color
    // si._transport_data[transport_type][number] -> gausime visas kitas kryptis 
    // patikrinsime ar gautuose marsrutuose (kuriu direction_id nelygus argumentui direction_id) yra stotele su tokiu pat pavadinimu.

    var route_data = pc.getSI().getRouteData(direction_id);
    var stop_data = pc.getSI().getStopData(stop_id);

    var filtered_directions = {};
    // insert itself

    filtered_directions[direction_id] = {stop_id: stop_id, direction_name: route_data.direction_name, city:pc.chooseCity(route_data)};

    // Visas kryptis kuriose yra tokia stotele
    if (show_all == 1) {
        // filter directions without a stop with such name // probably very rare situation
        //var stop_name = stop_data.name;
        var directions = pc.getSI()._transport_data[route_data.transport][route_data.route_nmb];
        for (var i=0; i<directions.length; i++) {
            if (directions[i] == direction_id) continue; // skip itself
            var iroute_data = pc.getSI().getRouteData(directions[i]);
            var stops = iroute_data.stops;


            for(var j=0; j<stops.length; j++) {
                var name = pc.getSI().getStopData(stops[j]).name;
                if (name == stop_data.name) { // kryptyje yra tokio paties pavadinimo stotele
                    filtered_directions[directions[i]] = {stop_id: stops[j], direction_name: iroute_data.direction_name, city:pc.chooseCity(iroute_data)};
                    break;
                }
            }
        }
    }
    // Tik panashias i direction_id argumenta
    else if (show_all == 2) {
        var main_route = route_data.route_type.split(">");

        var directions = stop_data.temp[route_data.transport][route_data.route_nmb];
        for (var i=0; i<directions.length; i++) {
            if (directions[i] == direction_id) continue; // skip itself

            var iroute_data = pc.getSI().getRouteData(directions[i]);

            var side_route = iroute_data.route_type.split(">");
            if (isSimilar(main_route, side_route)) {
                filtered_directions[directions[i]] = {stop_id: stop_id, direction_name: iroute_data.direction_name, city:pc.chooseCity(iroute_data)};
            }
            //else {
            //}
        }
    }

    var transport_data = pc.getTransportData(route_data.transport);

    var directionsHTML = ["<span style='font-weight:bold;font-size:10pt;color:", transport_data.color, ";'>", pc.i18n(transport_data.title), "</span><img title=\"", pc.i18n("lngShowRouteMap"), "\" src='", _sub_dir, "common/images/zoom.gif' border='0' onclick=\"gmap_loadroute(", direction_id, ");\" style='margin-left:5px;cursor:pointer'/> <span style=\"cursor:pointer\" onclick=\"gmap_loadroute(", direction_id, ");\" class=\"command\">", pc.i18n("lngShowRouteMap"), "</span><br/>",
        "<span class=\"transport_spn\">", pc.i18n("lngShowTimeTables"), "</span><br/>"];
    for (var direction_id in filtered_directions) {
        var asterix = (stop_id != filtered_directions[direction_id].stop_id);

        var nmb_class = (route_data.route_nmb.length == 1) ? "number_small1" : ((route_data.route_nmb.length == 2) ? "number_small" : "number_small3");
        var city = filtered_directions[direction_id].city;

        directionsHTML.push(["<img title=\"", pc.i18n("lngShowRouteMap"), "\" src='", _sub_dir, "common/images/zoom.gif' border='0' onclick=\"gmap_loadroute(", direction_id, ", true);\" style='margin-right:5px;cursor:pointer'/><a class='number_link' href=\"", pc.url({city: city, page:"schedule", args:[route_data.transport, direction_id, filtered_directions[direction_id].stop_id]}),
            "\"><span class='", nmb_class, "' style='background-color:", transport_data.color, "'>", route_data.route_nmb, "</span></a><a style='color:",
            transport_data.color, ";' href='", pc.url({city: city, page:"schedule", args:[route_data.transport, direction_id, filtered_directions[direction_id].stop_id]}),
            "' onclick='hidetip()'>", filtered_directions[direction_id].direction_name, "</a>",
            (asterix?"*":""), "<br/>"].join(""));
    }

    var html = directionsHTML.join("");

    var scroll_div_id = layer_name;
    if(layer_name == "left" || layer_name == "right") {
        var scroll_div_id = pc._current_page_name + "_" + layer_name;
    }

    fixedtooltip({width: 270, html:html, scroll_div_id:scroll_div_id, obj:obj, event:event, offset:[0,-1], show_border:true}, 300);
}

function pt_autosearch_startend(cfg) {
    var arr = cfg.results;
    var rows = [];

    // loop throught arr of suggestions
    var line_count = 0;
    for (var i=0;i<arr.length;i++) {
        var val = arr[i].ascii;
        var name = arr[i].name;
        var area = arr[i].area;
        var street = arr[i].street;
        var id = arr[i].id;

        //console.log(arr[i]);

        var area_street = [];
        if (area != "0") area_street.push(area);
        if (street != "0") area_street.push(street);

        var stops = arr[i].stops;
        var stops_ids = [];
        for(var k=0;k<stops.length;k++) {
            stops_ids.push(stops[k].id);
        }

        var st = val.indexOf(toascii(cfg.search_str)); // reikia kazhkur ishkelti! arba kitaip spresti! // gal ishkelti tik teksto formavima! zhemiau esanti
        var stop_name = ["<span>", name.substring(0,st), "<em>", name.substring(st, st+cfg.search_str.length), "</em>", name.substring(st+cfg.search_str.length), "</span>"].join("");

        var full_name = "<span style=\"white-space:nowrap;\">" + stop_name + "</span>" + (area_street.length?("<br/><span style='white-space:nowrap;font-size:10px;'>(" + area_street.join(", ") + ")</span>"):"");
        var title = name + (area_street.length?(" (" + area_street.join(", ") + ")"):"");
        var stop_html = ["<img onclick=\"showStopInfo({search:{type:'",cfg.sf_type,"', index:'",i,"'},stop_id:'",id,"',layer_name:'search_start_result',obj:this,event:event,show_header:true})\" class=\"img_info\" style=\"cursor:pointer;margin:-3px 3px -3px -1px;\" src=\"common/images/info.gif\" width=\"13\" height=\"15\"/><span title=\"", title, "\" onclick=\"sf_click_stops('", cfg.sf_type, "',[", stops_ids.join(","), "]);\" style=\"cursor:pointer;\" class=\"text_link\">", full_name, "</span>"].join("");

        var odd_class = (cfg.sf_type == "start") ? "odd_start" : ((cfg.sf_type == "finish")? "odd_finish": "odd");
        rows.push(["<tr", ((!(i%2))? (" class=\""+odd_class+"\""):""), "><td onclick=\"\" style='vertical-align:top;'>", stop_html, "</td></tr>"].join(""));
    }

    if ((arr.length == 0) && (cfg.search_str.length)) {
        return pc.i18n("lngNoStopsFound");
    }

    if(rows.length == 0) {
        return "";
    }

    //return ["<table class=\"data\" style=\"", (cfg.sf_type == "finish")?"margin-left: 110px;":"" ,"margin-bottom:0px; margin-top:5px; width: 350px;\">", rows.join(""),"</table>"].join("");
    return ["<table class=\"data\" style=\"background-color:white;margin-bottom:0px; margin-top:5px;\">", rows.join(""),"</table>"].join("");
}


function pt_autosearch(cfg) {
    var arr = cfg.results;
    var rows = [];

    // loop throught arr of suggestions
    var line_count = 0;
    for (var i=0;i<arr.length;i++) {
        var val = arr[i].ascii;
        var name = arr[i].name;
        var area = arr[i].area;
        var street = arr[i].street;
        var id = arr[i].id;

        //console.log(arr[i]);

        var area_street = [];
        if (area != "0") area_street.push(area);
        if (street != "0") area_street.push(street);

        var stops = arr[i].stops;
        var stops_ids = [];
        for(var k=0;k<stops.length;k++) {
            stops_ids.push(stops[k].id);
        }

        var st = val.indexOf(toascii(cfg.search_str)); // reikia kazhkur ishkelti! arba kitaip spresti! // gal ishkelti tik teksto formavima! zhemiau esanti
        var stop_name = ["<span>", name.substring(0,st), "<em>", name.substring(st, st+cfg.search_str.length), "</em>", name.substring(st+cfg.search_str.length), "</span>"].join("");

        var stop_html = ["<img onclick=\"showStopInfo({search:{type:'",cfg.sf_type,"', index:'",i,"'},stop_id:'",id,"',layer_name:'search_result',obj:this,event:event,show_header:true})\" class=\"img_info\" style=\"cursor:pointer;margin:-3px 3px -3px 4px;\" src=\"common/images/info.gif\" width=\"13\" height=\"15\"/>",
            "<span style=\"cursor:pointer;\" class=\"text_link\"><span style=\"white-space:nowrap;\">", stop_name, "</span>", (pc.show_transport)?"<br/>":"", (area_street.length?(" <span style='font-size:10px;white-space:nowrap;'>(" + area_street.join(", ") + ")</span>"):""), "</span>"].join("");

        var is_alias = arr[i].is_alias;
        var is_expandable = (stops.length > 1 || is_alias);
        var class_name = (is_alias) ? "alias" : (!(i%2)? "odd":"");

        rows.push(["<tr class=\"", class_name, "\"><td onclick=\"highlight_stopname_2(this);load_search_result_schedule({index:", i, "});\" style='vertical-align:top;'>",
            is_expandable ? "<img onclick=\"expand_stops('" + id + "',this,event);\" style=\"margin-left:-3px;\" src=\"common/images/expand.gif\"/>":"<span style=\"margin-left:8px;white-space:nowrap;\"/>", stop_html, "</span></td>"].join(""));
        if (pc.show_transport) {
            rows.push(["<td style=\"vertical-align:top;\">",  pt_transport_in_stop_html({search:{type:cfg.sf_type, index:i}, parent_id:"search_result"}), "</td></tr>"].join(""));
        }
        else {
            rows.push("</tr>");
        }

        if(is_expandable) {
            for(var j=0; j<stops.length; j++) {
                var stop_data = pc.getSI().getStopData(stops[j]);
                var area_street = [];
                if (stop_data.area != "0") area_street.push(stop_data.area);
                if (stop_data.street != "0") area_street.push(stop_data.street);

                var stop_html = ["<img onclick=\"showStopInfo({stop_id:'",stops[j],"',layer_name:'search_result',obj:this,event:event,show_header:true})\" class=\"img_info\" style=\"cursor:pointer;margin:-3px 3px -3px 4px;\" src=\"common/images/info.gif\" width=\"13\" height=\"15\"/>",
                    "<span style=\"cursor:pointer;\" class=\"text_link\"><span style=\"white-space:nowrap;\">", stop_data.name, "</span>", (pc.show_transport)?"<br/>":"", (area_street.length?(" <span style='font-size:10px;white-space:nowrap;'>(" + area_street.join(", ") + ")</span>"):""), "</span>"].join("");

                var row_classes = ["row_" + id];
                if(class_name) row_classes.push(class_name);
                rows.push(["<tr style=\"display:none;\" class=\"", row_classes.join(" "), "\"><td onclick=\"highlight_stopname_2(this);load_search_result_schedule({stop_id:", stops[j], "});\" style='vertical-align:top;'><span style=\"margin-left:8px;white-space:nowrap;\"/>", stop_html, "</span></td>"].join(""));

                if (pc.show_transport) {
                    rows.push(["<td style=\"vertical-align:top;\">",  pt_transport_in_stop_html({stop:stop_data, parent_id:"search_result"}), "</td></tr>"].join(""));
                }
                else {
                    rows.push("</tr>");
                }
            }
        }
    }

    if ((arr.length == 0) && (cfg.search_str.length)) {
        return pc.i18n("lngNoStopsFound");
    }
    else if(arr.length == 0) {
        return pc.i18n("lngSearchTip");
    }

    return ["<table class=\"data\" style=\"margin-bottom:40px;\">", rows.join(""),"</table>"].join("");
}

function collapse_stops(stop_id, button, event) {
    if(!event) var event = window.event;
    event.cancelBubble=true;
    if (event.stopPropagation) event.stopPropagation();

    button.src = "common/images/expand.gif";
    button.onclick = function(event) { expand_stops(stop_id, button, event); };

    var content_el = document.getElementById("search_result");
    var rows = getElementsByClassName("row_" + stop_id, content_el);

    for(var i=0;i<rows.length;i++) {
        rows[i].style.display = "none";
    }
}


function expand_stops(stop_id, button, event) {
    if(!event) var event = window.event;
    event.cancelBubble=true;
    if (event.stopPropagation) event.stopPropagation();

    button.src = "common/images/collapse.gif";
    button.onclick = function(event) { collapse_stops(stop_id, button, event); };

    var content_el = document.getElementById("search_result");
    var rows = getElementsByClassName("row_" + stop_id, content_el);

    for(var i=0;i<rows.length;i++) {
        rows[i].style.display = "";
        //rows[i].style.display = "table-row";
    }
    //console.log(rows);
}

function pt_search_input() {

    return  ["<div id='search_tag' style='padding-left:8px;padding-bottom:6px;'>",
        "<table><tr><td colspan='2'>",
        "<form style='margin-left:2px;margin-top:4px;' onSubmit='return false;'><input id='search_tab_input' class='tlp-input-empty' type='text' title='", pc.i18n("lngTypeName"), "' size='36' value='", pc.i18n("lngTypeName"), "' name='stotele'/></form>",
        "</td><td>",
        "<input type='checkbox' ", (pc.show_transport?("checked='true'"):"") ," style='margin: 8px 0px 0px 8px;' onclick=\"loadRouteNumbersSearch(this);\" name='routenum'/>",
        "<span style='line-height: 14pt; margin-left:4px; font-size: 8pt;'>", pc.i18n("lngShowRouteNumbers"), "</span>",
        "</td></tr></table>",
        "</div>",
        "<div class='tagContent'>",
        "<div id='search_tab_content'>",
        "<div id='search_result' style='margin-top:0px;'><div style=\"padding-left:0px;\" class=\"content\">", pc.i18n("lngSearchTip"), "</div></div>","</div>",
        "</div>",
        "</div>"].join("");
}



function pt_route_search_input() {
    var stamp = new Date();
//  var myValue = '\'\'' + stamp.getFullYear() + "-" + addZero((stamp.getMonth() + 1)) + "-" + addZero(stamp.getDate()) + " " + addZero(stamp.getHours()) + ":" + addZero(stamp.getMinutes()) + '\'\'';
    var my_date = [addZero(stamp.getDate()), "-", addZero((stamp.getMonth() + 1)), "-", stamp.getFullYear()].join("");
    var my_time = [addZero(stamp.getHours()), ":", addZero(stamp.getMinutes())].join("");


    var transport_types = pc.getAllTransportTypes();
    var transport = {};
    for (var tt in transport_types) {
        transport[tt] = true;
    }
    // var transport = {buss:true,troll:false};
    //work_days[mydate.getDay() || 7] = 1;

    return  ["<div id='route_search_tag' style='height:50px;position:relative; padding-top:8px; padding-left:8px;'>",
        "<table style='width:473px; height:35px;'>",

        "<tr><td style=\"width:12px;\">",
        "<img style=\"margin-top:5px;\" src=\"common/images/MarkerStartSmall.gif\"></td><td><form style='margin-left:2px;margin-top:4px;' onSubmit='return false;'><input id='search_start_tab_input' class='tlp-input-empty' type='text' title='", pc.i18n("lngStartStop"), "' size='28' value='", pc.i18n("lngStartStop"), "' name='stotele'/></form>",
        "</td><td style=\"width:12px;\"><img style=\"margin-top:5px;margin-right:3px;cursor:pointer;\" onclick=\"switch_directions();\" title=\"", pc.i18n("lngReverseDirections"), "\" src=\"common/images/switch.gif\"></td>",
        "<td style=\"width:12px;\"><img style=\"margin-top:5px;\" src=\"common/images/MarkerEndSmall.gif\">",
        "</td><td><form style='margin-left:2px;margin-top:4px;' onSubmit='return false;'><input id='search_end_tab_input' class='tlp-input-empty' type='text' title='", pc.i18n("lngDestinationStop"), "' size='28' value='", pc.i18n("lngDestinationStop"), "' name='stotele'/></form>",
        "</td></tr>",

        "<tr><td></td><td style=\"width:220px; height:24px; padding:0px; line-height: 7pt; vertical-align:top;\" id=\"search_start_address\"></td><td></td><td></td><td style=\"width:220px; padding:0px; line-height: 7pt; vertical-align:top;\" id=\"search_end_address\"></td></tr>",

        "<tr><td></td><td colspan=\"4\"><table style=\"width:100%;\"><tr><td style=\"white-space:nowrap\">",
        "<select id=\"dep_arr_input\" name=\"dep_arr\">",
        "<option selected=\"selected\" value=\"\">", pc.i18n("lngDepart"), "</option>",
        "<option value=\"1\">", pc.i18n("lngArrive"), "</option></select>",
        "<input style=\"margin-left:6px;width:36px;\" type=\"text\" id=\"search_time_input\" name=\"txtStartTime\" maxlength=\"5\" value=\"", "", my_time, "" ,"\" size=\"4\"/>",
        "<form id=\"mytime_form\" style=\"display:inline;\" name=\"time_form\"><input style=\"margin-right:0px;margin-left:6px;width:70px;\" type=\"text\" id=\"search_date_input\" xdisabled=\"true\" maxlength=\"10\" value=\"", my_date, "\" name=\"txtStartDate\"/>",
        "</form>",
        "</td><td><span onclick=\"show_options(document.getElementById('dep_arr_input'), event)\" class=\"command\" style=\"margin-left:8px;cursor:pointer;\">", pc.i18n("lngMoreOptions"), "</span>",
        "<!--/td><td style=\"margin-top:10px; padding-bottom:6px;text-align:right;\" colspan=\"3\"-->",
        "</td><td style=\"text-align:right;\">",
        "<input class=\"button\" style=\"width:125px;margin-left:10px;margin-right:3px;font-weight:bold;\" onclick=\"get_directions();\" type=\"submit\" value=\"", pc.i18n("lngGetDirections"), "\" name=\"optimal\"/>",
        "<input id=\"search_transport_input\" type=\"hidden\" value=\"", ObjectToString(transport), "\"/>",
        "<input id=\"search_change_input\" type=\"hidden\" value=\"3\"/>",
        "<input id=\"search_maxwalk_input\" type=\"hidden\" value=\"2000\"/>",
        "<input id=\"search_walkspeed_input\" type=\"hidden\" value=\"4\"/>",
        "</td><td style=\"width:18px;\"><img id=\"otsi_loader\" src=\"common/images/loader.gif\" style=\"display: none; margin-left: 4px;\"/></td></tr></table></td></tr>",





        "</table></div>",
        "<div id='search_start_result' style='margin-left:0px; margin-top:55px;'></div>",
        ""].join("");
}

/*
 showTransportMenu = function(obj,event) {
 var trans = pc.content[pc.city].transport;

 var ils = ["<span class='simple'><ul class='menu'>"];
 for(var transport in trans) {
 var color = trans[transport].color;
 var title = trans[transport].title;
 // {city: "valga", page:"schedule", args:["regionalbus"]},
 var url = pc.url({city: pc.city, page:"schedule", args:[transport]});
 ils.push(["<li><a style='font-family:arial;font-weight:bold;font-size:12pt;color:",color,"' onclick=\"hidetip();\" href=\"", url , "\">", pc.i18n(title), "</a></li>"].join(""));
 }
 //style='font-weight:bold;font-size:10pt;'

 ils.push("</ul></span>");

 var html = ils.join("");
 fixedtooltip({width:250, html:html, obj:obj, event:event, offset:[0,1]}, 0);
 }

 */

function pt_transport_menu() {
    var trans = pc.content[pc.city].transport;

    var html = ["<div id='nav-menu'>"];
    for(var transport in _transport_data) {
        if(!(transport in trans)) continue;

        var transport_data = pc.getTransportData(transport);
        var color = (transport == pc.transport) ? transport_data.color : "black";
        var title = transport_data.title;
        // {city: "valga", page:"schedule", args:["regionalbus"]},
        var url = pc.url({city: pc.city, page:"schedule", args:[transport]});
        html.push(["<span class='nav-menu-item'><a style='", ((transport == pc.transport)?"font-weight:bold;":""), "text-decoration:underline;font-family:arial;font-size:12pt;color:",color,"' href=\"", url , "\">", pc.i18n(title), "</a></span> "].join(""));
    }
    html.push("</div>");
    return html.join("");
    //"<div id='nav-menu'><span class='nav-menu-item'><a href='#'>Services</a></span><span class='nav-menu-item'><a href='#'>Services</a></span><span class='nav-menu-item'><a href='#'>Services</a></span><span class='nav-menu-item'><a href='#'>About us</a></span><span class='nav-menu-item'><a href='#'>Contact us</a></span></div>";
}

function pt_schedule_top() {
    return  ["<div id='transport_menu_tag' style='position:relative; top:0px; padding-left:8px;'>",
        , pt_transport_menu(),
        "</div><div id='schedule_result' style='margin-left:0px; margin-top:0px;'>", pt_loading(), "</div>",
        ""].join("");
}


function sf_activate(span) {
    var className = span.className;
    var classname_split = className.split("_");
    var sf_type = classname_split[0];
    var sf_status = classname_split[1];

    if(sf_status == "passive") {
        span.className = sf_type + "_active";

        span.onmouseout = function() {
            span.className = className;
        }
    }
}

function sf_click_stops(sf_type, stops_ids) {
    //var data = pc.as._search_results[index];

    //var stops_ids = [];
    //for (var i=0; i<data.stops.length; i++) {
    //	stops_ids.push(data.stops[i].id);
    //}

    var cfg = {};
    cfg[sf_type] = stops_ids;

    markStopAs(cfg);
}


function sf_click(sf_type, index) {
    var data = pc.as._search_results[index];

    var stops_ids = [];
    for (var i=0; i<data.stops.length; i++) {
        stops_ids.push(data.stops[i].id);
    }

    var cfg = {};
    cfg[sf_type] = stops_ids;
    markStopAs(cfg);
}


function pt_direct_routes_stops() {

    /*
     if(!pc._temp.direct_routes) {
     document.getElementById("start_search_choice").innerHTML = "<span style=\"font-size: 10px;\">(" + pc.i18n("lngNoStartStop") + ")</span>";
     document.getElementById("end_search_choice").innerHTML = "<span style=\"font-size: 10px;\">(" + pc.i18n("lngNoEndStop") + ")</span>";
     }
     */
    if (!pc.as_start) return;

    if(pc._temp.direct_routes && (pc._temp.direct_routes.start || pc._temp.direct_routes.finish)) {

        if(pc._temp.direct_routes.start) {
            var start_stop_data = pc.getSI().getStopData(pc._temp.direct_routes.start[0]);
            var name = start_stop_data.name;
            pc.as_start.fld.value = name;

            var address = format_streetarea(start_stop_data);
            var title = name + " " + address;

            document.getElementById("search_start_address").innerHTML = "<span style=\"font-size: 10px;\">" + address + "</span>";

            /*
             document.getElementById("search_start_result").innerHTML = ["<div title=\"", title, "\" style=\"font-size:small;\"><b>", name,
             "</b><span style=\"font-size: 10px;\"> ", address, "</span></div>"].join("");
             */
        }
        else {
            document.getElementById("search_start_address").innerHTML = "<span style=\"color:blue;font-size: 10px;\">(" + pc.i18n("lngTypeName") + ")</span>";
            //pc.as_start.fld.value = pc.as_start.fld.initial_value || "";
        }

        if(pc._temp.direct_routes.finish) {
            var finish_stop_data = pc.getSI()._stops_data[pc._temp.direct_routes.finish[0]];
            var name = finish_stop_data.name;
            pc.as_end.fld.value = name;

            var finish_area_street = [];
            if (finish_stop_data.area != "0") finish_area_street.push(finish_stop_data.area);
            if (finish_stop_data.street != "0") finish_area_street.push(finish_stop_data.street);

            var address = (finish_area_street.length ? " ("+finish_area_street.join(", ")+")" : "");
            var title = name + " " + address;

            document.getElementById("search_end_address").innerHTML = "<span style=\"font-size: 10px;\">" + address + "</span>";

            /*
             document.getElementById("search_end_result").innerHTML = ["<div title=\"", title ,"\" style=\"font-size:small;\"><b>", name,
             "</b><span style=\"font-size: 10px;\"> ", address, "</span></div>"].join("");
             */
        }
        else {
            document.getElementById("search_end_address").innerHTML = "<span style=\"color:blue;font-size: 10px;\">(" + pc.i18n("lngTypeName") + ")</span>";
            //pc.as_end.fld.value = pc.as_end.fld.initial_value || "";
        }

    }
    else {
        document.getElementById("search_start_address").innerHTML = "<span style=\"color:blue;font-size: 10px;\">(" + pc.i18n("lngTypeName") + ")</span>";
        document.getElementById("search_end_address").innerHTML = "<span style=\"color:blue;font-size: 10px;\">(" + pc.i18n("lngTypeName") + ")</span>";
    }

}

function loadRouteNumbersSearch(cb) {
    pc.show_transport = cb.checked;
    if(pc.as_stopsearch.arr && pc.as_stopsearch.arr.length) {
        pc.as_stopsearch.search();
    }
}

function loadRouteNumbers(transport, direction_id, stop_id, cb, time_page) {
    pc.show_transport = cb.checked;
    //console.log(pc.show_transport);
    if(time_page) {
        pc.show_content("schedule_result", pt_schedule_time({transport:transport, direction_id:direction_id, stop_id:stop_id, si:pc.getSI(), template:pt_left_shedule_time}), "transport_menu_tag");
        //pc.show_left_content({transport:transport, direction_id:direction_id, stop_id:stop_id, si:pc.getSI(), template:pt_left_shedule_time}, pt_schedule_time);
    }
    else {
        pc.show_content("schedule_result", pt_schedule_change({transport:transport, direction_id:direction_id, stop_id:stop_id, si:pc.getSI(), template:pt_left_shedule_change}), "transport_menu_tag");
        //pc.show_left_content({transport:transport, direction_id:direction_id, stop_id:stop_id, si:pc.getSI(), template:pt_left_shedule_change}, pt_schedule_change);
    }
}



function load_search_result_schedule(cfg) {
    // arba duomenis ish paieshkos objekto pagal index'a arba cfg.data - duomenu objektas, kai atidaroma per url'a
    if(typeof cfg.index == "number") {
        var stop_data = pc.as_stopsearch.arr[cfg.index];
        pc._temp.search_stopsindex = cfg.index; // nuskaitysime search callbacke
        var stops_arg = stop_data.is_alias ? stop_data.id : stop_data.stops.join(",");

        //console.log(stop_data, stops_arg, stop_data.is_alias);
    }
    else {
        //var stop_data = pc.getSI().getStopData(cfg.stop_id);
        //console.log("wtf to do with pc._temp.search_stopsindex ", cfg.stop_id);
        delete pc._temp.search_stopsindex; // tam kad neimatume to kuris buvo galimai pasirinktas anksciau;
        var stops_arg = cfg.stop_id;
    }


    //var stops_arg = [];
    //for(var i=0; i<stop_data.stops.length; i++) stops_arg.push(stop_data.stops[i].id); // surenkame visu stoteliu idus

    //pc.as_stopsearch.fld.value = stop_data.stops[0].name;
    pc.hash(pc.url({city:pc.city, page:"search", args:[stops_arg]}));
}

/* naudojamas tik search ir route search - schedule veikia kitaip per pc._temp.schedule_selected_routes*/
function init_transport_filter_checkboxes(transport) {

    var hash = window.location.hash;

    //var transport = pc.getSI()._stops_data[args[2]].temp;
    var selected_routes = {};
    pc._temp[hash] = {selected_routes:selected_routes};

    for(var tt in transport) {

        //console.log(sizeOf(transport[tt]));
        //if(sizeOf(transport[tt]) ==0) continue;

        selected_routes[tt] = {};
        for(var route_nmb in transport[tt]) {
            selected_routes[tt][route_nmb] = true; // pradzioje rodome visus
        }
    }

    //console.log("sr: ", selected_routes);

    //console.log(pc._temp[hash]);
}

function pt_search_results_right(cfg) {
    pc.frame(0);
    pc.show_right_content(pt_loading(), function(x){return x;});

    window.setTimeout(function () {

        var stop_data = ("index" in cfg) ? pc.as_stopsearch.arr[cfg.index] : cfg.data;

        stop_data.transport = OldStopsInfo.getTransportInStops(stop_data.stops);

        //console.log("pt_search_results_right: ", cfg, stop_data, stop_data.transport);

        var transport_obj = stop_data.transport;
        init_transport_filter_checkboxes(transport_obj);


        var stops = {};

        /*
         for(var i=0; i<stop_data.stops.length; i++) {
         var stop_id = stop_data.stops[i];
         var data = pc.getSI().getStopData(stop_id);

         stops[stop_id] = [];

         for(var tt in data.temp) {
         for(var route_nmb in data.temp[tt]) {
         stops[stop_id] = stops[stop_id].concat(data.temp[tt][route_nmb]);
         }
         }
         }
         */
        var number_filter = false;
        if(cfg.number) {
            var intValue = parseInt(cfg.number, 10);
            var filter_ascii = toascii(cfg.number);
            number_filter = true;
        }


        for(var i=0; i<stop_data.stops.length; i++) {
            var stop_id = stop_data.stops[i];
            var data = pc.getSI().getStopData(stop_id);
            stops[stop_id] = [];

            for(var ll=0; ll<data.indir2.length; ll+=2) {
                var direction_id = data.indir2[ll];

                if(number_filter) {
                    var route_data = pc.getSI().getRouteData(direction_id);
                    var number = route_data.route_nmb;

                    var intValueNumber = parseInt(number, 10);
                    if (isNaN(intValueNumber) || isNaN(intValue)) { // yra neskaiciu
                        if(filter_ascii != toascii(number)) continue; // ir jie ne vienodi
                    }
                    else if(!isNaN(intValueNumber) && !isNaN(intValue)) { // abu skaiciai
                        if(intValueNumber != intValue) continue; // ir abu nevienodi
                        if(filter_ascii.length > number.length) continue; // ivdede 17A - tai nerodom 17
                    }
                }
                stops[stop_id].push(direction_id);
            }
        }

        pc.getSI().executeOnScheduleList({download:true}, function(data) {

            //console.log("on schedulest!", data);

            var arg = {
                workdays:{},
                direction_schedule:{},
                direction_schedule_list:{},
                stop_schedule_byhour:{"1":{}, "2":{}, "3":{}, "4":{}, "5":{}, "6":{}, "7":{}},
                stop_schedule_byhour_all:{}
                , valid_from: 2 // nerodome tu kuriu valid_from daugiau kaip dvi savaites nuo siandienos
            }

            //console.log("stops: ", stops);

            for(var id in stops) {
                var directions = stops[id];

                //console.log("directions: ", directions);

                arg.directions = directions;
                arg.directions_stack = directions.slice(0, directions.length); // reiketu nukelti i f-cijos vidu...
                arg.stop_id = id;

                pc.getSI().executeOnScheduleList(arg, function(data) {
                    //direct_routes_html.push(pt_stops_search_direct_routes(data));
                });
            }

            pc.show_right_content(arg, function(arg) {
                //console.log("args: ", arg);

                var stop_id = arg.stop_id;
                if (!stop_id) return "<div class='content'>No data found.</div>";

//	                var stop_data = pc.getSI().getStopData(stop_id);
                //console.log(stop_id, stop_data);

                var area_street = [];
                if (stop_data.area != "0") area_street.push(stop_data.area);
                if (stop_data.street != "0") area_street.push(stop_data.street);

                var transport_html = ["<table><tr><td style=\"vertical-align:top;white-space:nowrap;padding-right:20px;\"><b>", stop_data.name,
                    "</b><br/>", (area_street.length?("<span style='font-size:10px;'>(" + area_street.join(", ") + ")</span><br/>"):""), "</td><td>"];

                // TODO:
                for (var tt in transport_obj) {
                    var transport_data = pc.getTransportData(tt);

                    transport_html.push(["<div class=\"transport_choice\" style=\"cursor:default;\" onclick=\"report_choices(this, event);\" onmouseover=\"report_choices(this, event);\"><input style='margin:0px;' onclick=\"checkReportTableAllFilter(this,'", tt, "');\" name='cb' ", ((true) ?"checked='checked'":"") ," type='checkbox' value='", tt, "'/> <span style=\"color:", transport_data.color,
                        ";font-weight: bold;\">", pc.i18n(transport_data.title), "</span></div>"].join(""));
                }


                transport_html.push("</td></tr></table><br/>");
                //transport_html.push("</td></tr><tr><td></td><td><input type=\"checkbox\" name=\"showfilter\" onclick=\"filter_toggle(this);\" style=\"margin: 0px;\"/>"+
                //                    "<span class=\"show_filter\" style=\"line-height: 14pt; margin-left: 4px; font-size: 8pt;\">" + pc.i18n("lngShowFilter") + "</span></td></tr></table><br/>");


                return "<div class='content'>" + transport_html.join("") + pt_report_timetable(arg) + "</div>";
            });

            //window.setTimeout(function () {
            var el_id = pc._current_page_name + "_viastop_filter_input";
            //console.log(el_id, document.getElementById(el_id));

            var as_viastop = new AutoSearch2({
                input:pc._current_page_name + "_viastop_filter_input",
                type:"viastop",
                template:pt_autosearch_startend,
                callback: function(stop_data) {
                    var visa_stop_filter = document.getElementById(pc._current_page_name + "_viastop_filter");
                    if(visa_stop_filter) {
                        visa_stop_filter.value = stop_data.stops.join(",");
                    }
                }
            });
            //}, 10);


        });
    }, 10);
}


function load_direct_routes(cfg) {
    pc.frame(0);
    pc.show_right_content(pt_loading(), function(x){return x;});

    window.setTimeout(function () {

        var start_ids = cfg.start_ids; //pc._temp.direct_routes.start;
        var finish_ids = cfg.finish_ids; //pc._temp.direct_routes.finish;

        var si = pc.getSI();

        var direct_routes_hash = {};
        var directions = [];

        for(var st=0; st<start_ids.length; st++) {
            var start_id = start_ids[st];
            for (var fi=0; fi<finish_ids.length; fi++) {
                var finish_id = finish_ids[fi];

                var key = [start_id, ":", finish_id].join("");

                var start_transport = si._stops_data[start_id].temp;
                var finish_transport = si._stops_data[finish_id].temp;

                for(var tt in finish_transport) {
                    var transport_nmb_list = finish_transport[tt];
                    for(var route_nmb in transport_nmb_list) {
                        var transport_list = transport_nmb_list[route_nmb];

                        for(var i=0; i<transport_list.length; i++) {
                            var direction_id = transport_list[i];
                            var direction_data = si._routes_data[direction_id];
                            var stops = direction_data.stops;

                            for(var j=0; j<stops.length;j++) {
                                if (stops[j] == start_id) {
                                    directions.push(direction_id);

                                    if (!(key in direct_routes_hash)) { direct_routes_hash[key] = [];}
                                    direct_routes_hash[key].push(direction_id);

                                    break;
                                }
                                if (stops[j] == finish_id) { // radome fiisha ankschiau nei starta (atbuline kryptis kuri netinka).
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        //preiname per visus finisho transportus ir tikriname ar tarp jo stoteliu yra starto stotele (svarbu kad starto indexas butu iki finisho indexo).

        if(directions.length) {

            var arg = {
                directions: directions,
                directions_stack: directions.slice(0, directions.length), // reiketu nukelti i f-cijos vidu...
                workdays:{},
                direction_schedule:{},
                direction_schedule_list:{},
                valid_from:1
            }

            //TODO padaryti shios funkcijos versija kuri nedarytu jokio duomenu processingo, bet tik atsiustu failus (Nereikes jeigu visi duomenis bus pas klienta)
            pc.getSI().executeOnScheduleList(arg, function(data) {
                // Dabar jau galima vel kviesti executeOnScheduleList - kuris tikrai bus vykdomas sinchronishkai - failai atsiusti ankstesnio kvietimo metu
                var si = pc.getSI();

                var start_id_instance = start_ids[0]; //pc._temp.direct_routes.start[0];
                var finish_id_instance = finish_ids[0]; //pc._temp.direct_routes.finish[0];

                var start_stop_data = pc.getSI().getStopData(start_id_instance);
                var finish_stop_data = pc.getSI().getStopData(finish_id_instance);

                //TODO:  "area+street" padaryti stoteliu nuskaitymo metu.... nes labai daug kur prireikia...
                var start_area_street = [];
                if (start_stop_data.area != "0") start_area_street.push(start_stop_data.area);
                if (start_stop_data.street != "0") start_area_street.push(start_stop_data.street);

                var finish_area_street = [];
                if (finish_stop_data.area != "0") finish_area_street.push(finish_stop_data.area);
                if (finish_stop_data.street != "0") finish_area_street.push(finish_stop_data.street);


                var direct_routes_html = [];
                var route_html = ["<b>", start_stop_data.name,
                    "</b> - <br/><span style=\"font-size: 10px;\">",
                    (start_area_street.length ? " ("+start_area_street.join(", ")+")" : ""),
                    "</span></td><td><b>",
                    finish_stop_data.name,
                    "</b><br/><span style=\"font-size: 10px;\">",
                    (finish_area_street.length ? " ("+finish_area_street.join(", ")+")" : ""),
                    "</span>"
                ].join("");


                //     Dabar man chia reikia prasukti cikla per visas skirtingas start_id - finish_id poras (ir per jas vazhiuojanchius marshrutus).
                //     ir joms ishkviesti executeOnScheduleList su argumentais - marsrutai, start_id


                var start_finish_hash = {};
                for (var key in direct_routes_hash) {
                    var key_split = key.split(":");
                    var start_id = key_split[0];
                    var finish_id = key_split[1];

                    var direction_ids = direct_routes_hash[key];
                    for(var i=0; i<direction_ids.length; i++) {
                        var dir_id = direction_ids[i];
                        start_finish_hash[dir_id] = {start_id:start_id, finish_id:finish_id};
                    }
                }

                var arg = {
                    start_finish_hash:start_finish_hash,
                    workdays:{},
                    direction_schedule:{},
                    direction_schedule_list:{},
                    stop_schedule_byhour:{"1":{}, "2":{}, "3":{}, "4":{}, "5":{}, "6":{}, "7":{}},
                    stop_schedule_byhour_all:{},
                    valid_from:1
                }

                for (var key in direct_routes_hash) {
                    var key_split = key.split(":");
                    var start_id = key_split[0];
                    var finish_id = key_split[1];

                    arg.direction_ids = direct_routes_hash[key];
                    arg.directions_stack = arg.direction_ids.slice(0, arg.direction_ids.length); // reiketu nukelti i f-cijos vidu...
                    arg.stop_id = start_id;

                    pc.getSI().executeOnScheduleList(arg, function(data) {
                        //direct_routes_html.push(pt_stops_search_direct_routes(data));
                    });
                }

                var transport_obj = {}; // susirenkame duomenis filtrams...
                for(var i=0; i<arg.direction_ids.length; i++) {
                    var route_data = pc.getSI().getRouteData(direction_ids[i]);
                    var route_nmb = route_data.route_nmb;
                    var tt = route_data.transport;
                    if(!(tt in transport_obj)) {
                        transport_obj[tt] = {};
                    }
                    transport_obj[tt][route_nmb] = true;
                }
                init_transport_filter_checkboxes(transport_obj);

                var transport_html = ["<table><tr><td style=\"vertical-align:top;white-space:nowrap;padding-right:20px;\">", route_html, "</td><td>"];

                // TODO:
                for (var tt in transport_obj) {
                    var transport_data = pc.getTransportData(tt);
                    transport_html.push(["<div class=\"transport_choice\" style=\"cursor:default;\" onclick=\"report_choices(this, event);\" onmouseover=\"report_choices(this, event);\"><input style='margin:0px;' onclick=\"checkReportTableAllFilter(this,'", tt, "');\" name='cb' ", ((true) ?"checked='checked'":"") ," type='checkbox' value='", tt, "'/> <span style=\"color:", transport_data.color,
                        ";font-weight: bold;\">", pc.i18n(transport_data.title), "</span></div>"].join(""));
                }

                //transport_html.push("</td></tr><tr><td></td><td></td><td><input type=\"checkbox\" name=\"showfilter\" onclick=\"filter_toggle(this);\" style=\"margin: 0px;\"/>"+
                //                         "<span class=\"show_filter\" style=\"line-height: 14pt; margin-left: 4px; font-size: 8pt;\">" + pc.i18n("lngShowFilter") + "</span></td></tr></table><br/>");

                transport_html.push("</td></tr></table><br/>");

                direct_routes_html.push(transport_html.join(""));

                direct_routes_html.push(pt_stops_search_direct_routes(arg));

                //pc.show_left_content("<div id=\"direct_routes_list\" class=\"content routes\">" + direct_routes_html.join("") + "</div>", function(str) {return str;});
                pc.show_right_content("<div id=\"direct_routes_list\" class=\"content routes\">" + direct_routes_html.join("") + "</div>", function(x) {return x;});

                var as_viastop = new AutoSearch2({
                    input:pc._current_page_name + "_viastop_filter_input",
                    type:"viastop",
                    template:pt_autosearch_startend,
                    callback: function(stop_data) {
                        var visa_stop_filter = document.getElementById(pc._current_page_name + "_viastop_filter");
                        if(visa_stop_filter) {
                            visa_stop_filter.value = stop_data.stops.join(",");
                        }
                    }
                });

                //window.setTimeout(function () {
                //    var filter_button = document.getElementById(pc._current_page_name + "_filter_submit");
                //	filter_button.click();
                //}, 10);

            });


        }
        else {
            //pc.show_left_content("<div id=\"direct_routes_list\" class=\"content\">" + pc.i18n("lngNoDirectRoutes") + "</div>", function(str) {return str;});
            pc.show_right_content("<div id=\"direct_routes_list\" class=\"content\">" + pc.i18n("lngNoDirectRoutes") + "</div>", function(x) {return x;});
        }
    }, 10);
}


function switch_directions() {
    if(!("direct_routes" in pc._temp)) {
        return;
    }

    var tmp_start = ("start" in pc._temp.direct_routes) ? pc._temp.direct_routes.start : null;
    var tmp_finish = ("finish" in pc._temp.direct_routes) ? pc._temp.direct_routes.finish : null;

    pc._temp.direct_routes.finish = tmp_start;
    pc._temp.direct_routes.start = tmp_finish;

    if(!tmp_start && pc.as_end) {
        pc.as_end.fld.value = pc.as_end.fld.title || "";
    }
    if(!tmp_finish && pc.as_start) {
        pc.as_start.fld.value = pc.as_start.fld.title || "";
    }

    pt_direct_routes_stops();
}

function markStopAs(cfg) {
    if(!("direct_routes" in pc._temp)) {
        pc._temp.direct_routes = {};
        pc._temp.direct_routes_menu = {};
    }

    /* Jeigu pasirinktas start/finishas - ne ;search puslapyje - tai reiketu patikrinti ir permesti ji tenai... */
    //if(pc._current_page_name != "search") { // tokiu atveju manau reiketu panaikinti sena pasirinkima...
    //console.log("nakiname sena pasirinkima...");
    //    pc._temp.direct_routes = {};
    //}
    //console.log(pc._current_page_name);

    if ("start" in cfg) {
        pc._temp.direct_routes.start = cfg.start;
        if(pc.current_page_name != "route_search") {
            pc._temp.direct_routes_menu.start = true;
        }
    }
    if ("finish" in cfg) {
        pc._temp.direct_routes.finish = cfg.finish;
        if(pc.current_page_name != "route_search") {
            pc._temp.direct_routes_menu.finish = true;
        }
    }

    if(pc._current_page_name == "route_search") {
        pc._temp.direct_routes_menu = {}; // nerodysim start/end ikoneliu tab'e.

        //if ("start" in cfg) {
        //    pc.as_end.force_focus();
        //}
    }

    pc.reload_menu();
    pt_direct_routes_stops();

    if(pc.map_visible) {
        pc.map_show(function(){
            map = pc.getGMap();
            //map.checkResize();

            if(pc._temp.direct_routes.start) {
                var start_id = pc._temp.direct_routes.start[0];
                var st = pc.getSI().getStopData(start_id);
                var gpoint = new GLatLng(st.lat, st.lng);

                if (!(typeof(route_start_marker) == "undefined")) {
                    map.removeOverlay(route_start_marker);
                }

                var icon = new GIcon(G_DEFAULT_ICON);
                icon.image = _sub_dir + 'common/images/MarkerStart.gif';
                route_start_marker = new GMarker(gpoint, {icon:icon, inert:true});
                map.addOverlay(route_start_marker);
            }
            else if(!(typeof(route_start_marker) == "undefined")) {
                map.removeOverlay(route_start_marker);
            }


            if(pc._temp.direct_routes.finish) {
                var finish_id = pc._temp.direct_routes.finish[0];
                var st = pc.getSI().getStopData(finish_id);
                var gpoint = new GLatLng(st.lat, st.lng);

                if (!(typeof(route_end_marker) == "undefined")) {
                    map.removeOverlay(route_end_marker);
                }

                var icon = new GIcon(G_DEFAULT_ICON);
                icon.image = _sub_dir + 'common/images/MarkerEnd.gif';
                route_end_marker = new GMarker(gpoint, {icon:icon, inert:true});
                map.addOverlay(route_end_marker);
            }
            else if(!(typeof(route_end_marker) == "undefined")) {
                map.removeOverlay(route_end_marker);
            }


        });
    }
}

function get_directions() {
    if(!pc._temp.direct_routes) {
        pc._temp.direct_routes = {};
    }

    if(!(pc._temp.direct_routes.start)) {
        if(pc.as_start.aSug && pc.as_start.aSug.length) {
            //console.log(pc.as_start.aSug);
            var stops = [];
            var first_choice = pc.as_start.aSug[0];
            for(var i=0; i<first_choice.stops.length; i++) {
                stops.push(first_choice.stops[i].id);
            }
            sf_click_stops('start', stops);
        }
        else {
            document.getElementById("search_start_address").innerHTML = "<span style=\"color:red;font-size: 10px;\">(" + pc.i18n("lngStartStopNot") + ")</span>";
        }
    }
    if(!(pc._temp.direct_routes.finish)) {
        if(pc.as_end.aSug && pc.as_end.aSug.length) {
            var stops = [];
            var first_choice = pc.as_end.aSug[0];
            for(var i=0; i<first_choice.stops.length; i++) {
                stops.push(first_choice.stops[i].id);
            }
            sf_click_stops('finish', stops);
        }
        else {
            document.getElementById("search_end_address").innerHTML = "<span style=\"color:red;font-size: 10px;\">(" + pc.i18n("lngDestinationStopNot") + ")</span>";
        }
    }

    if(pc._temp.direct_routes && pc._temp.direct_routes.start && pc._temp.direct_routes.finish){
        pc.suspend_left(true);
        pc._force_reload = true;
        pc.hash(pc.url({city:pc.city, page:"route_search", args:[pc._temp.direct_routes.start.join(","), pc._temp.direct_routes.finish.join(",")]}));
        //old -> load_direct_routes({start_ids:pc._temp.direct_routes.start, finish_ids:pc._temp.direct_routes.finish});
    }

}

function check_days_all(input, event) {
    var day_input = document.getElementById(pc._current_page_name + "_day_filter_input");
    day_input.value = input.checked?"{1:1,2:1,3:1,4:1,5:1,6:1,7:1}":"{1:0,2:0,3:0,4:0,5:0,6:0,7:0}";
    show_dayfilter(input, event);
}

function check_day(input, day) {
    var day_input = document.getElementById(pc._current_page_name + "_day_filter_input");
    var days = eval("(" + day_input.value + ")");
    days[day] = input.checked?1:0;
    day_input.value = ObjectToString(days);
}

function show_dayfilter(input, event) {
    //console.log(input.checked);

    var days = eval("(" + document.getElementById(pc._current_page_name + "_day_filter_input").value + ")");
    //console.log(days);

    var transport_html = [pc.i18n("lngShowValidOn"), "<br/>"];
    for(var i=0; i<7; i++) {
        var check = days[i+1]; //selected_routes[tt][route_nmb];
        transport_html.push(["<span style=\"white-space:nowrap;\"><input style='margin:0px;' onclick=\"check_day(this, ", (i+1), ")\" name='cb' ", (check?"checked='checked'":""),
            " type='checkbox' value='", i+1, "'/> <span>", pc.i18n("lng" + (i+1) + "Day"), "</span></span><br/>"].join(""));
    }
    var html = transport_html.join("");

    fixedtooltip({width:250, html:html, obj:input, event:event, offset:[0,-2]}, 0);
}

function pt_stops_search_direct_routes(data) {
    var si = pc.getSI();

    var direct_routes_html = [];
    //var weekdays = ["12345", "67"];
    var weekdays = ["1234567"];

    var reports_html = [];
    for(var index=0; index<weekdays.length; index++) {
        var days = weekdays[index];

        var thead = ["<thead>"];
        if(index == 0) {
            var work_days = {1:1,2:1,3:1,4:1,5:1,6:1,7:1};

            var daybox = ["<input id=\"", pc._current_page_name, "_day_filter_input\" type=\"hidden\" value=\"", ObjectToString(work_days), "\"/><input id=\"i31052\" checked=\"true\" type=\"checkbox\" onclick=\"check_days_all(this,event)\" onmouseover=\"show_dayfilter(this, event);\" style=\"margin: 0px;\"/>"];

            thead.push(["<tr class=\"filter_row\"><td style=\"width: 5px;\" onmouseover=\"show_dayfilter(this, event);\">", daybox.join(""), "</td><td style=\"width: 10px;\">",
                "<input id=\"", pc._current_page_name, "_departure_filter_input\" type=\"text\" name=\"departure filter\" class=\"tlp-input-empty\" onblur=\"inputtest(this);\" onfocus=\"firstinput(this);\" title=\"", pc.i18n("lngDepart"), "\" value=\"", pc.i18n("lngDepart"), "\" size=\"12\" title=\"\"/>",
                "</td><td style=\"width: 10px;\"><input id=\"", pc._current_page_name, "_arrival_filter_input\" type=\"text\" name=\"arrival filter\" class=\"tlp-input-empty\" onblur=\"inputtest(this);\" onfocus=\"firstinput(this);\" title=\"", pc.i18n("lngArrive"), "\" value=\"", pc.i18n("lngArrive"), "\" size=\"8\" title=\"\"/></td><td></td><td style=\"width: 90%;\">",
                //"<input id=\"", pc._current_page_name, "_destination_filter_input\" type=\"text\" name=\"destination filter\" title=\"", pc.i18n("lngDestination"), "\" value=\"", pc.i18n("lngDestination"), "\" class=\"tlp-input-empty\" onblur=\"inputtest(this);\" onfocus=\"firstinput(this);\" size=\"20\" title=\"\"/>",

                "<input id=\"", pc._current_page_name, "_viastop_filter_input\" type=\"text\" name=\"destination filter\" class=\"tlp-input-empty\" title=\"", pc.i18n("lngMiddleStop"), "\" value=\"", pc.i18n("lngMiddleStop"), "\" size=\"20\" title=\"\"/>",
                "<input id=\"", pc._current_page_name, "_viastop_filter\" type=\"hidden\" value=\"\"/>",

                //"<input id=\"", pc._current_page_name, "_filter_submit\" onclick=\"filter([{filter:destination_filter, cell:4, input:'", pc._current_page_name, "_destination_filter_input'}, {filter:time_filter, cell:1, input:'", pc._current_page_name, "_departure_filter_input'}, {filter:arrival_filter, cell:2, input:'", pc._current_page_name,"_arrival_filter_input'}, {filter:day_filter, cell:0, input:'", pc._current_page_name, "_day_filter_input'}],true);\" type=\"submit\" value=\"", pc.i18n("lngFilter"), "\" name=\"filter\"/>",
                "<input id=\"", pc._current_page_name, "_filter_submit\" onclick=\"filter([{filter:viastop_filter, cell:0, input:'", pc._current_page_name, "_viastop_filter_input'}, {filter:time_filter, cell:1, input:'", pc._current_page_name, "_departure_filter_input'}, {filter:arrival_filter, cell:2, input:'", pc._current_page_name,"_arrival_filter_input'}, {filter:day_filter, cell:0, input:'", pc._current_page_name, "_day_filter_input'}],true);\" type=\"submit\" value=\"", pc.i18n("lngFilter"), "\" name=\"filter\"/>",
                "<input onclick=\"showall(true);\" type=\"submit\" value=\"", pc.i18n("lngShowAll"), "\" name=\"showall\"/>",
                "</td></tr>"].join(""));
        }

        thead.push(["<tr class=\"header_row\"><td colspan=\"5\" id=\"", pc._current_page_name, "_days_header\">", parseDaysAbb(days), "</td></tr><tr class=\"header_row\"><th style=\"width:5px;\"></th><th style=\"width:10px;\">",
            pc.i18n("lngDeparture"), "</th><th style=\"width:10px;\">", pc.i18n("lngArrival") ,"</th><th style=\"width:10px;\">", pc.i18n("lngDuration"), "</th><th style=\"width:90%;\">",
            pc.i18n("lngRoute"), "</th></tr></thead>"].join(""));

        var hours = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30];
        var hours_html = [];

        var tbody = ["<tbody>"];
        for (var i=0; i<hours.length; i++) { // kiekviena valanda
            var hour = hours[i];

            var race_times = [];
            var day_list = days.split(""); // 1,2,3,4,5 arba 6,7
            for(var k=0; k<day_list.length;k++) {
                var day_schedule = data.stop_schedule_byhour[day_list[k]]; // konkrechios dienos tvarkarashtis
                if (hour in day_schedule) {
                    for(var m=0; m<day_schedule[hour].length;m++) race_times.push(day_schedule[hour][m]); // susirenkame visus einamosios valandos marsrutus
                }
            }

            //race_times = race_times.unique(); // pashaliname identishkus duomenis... (kai pvz vazhiuoja visom darbo dienom tas pats)
            race_times = race_times.unique(function (a,b) {
                if((a.direction_id == b.direction_id) && (a.race_index == b.race_index) && (a.minute == b.minute)) return true;
                return false;
            }); // pashaliname identishkus duomenis...

            race_times.sort(function(x,y) {return x.minute - y.minute}); // ishrushiuojame pagal laika

            if(race_times.length) {

                for(var j=0; j<race_times.length; j++) { // per kiekviena minute
                    var x = race_times[j];

                    if(x.end) continue; // nerodom laiku paskutineje stoteleje?, chia x = visada isvykimo stotele, todel ok

                    //console.log("x.end? : ", x.end);

                    //var hour_html = (j==0) ? (((hour>23)?(hour-24):hour) + ":"):"";
                    //var hour_html = ((hour>23)?(hour-24):hour) + ":";
                    //var hour_html = (hour%24) + ":";

                    //var minute_html = hour_html + ((x.minute<10)?("0" + x.minute):x.minute);

                    var finish_id = data.start_finish_hash[x.direction_id].finish_id;
                    var start_id = data.start_finish_hash[x.direction_id].start_id;

                    var minute_html = ["<a style=\"cursor:pointer;\" onclick=\"load_racetime(", x.direction_id, ",",
                        start_id, ",", x.race_index, ");\">", (hour%24), ":", ((x.minute<10)?("0" + x.minute):x.minute),
                        "</a>"].join("");

                    var route_data = pc.getSI().getRouteData(x.direction_id);
                    var city = pc.chooseCity(route_data);

                    var finish_index = route_data.stops.lastIndexOf(finish_id); // gauname finisho stoteles indexa

                    var arrival_data = data.direction_schedule_list[x.direction_id][finish_index][x.race_index];
                    var arrival_hour = arrival_data.time.hour;
                    var arrival_minute = arrival_data.time.minute;

                    //var arrival_time_html = ((arrival_hour>23)?(arrival_hour-24):arrival_hour) + ":" + ((arrival_minute<10)?("0" + arrival_minute):arrival_minute);

                    var arrival_time_html = ["<a style=\"cursor:pointer;\" onclick=\"load_racetime(", x.direction_id, ",",
                        finish_id, ",", x.race_index, ");\">", arrival_hour, ":", ((arrival_minute<10)?("0" + arrival_minute):arrival_minute),
                        "</a>"].join("");

                    var color = pc.getTransportData(route_data.transport).color;

                    var hour_change = (j==0)?("border-top:4px solid #DDD;"):("");
                    var nmb_class = (route_data.route_nmb.length==1)?"number1":((route_data.route_nmb.length==2)?"number":"number3");

                    /* isrenkam tik reikiamas dienu raides */
                    if(x.days.indexOf(days) != -1)  {
                        var x_days = "";
                    }
                    else {
                        var my_days = [];
                        for(var z=0; z<day_list.length; z++) {
                            var d = day_list[z];
                            if(x.days.indexOf(d) != -1) {
                                my_days.push(d);
                            }
                        }
                        var x_days = parseDaysAbb(my_days.join(""), true);
                        //console.log(route_data.route_nmb, "> ", x.days, " : ", parseDaysAbb(x.days, true), " : ", days, " my: ", my_days.join(""), " ", parseDaysAbb(my_days.join(""), true));
                    }
                    /* isrenkam tik reikiamas dienu raides */


                    tbody.push(["<tr title=\"", route_data.transport, ":", route_data.route_nmb, "\" class=\"data_row\" style=\"", hour_change, "\">",
                        //"<input type=\"hidden\" value=\"", route_data.stops.join(","), "\"\>",
                        //"<th style=\"text-align:right;\">", hour_html, "</th>",
                        "<td class=\"d", (x_days?my_days.join(""):days) ,"\"style=\"text-align:center;font-size:10px;\">",
                        x_days || ("<span style=\"color:grey;\">" + pc.i18n("lngDaily") + "</span>"),
                        "<input type=\"hidden\" value=\"", route_data.stops.join(","), "\"\>",
                        "</td>",
                        "<td style=\"text-align:center;\">", minute_html, "</td>",
                        "<td style=\"text-align:center;\">", arrival_time_html, "</td>",
                        "<td style=\"text-align:center;\">", printTimeHM(subTime(arrival_data.time, {hour:hour, minute:x.minute})) , "</td>",
                        "<td style=\"white-space:nowrap;\" class=\"link\"><img title=\"", pc.i18n("lngRouteInfo"), "\" onclick=\"showRouteInfo('",x.direction_id, "','right',this,event)\" style=\"cursor:pointer; margin:2px 2px -3px 2px;\" class=\"img_info\" src=\"common/images/info.gif\" width=\"13\" height=\"15\"/>",
                        "<span><a href=\"",
                        pc.url({city:city, page:"schedule", args:[route_data.transport, x.direction_id, start_id]}), "\" class=\"number_link\" style=\"cursor:pointer;padding-left:3px;padding-right:0px;\">",
                        "<b class=\"", nmb_class, "\" style='line-height:1.5;background-color:", color, "'>", route_data.route_nmb, "</b>",
                        "</a></span>",


                        "<a href=\"",
                        pc.url({city:city, page:"schedule", args:[route_data.transport, x.direction_id, start_id]}), "\" style='padding:0 0px;color:",
                        color, ";'>", route_data.direction_name,
                        "</a></td></tr>"].join(""));
                }
            }
        }
        tbody.push("</tbody>");

        if(tbody.length > 2) {
            reports_html.push("<table id=\"" + pc._current_page_name + "_table_" + days + "\" style=\"width:100%;\" class=\"timetable\">" + thead.join("") + tbody.join("") + "</table><br/>");
        }
    }

    reports_html.push(pc.i18n("lngInfo") + "<br/>" + pc.i18n("lngClickTime"));
    return reports_html.join("");
}

/* Grazhina stoteleje stojanchio transporto sarasho html'a , marsrutai su route_tag '0' arba '-' nerodomi. */

function pt_transport_in_stop_html(cfg) {
    var width = cfg.width;
    if("search" in cfg) {
        var stop_data = pc.as_stopsearch.arr[cfg.search.index];
        //var stop_data = (cfg.search.type == "start") ? pc.as_start.arr[cfg.search.index] : pc.as_end.arr[cfg.search.index];

        //console.log("stop_data: ", stop_data);
        //var transport_obj = stop_data.transport;
        var transport_obj = OldStopsInfo.getTransportInStops(stop_data.stops);

    }
    else {
        var stop_data = cfg.stop;
        //var transport_obj = stop_data.temp;
        var stop_id = stop_data.id;
        var transport_obj = OldStopsInfo.getTransportInStops([stop_id]); //stop_data.temp;
    }

    //console.log(transport_obj);

    var transport_html = [];
    var transport_types = pc.getAllTransportTypes();

    var max_width_break = false;
    for(var tid in transport_types) {
        if (!(tid in transport_obj)) continue;
        if (stop_data.transport_byroute_tag && !(tid in stop_data.transport_byroute_tag)) continue; // pakeiteme ish .temp, kad prafiltruoti tuos kurie turi route_tag=0 arba -

        var color = transport_types[tid].color;

        var numspan_array = []; // [ [num, html] ] // for sorting

        var sorted_routes = [];
        for(var rid in transport_obj[tid]) { //stop_data.transport_byroute_tag[tid]
            if(stop_data.transport_byroute_tag && !(rid in stop_data.transport_byroute_tag[tid])) continue;
            sorted_routes.push([parseInt("0"+rid, 10), rid]);
        }
        sorted_routes.sort(function(x,y) {return (x[0] - y[0]);});

        for(var i=0; i<sorted_routes.length; i++) {
            var rid = sorted_routes[i][1];

            if(stop_data.transport_byroute_tag && !(rid in stop_data.transport_byroute_tag[tid])) continue;

            if("search" in cfg) {
                //console.log("xx: ", transport_obj[tid][rid]);
                var first_direction_id = transport_obj[tid][rid].direction_id;
                var stop_id = transport_obj[tid][rid].stop_id;
            }
            else {
                var first_direction_id = transport_obj[tid][rid].directions[0];
            }
            var city = pc.chooseCity(pc.getSI().getRouteData(first_direction_id));

            //console.log(pc.url({city:city, page:"schedule", args:[tid, first_direction_id, stop_id]}));

            numspan_array.push(["<a href=\"", pc.url({city:city, page:"schedule", args:[tid, first_direction_id, stop_id]}),
                "\" class=\"number_link\"><span class='number_white' style='color:", color,
                "' onclick=\"hidetip();\" onmouseover=\"showStopTransportX('", stop_data.id, "','",
                first_direction_id, "','", (cfg.parent_id)?cfg.parent_id:"left", "',1,this, event)\">", rid, "</span></a> "].join(""));

            if(width) {
                var row_length = calculateContentSize(transport_html.join("")).width + calculateContentSize(numspan_array.join("")).width;
                if(row_length >= width) {
                    numspan_array.pop();
                    var muli_dot_html = ["<b onclick=\"showStopInfo({",
                        ("search" in cfg) ? ("search:{type:'" + cfg.search.type + "',index:'" + cfg.search.index + "'}") : ("stop_id:'" + stop_data.id + "'"),
                        ",layer_name:'left', obj:this, event:event})\" style=\"margin-left:-4px;cursor:pointer;\">...</b>"].join("");
                    numspan_array.push(muli_dot_html);
                    max_width_break = true;
                    break;
                }
            }
        }
        transport_html.push(numspan_array.join(""));

        if(max_width_break) break;

    }

    return transport_html.join("");
}

function pt_left_shedule_change(cfg) {

    var route_data = pc.getSI().getRouteData(cfg.direction_id);
    var city_url = pc.chooseCity(route_data);
    //var transport_type = pc.getSI()._routes_data[cfg.direction_id].transport;
    var stoplist = route_data.stops;

    var stops_html = [];

    // expandas chia tik tam, kad po collapse/expand butu vel rodomas reiso laikas
    if((pc._temp.expand_data.direction_id == cfg.direction_id) &&
        (typeof(pc._temp.expand_data.race_index) != "undefined") &&
        (pc.getSI().isCached(cfg.direction_id))) {

        var schedule_data = pc.getSI().getScheduleObj(cfg.direction_id, 1);
        stops_html.push("<tr><td></td><td></td><td style='border: 1px solid black;text-align: center;'>" + parseDaysAbb(schedule_data.workdays[cfg.direction_id][pc._temp.expand_data.race_index]) + "</td><td></td></tr>");
    }

    for (var i=0;i<stoplist.length;i++)
    {
        var stop_data = pc.getSI()._stops_data[stoplist[i]];
        var id = stop_data.id;

        var bottom_border = "";
        if (i == stoplist.length-1) {
            bottom_border = "border-bottom:1px solid black;"
        }

        var name = stop_data.name;
        //var show_stop_id = ((cfg.stop_id)&&(id == cfg.stop_id)) || ((pc._temp.expand_data.direction_id == cfg.direction_id) && (pc._temp.expand_data.stop_id == id));
        var show_stop_id = ((pc._temp.expand_data.direction_id == cfg.direction_id) && (pc._temp.expand_data.stop_id == id));

        var output = ["<a class=\"stop\" href=\"", pc.url({city:city_url, page:"schedule", args:[route_data.transport, cfg.direction_id, id]}), "\">", name, "</a>"].join("");

        var class_name = [];
        if(!(i%2)) class_name.push("odd");
        if(show_stop_id) class_name.push("gold");

        stops_html.push(["<tr class=\"", class_name.join(" "), "\">",
            "<td style=\"width:12px;\"><img onclick=\"showStopInfo({stop_id:'", id, "',layer_name:'", (cfg.layer?cfg.layer:"schedule_result"), "',obj:this,event:event,show_header:true})\" class=\"img_info\" style=\"cursor:pointer;margin:0px 3px -3px 0px;\" src=\"common/images/info.gif\" width=\"13\" height=\"15\"/>",
            "</td><td style='cursor:pointer;'",
            " onclick=\"highlight_stopname(", cfg.direction_id, ",", id, ",this); ",
            "pc.hash('", pc.url({city:city_url, page:"schedule", args:[route_data.transport, cfg.direction_id, id]}), "')\"",
            ,">",
            output, "</td>"].join(""));


        if((pc._temp.expand_data.direction_id == cfg.direction_id) &&
            (typeof(pc._temp.expand_data.race_index) != "undefined") &&
            (pc.getSI().isCached(cfg.direction_id))) {

            var schedule_data = pc.getSI().getScheduleObj(cfg.direction_id, 1);

            var race_time_html = "<td></td>";
            var race_data = schedule_data.direction_schedule_list[cfg.direction_id][i];

            var hour = race_data[pc._temp.expand_data.race_index].time.hour;
            var minute = race_data[pc._temp.expand_data.race_index].time.minute;
            var time = "";
            if(hour != -1) {
                var time = ((hour==0)&&(minute==0))? (""):((hour%24) + ":" + ((minute<10)?("0" + minute):minute));
            }
            race_time_html = ["<td style='", bottom_border, "text-align: center;border-left:1px solid black;border-right:1px solid black;cursor:pointer;'",
                " onclick=\"highlight_stopname(", cfg.direction_id, ",", id, ",this); ",
                "pc.hash('", pc.url({city:city, page:"schedule", args:[route_data.transport, cfg.direction_id, id]}), "')\"",
                ,">", time, "</td>"].join("");
            stops_html.push(race_time_html);
        }

        if(pc.show_transport && (!cfg.layer)) { // nerodome kai norime parodyti tik laikus kaireje, tada buna layer
            if(pc._current_page_name == "schedule") {
                var parent_id	= "schedule_result";
            }
            else if(pc._current_page_name == "search") {
                var parent_id	= "search_result";
            }
            else if(pc._current_page_name == "route_search") {
                var parent_id	= "search_start_result";
            }

            stops_html.push("<td style=\"width:275px;\"><div style=\"width:275px;\">" + pt_transport_in_stop_html({stop:stop_data, parent_id:parent_id}) + "</div></td>");
        }
        stops_html.push("</tr>");
    }

    return ["<table style=\"width:463px;\">", stops_html.join(""), "</table>"].join("");
}



function pt_left_shedule_time(cfg) {
    var route_data = pc.getSI().getRouteData(cfg.direction_id);
    var city_url = pc.chooseCity(route_data);

    var stoplist = route_data.stops;

    var stops_html = [];

    if(pc.getSI().isCached(cfg.direction_id)) {
        var data = pc.getSI().getScheduleObj(cfg.direction_id, 1);

        if (cfg.direction_id in data.direction_schedule) {
            var workdays = data.workdays[cfg.direction_id];
            var day_change_cells = [];

            if(cfg.race_index) {
                // TOKIO jau buti gi negali....	tam naudojamas pt_left_shedule_change
                //stops_html.push("<tr><td></td><td>" + parseDaysAbb(workdays[cfg.race_index]) + "</td></tr>");
            }
            else {
                var cells_html = [];
                if(workdays.length) {
                    var previous_days = "";
                    var colspan = 1;
                    var cells = [];

                    for(var i=0; i<workdays.length; i++) {
                        if(previous_days == "") {
                            previous_days = workdays[i];
                        }
                        else if (workdays[i] != previous_days) {
                            day_change_cells.push(i); // Tokioms dienoms rodysime left borderi
                            cells.push({days: previous_days, colspan: colspan});
                            previous_days = workdays[i];
                            colspan = 1;
                        }
                        else {
                            colspan += 1;
                        }

                        if(i+1 == workdays.length) {
                            cells.push({days: workdays[i], colspan: colspan});
                        }

                    }

                    for(var j=0; j<cells.length; j++) {
                        cells_html.push(["<td style='border: 1px solid black;'", (cells[j].colspan>1 ? (" colspan='" + cells[j].colspan + "'"): ""), ">", parseDaysAbb(cells[j].days), "</td>"].join(""));
                    }
                }

                stops_html.push("<tr><td></td><td style='border-right: 1px solid black;'></td>" + cells_html.join("") + "</td><td style='border-left: 1px solid black;'></td></tr>");
                //stops_html.push("<tr><td></td><td>" + workdays.join("</td><td>") + "</td></tr>");
            }

            var stops = cfg.filter ? cfg.filter : stoplist;

            for (var i=0;i<stops.length;i++) {
                var stop_data = pc.getSI()._stops_data[stops[i]];

                var name = stop_data.name;
                var id = stop_data.id;

                var bottom_border = "";
                if (i == stops.length-1) {
                    bottom_border = "border-bottom:1px solid black;"
                }

                var transport_html = [];


                var race_data = data.direction_schedule_list[cfg.direction_id][cfg.filter?stoplist.indexOf(id):i];// reikia palikti:i nes buna kad kartojasi stotele...

                var race_times = [];

                for(var j=0; j<race_data.length; j++) {
                    if(cfg.race_index && (cfg.race_index != race_data[j].race_index)) continue;

                    //console.log(race_data[j]);

                    var hour = race_data[j].time.hour;
                    var minute = race_data[j].time.minute;
                    var time = "";
                    if(hour != -1) {
                        var time = ((hour==0)&&(minute==0))?(""):((hour%24)+ ":" + ((minute<10)?("0" + minute):minute));
                    }

                    var border_style = "";
                    if(day_change_cells.indexOf(j) != -1) {
                        border_style = "border-left:1px solid black;";
                    }
                    var right_border = "";
                    if(j == race_data.length-1) {
                        right_border = "border-right:1px solid black;";
                    }

                    race_times.push("<td style=\"text-align: center;" + bottom_border + border_style + right_border + "\">" + time + "&nbsp;&nbsp;</td>");
                }

                transport_html.push(race_times.join(""));

                if(!cfg.filter && pc.show_transport) {
                    transport_html.push("<td style=\"" + (pc._temp.race_time_show ? "white-space:nowrap;":"") + "\">");
                    transport_html.push(pt_transport_in_stop_html({stop:stop_data, parent_id:"schedule_result"}));
                    transport_html.push("</td>");
                }
                else {
                    //transport_html.push("<td style=\"border-left:1px solid black;\"></td>");
                }

                var place_html = "";
                if(cfg.filter) {
                    var area_street = [];
                    if (stop_data.area != "0") area_street.push(stop_data.area);
                    if (stop_data.street != "0") area_street.push(stop_data.street);
                    place_html = " <span style=\"font-size: 10px;\">(" + area_street.join(", ") + ")</span>";
                }

                //var show_stop_id = (cfg.stop_id)&&(id == cfg.stop_id);
                var show_stop_id = ((pc._temp.expand_data.direction_id == cfg.direction_id) && (pc._temp.expand_data.stop_id == id));


                var output = ["<a class=\"stop\" href=\"", pc.url({city:city_url, page:"schedule", args:[route_data.transport, cfg.direction_id, id]}), "\">", name, place_html, "</a>"].join("");
                stops_html.push(["<tr",
                    (show_stop_id?(" style=\"background-color:gold;font-weight:bold;\""):("")),
                    ((!(i%2))?" class=\"odd\"":""), "><td style=\"width:12px;\"><img onclick=\"showStopInfo({stop_id:'", id, "',layer_name:'schedule_result',obj:this,event:event,show_header:true})\" style=\"cursor:pointer;margin:0px 3px -3px 0px;\" class=\"img_info\" src=\"common/images/info.gif\" width=\"13\" height=\"15\"/>",
                    "<td style='border-right:1px solid black;white-space:nowrap;cursor:pointer;'",
                    " onclick=\"", ((pc._temp.race_time_show) ? "":("highlight_stopname(" + cfg.direction_id + "," + id + ",this);")) ,
                    " pc.hash('", pc.url({city:city_url, page:"schedule", args:[route_data.transport, cfg.direction_id, id]}), "')\"",
                    ,">",
                    output, "</td>", transport_html.join(""), "</tr>"].join(""));

            }
        }
    }

    return ["<table>", stops_html.join(""), "</table>"].join("");
}

function pt_loading() {
    return ["<div class=\"content\">", pc.i18n("lngLoading"), "</div>"].join("");
}

function pt_route_comments(comments) {
    var rvalue = false;
    if (comments) {
        rvalue = comments.map(function (x) {return "<tr><td style=\"line-height:10pt;\"><span style=\"padding-left:0px;\" class=\"result_days\">" + ((x.date_from&&x.date_to) ? (f_tcalGenerDate(x.date_from, ".") + " - " + f_tcalGenerDate(x.date_to, ".") + " "):"") + x.comment + "</span></td></tr>";}).join("");
    }
    return rvalue;
}

function pt_schedule_routes(args) {
    var si = args.si;

    var schedule_html_list = ["<div id=\"schedule_list\" style=\"padding-top:4px;\" class=\"inner content routes\"><ul>"];


    schedule_html_list.push(["<li class=\"filter_row\"><input id=\"", pc._current_page_name, "_title_filter_input\" type=\"text\" name=\"title filter\" title=\"", pc.i18n("lngTypeNumberCity"), "\" value=\"", pc.i18n("lngTypeNumberCity"), "\" class=\"tlp-input-empty\" onblur=\"inputtest(this);\" onfocus=\"firstinput(this);\" size=\"50\" title=\"\"/>",
        "<!--input id=\"", pc._current_page_name, "_filter_submit\" onclick=\"filter_routes([{filter:title_filter, input:'", pc._current_page_name, "_title_filter_input'}]);\" type=\"submit\" value=\"", pc.i18n("lngFilter"), "\" name=\"filter\"/>",
        "<input onclick=\"showall(true);\" type=\"submit\" value=\"", pc.i18n("lngShowAll"), "\" name=\"showall\"/-->",
        "</li>"].join(""));


    var prev_route_nmb = "";
    var prev_route_data = {route_nmb:"", weekdays:[]};
    var line_count = 0;
    var transport = args.transport; //args[0];
    var color = pc.getTransportData(transport).color;
    var report = pc.getTransportData(transport).report;

    function route_info_html(route_data) {
        var route_weekdays = route_data.weekdays.unique(); //.sort();
        var week = ["1","2","3","4","5","6","7"];
        var weekdays_html_list = []
        for(var i=0; i<week.length; i++) {
            var day = week[i];
            if(day == "6") { weekdays_html_list.push("<span style=\"margin-left: 0px;\"> </span>");}

            var day_html = pc.i18n("lng" + day + "Day");
            if(route_weekdays.indexOf(day) != -1) {
                weekdays_html_list.push("<font title=\"", (day_html + ": " + pc.i18n("lngTimeTables")) ,"\" style=\"font-size:10px;font-family:'Lucida Console', courier, monospace;\" class=\"weekday" + ((parseInt(day,10)>5)?"2":"1") + "\">" + day_html.charAt(0) + "</font>");
            }
            else {
                weekdays_html_list.push("<font title=\"", (day_html + ": " + pc.i18n("lngNotOperate")) ,"\" style=\"font-size:10px;font-family:'Lucida Console', courier, monospace;\" class=\"weekday0\">&nbsp;</font>");
            }
        }
        line_count +=1;

        var nmb_class = (route_data.route_nmb.length==1)?"number1":((route_data.route_nmb.length==2)?"number":"number3");


        var arg = [transport, route_data.direction_id, route_data.stop_id];
        if(report) { arg.push("time"); }
        var url = pc.url({page:"schedule", args:arg});

        return ["<li title=\"", [route_data.route_nmb, ": ", si._routes_data[route_data.direction_id].direction_name].join("") ,"\" class=\"", ((line_count%2)?"odd data_row":"data_row"), "\"><span><a class=\"number_link\" href=\"",
            url, "\">", weekdays_html_list.join(""),
            "</a></span>", "<img title=\"", pc.i18n("lngRouteInfo"), "\" onclick=\"showRouteInfo('", route_data.direction_id,
            "','schedule_result',this,event)\" style=\"cursor:pointer; margin:0px -1px -3px 3px;\" class=\"img_info\" src=\"common/images/info.gif\" width=\"13\" height=\"15\"/>",
            "<span title=\"", pc.i18n("lngTimeTables"), "\"><a class=\"number_link\" href=\"", url,
            "\"><b class=\"", nmb_class, "\" style='background-color:", color, "'>", route_data.route_nmb, "</b></a></span>",
            "<a title=\"", pc.i18n("lngTimeTables"), "\" href=\"", url, "\" style='color:", color, ";'>",
            si._routes_data[route_data.direction_id].direction_name, "</a></li>"].join("");
    }

    var unsorted_routes = [];
    // collect all routes from current city
    for (var direction_id in si._routes_data) {
        var cities = si._routes_data[direction_id].cities; //.city.split(",");

        //if(si._routes_data[direction_id].route_nmb == "103A") {
        //    console.log(si._routes_data[direction_id]);
        //}

        if((cities.indexOf(pc.city) != -1) && (si._routes_data[direction_id].transport == transport)) {
            var route_nmb = si._routes_data[direction_id].route_nmb;
            var first_stop = si._routes_data[direction_id].stops[0]
            unsorted_routes.push([route_nmb, direction_id, first_stop]);
        }
    }
    // sort by route number
    unsorted_routes.sort(function(a,b) {
        return str_int_sort(a[0], b[0]);
    });

    // now unsorted_routes is already sorted, must remove directions with same route_nmb
    for(var j=0; j<unsorted_routes.length; j++) {
        var route_nmb = unsorted_routes[j][0];
        var direction_id = unsorted_routes[j][1];
        var stop_id = unsorted_routes[j][2]

        //alert(route_nmb);

        if (prev_route_data.route_nmb != route_nmb) {
            if(prev_route_data.route_nmb) {
                schedule_html_list.push(route_info_html(prev_route_data));
            }
            var weekdays = si._routes_data[direction_id].weekdays.split("");
            prev_route_data = {route_nmb:route_nmb, weekdays:weekdays, direction_id:direction_id, stop_id:stop_id};
        }
        else {
            var route_data = si._routes_data[direction_id];

            if(route_data.route_type == "A>B") { // opera fix
                prev_route_data.direction_id = direction_id;
                prev_route_data.stop_id = stop_id;
            }

            var weekdays = route_data.weekdays.split("");
            prev_route_data.weekdays = prev_route_data.weekdays.concat(weekdays);
        }
    }
    // idedame paskutini marsruta  - ciklo pabaigoje
    if(prev_route_data.route_nmb != "") {
        schedule_html_list.push(route_info_html(prev_route_data));
    }

    schedule_html_list.push("</ul></div>");
    return schedule_html_list.join("");
}


function loadScheduleTime(args) {
    var route_nmb_arg = args.si._routes_data[args.direction_id].route_nmb;
    var directions = args.si._transport_data[args.transport][route_nmb_arg];

    var arg = {
        args: args,
        directions: directions,
        directions_stack: directions.slice(0, directions.length),
        direction_id:args.direction_id,
        workdays:{},
        direction_schedule:{},
        direction_schedule_list:{},
        valid_from: 1
    }

    //pc.show_left_content(pt_loading(), function(x){return x;});
    pc.show_left_content({}, pt_schedule_top);
    //pc.show_content("schedule_result", pt_schedule_change({transport:args[0], direction_id:args[1], stop_id:args[2], si:si, template:pt_left_shedule_change}), "transport_menu_tag");
    //console.log("valid: ", arg.valid_from);

    arg.download = true;
    pc.getSI().executeOnScheduleList(arg, function(data) {
        //console.log("xx: ", arg.valid_from);
        data.args.template = pt_left_shedule_time;
        //pc.show_left_content(data.args, pt_schedule_time);
        pc.show_content("schedule_result", pt_schedule_time(data.args), "transport_menu_tag");
    });
}


function updateReportTable(init) {
    var stop_id = pc._temp.expand_data.stop_id;
    var direction_id = pc._temp.expand_data.direction_id;


    var timetable = null;
    if(init) {
        timetable = document.createElement('div');
        timetable.id = "timetable";
        timetable.className = "content";
        var container_rcontent = document.getElementById(pc._current_page_name + "_rcontent");
        container_rcontent.appendChild(timetable);
    }
    else {
        timetable = document.getElementById("timetable");
    }
    timetable.innerHTML = pc.i18n("lngLoading");
    //alert("Loading timetable...");

    window.setTimeout(function () {
        var directions = [];
        var selected_routes = pc._temp.schedule_selected_routes;
        for(var tt in selected_routes) {
            for(var route_nmb in selected_routes[tt]) {
                if(selected_routes[tt][route_nmb]) {
                    var new_directions = pc.getSI()._stops_data[stop_id].temp[tt][route_nmb];
                    directions = directions.concat(new_directions);
                }
            }
        }

        if(!directions.length) {
            updateTimeTable(stop_id, direction_id);
        }
        else {
            var arg = {
                directions: directions,
                directions_stack: directions.slice(0, directions.length), // reiketu nukelti i f-cijos vidu...
                stop_id:stop_id,
                workdays:{},
                direction_schedule:{},
                direction_schedule_list:{},
                stop_schedule_byhour:{"1":{}, "2":{}, "3":{}, "4":{}, "5":{}, "6":{}, "7":{}},
                stop_schedule_byhour_all: {} // "123": {}...
                ,valid_from :1
            }

            pc.getSI().executeOnScheduleList(arg, function(data) {
                timetable.innerHTML = pt_report_timetable(data);
            });


            //console.log(document.getElementById("schedule_destination_filter_input"));
            var as_viastop = new AutoSearch2({
                input:pc._current_page_name + "_viastop_filter_input",
                type:"viastop",
                template:pt_autosearch_startend,
                callback: function(stop_data) {
                    //console.log("stop_data: ", stop_data);
                    var visa_stop_filter = document.getElementById(pc._current_page_name + "_viastop_filter");
                    if(visa_stop_filter) {
                        visa_stop_filter.value = stop_data.stops.join(",");
                    }
                }
            });

        }
    },10);

}


// pagal direction_id galima gauti route_num ir tada susirinkti visus direction_id ish to route_num
function updateTimeTable(stop_id, direction_id, init, interval) {
    //console.log("updateTimeTable: ",stop_id, direction_id, init);

    var directions = [];
    var opposite_directions = [];

    var timetable = null;
    if(init) {
        timetable = document.createElement('div');
        timetable.id = "timetable";
        timetable.className = "content";
        var container_rcontent = document.getElementById(pc._current_page_name + "_rcontent");
        container_rcontent.appendChild(timetable);
    }
    else {
        timetable = document.getElementById('timetable');
    }
    timetable.innerHTML = pc.i18n("lngLoading");

    if(direction_id) {
        directions.push(direction_id);
    }
    else { // paspaustas checkboxas - reikia susirinkti duomenis
        var routes_filter = document.getElementById(pc._current_page_name + "_transport_filter");
        var routes_obj = eval("(" + routes_filter.value + ")");
        var stop_data = pc.getSI().getStopData(stop_id);

        for(var transport in routes_obj) {
            for(var route_nmb in routes_obj[transport]) {
                if(routes_obj[transport][route_nmb]) {
                    directions.push(stop_data.temp[transport][route_nmb][0]); // pirmas pasitaikes direction_id - kitus veliau susirinksime
                }
            }
        }
    }


    if (directions.length == 0) {
        //var timetable = document.getElementById('timetable');
        timetable.innerHTML = "<tr></tr>";
    }
    else {
        // dabar chia reikia susirinkti ir kitus direction_id stojanchius toje stoteleje ir turinchius tokius pachius route_num kaip ir tie kurie pazhymeti.

        var one_transport = true; // only one kind of transport stops (x.transport==transport&&x.direction_nmb==direction_nmb).

        if (!direction_id && (directions.length == 1)) {
            direction_id = directions[0]; // vienas checkboxas
        }
        if (directions.length > 1) {
            one_transport = false;
        }

        var all_directions = directions.slice(0, directions.length);


        for(var i=0; i<directions.length; i++) {
            var idirection_id = directions[i];
            var is_opposite = (opposite_directions.indexOf(idirection_id) != -1);

            var route_nmb = pc.getSI()._routes_data[idirection_id].route_nmb;
            var transport_type = pc.getSI()._routes_data[idirection_id].transport;

            //console.log("transport: ", OldStopsInfo.getTransportInStops([stop_id])[transport_type][route_nmb]);
            //var transport = pc.getSI()._stops_data[stop_id].temp[transport_type][route_nmb];
            var transport = OldStopsInfo.getTransportInStops([stop_id])[transport_type][route_nmb].directions;
            // gauname visus tokio numerio transportus

            var main_route = pc.getSI()._routes_data[idirection_id].route_type.split(">");

            for(var j=0; j<transport.length; j++) {
                if(transport[j] != idirection_id) {
                    var side_route = pc.getSI()._routes_data[transport[j]].route_type.split(">");
                    if (isSimilar(main_route, side_route)) {
                        if(is_opposite) {
                            opposite_directions.push(transport[j]);
                        }
                        all_directions.push(transport[j]);
                    }
                }
            }
        }

        //console.log("directions: ", directions);
        //console.log("all_directions: ", all_directions);

        var arg = {
            directions: directions, // direction_id - vinas prie vieno su route_nmb
            directions_stack: all_directions, //filtered_directions, // cia jau galia buti keli direction_id atitinkantys route_nmb
            stop_id:stop_id,
            direction_id:(one_transport?direction_id:0),
            opposite_directions: opposite_directions,
            workdays:{},
            direction_schedule:{},
            direction_schedule_list:{},
            //stop_schedule_byhour:{"12345":{}, "6":{}, "7":{}}
            stop_schedule_byhour:{"1":{}, "2":{}, "3":{}, "4":{}, "5":{}, "6":{}, "7":{}},
            stop_schedule_byhour_all: {}, // "123": {}...
            valid_from:1
        }


        pc.getSI().executeOnScheduleList(arg, function(data) {
            timetable.innerHTML = pt_timetable(data, interval);
        });
    }
}

/*
 function filter_toggle(el) {
 var node = document.getElementById(pc._current_page_name + "_rcontent");
 var filter_rows = getElementsByClassName("filter_row", node);

 for(var i=0; i<filter_rows.length; i++) {
 filter_rows[i].style.display = (el.checked) ? "" : "none";
 }
 }
 */

function arrival_filter(data_rows, input, cell_index) {
    var input_el = document.getElementById(input);
    if(input_el.className == "tlp-input-empty") return data_rows;

    var filter_value = input_el.value;
    if (!filter_value) return data_rows;

    var filter_time = parseTime2(filter_value);
    if(filter_time.hour==-1 || filter_time.minute==-1) {
        return data_rows;
    }

    var fits = [];
    for(var i=0; i<data_rows.length; i++) {
        var cell = data_rows[i].cells[cell_index];
        var cell_content = toascii(cell.textContent || cell.innerText);

        var indexOf = cell_content.indexOf(":");
        var cell_time = parseTime(cell_content.substr(0,indexOf) + cell_content.substr(indexOf+1));

        if(cell_time.hour < filter_time.hour) {
            fits.push(data_rows[i]);
        }
        else if(cell_time.hour == filter_time.hour && cell_time.minute <= filter_time.minute) {
            fits.push(data_rows[i]);
        }
        else {
            data_rows[i].style.display = "none";
        }
    }
    return fits;
}


function time_filter(data_rows, input, cell_index) {
    var input_el = document.getElementById(input);
    if(input_el.className == "tlp-input-empty") return data_rows;

    var filter_value = input_el.value;
    if (!filter_value) return data_rows;


    /*
     var indexOf = filter_value.indexOf(":");
     if(indexOf != -1) { // valandos:minutes
     var filter_time = parseTime(filter_value.substr(0,indexOf) + filter_value.substr(indexOf+1));
     }
     else if(!isNaN(parseInt(filter_value, 10))) { // ivestos tik valandos be dvitaskio
     var filter_time = {hour:parseInt(filter_value, 10), minute:0};
     }*/
    var filter_time = parseTime2(filter_value);
    if(filter_time.hour==-1 || filter_time.minute==-1) {
        return data_rows;
    }

    var fits = [];
    for(var i=0; i<data_rows.length; i++) {
        var cell = data_rows[i].cells[cell_index];
        var cell_content = toascii(cell.textContent || cell.innerText);

        var indexOf = cell_content.indexOf(":");
        var cell_time = parseTime(cell_content.substr(0,indexOf) + cell_content.substr(indexOf+1));

        if(cell_time.hour > filter_time.hour) {
            fits.push(data_rows[i]);
        }
        else if(cell_time.hour == filter_time.hour && cell_time.minute >= filter_time.minute) {
            fits.push(data_rows[i]);
        }
        else {
            data_rows[i].style.display = "none";
        }
    }
    return fits;
}

function transport_filter(data_rows, input, cell_index) {
    var fits = [];

    if(typeof pc._temp[window.location.hash] == "undefined") {
        return data_rows;
    }
    else {
        var selected_routes = pc._temp[window.location.hash].selected_routes;
    }

    for(var i=0; i<data_rows.length; i++) {
        if(data_rows[i].style.display != "none") { // jeigu dabar yra rodomas reikia tikrinti, kitu atveju jis jau yra prafiltruotas ir neliechiam
            var title_split = data_rows[i].title.split(":");
            var tranport = title_split[0];
            var number = title_split[1];

            if(selected_routes[tranport][number] == true) {
                fits.push(data_rows[i]);
            }
            else {
                data_rows[i].style.display = "none";
                //console.log(title);
            }
        }
    }
    return fits;
}

function day_filter(data_rows, input, cell_index) {
    //var filter_value = document.getElementById(input).value;
    //if (filter_value == "1234567") return data_rows;

    var day_input = document.getElementById(pc._current_page_name + "_day_filter_input");
    var work_days = eval("(" + day_input.value + ")");

    var show_all = true;
    var day_join = "";
    for(var day in work_days) {
        if(work_days[day] == 0) {
            show_all = false;
            //break;
        }
        else {
            day_join += day;
        }
    }

    var day_header = document.getElementById(pc._current_page_name + "_days_header");

    if (show_all) {
        if(day_header) day_header.innerHTML = parseDaysAbb("1234567");
        return data_rows;
    }

    var fits = [];
    for(var i=0; i<data_rows.length; i++) {
        var cell = data_rows[i].cells[cell_index];
        var days = cell.className.substr(1);

        var hide = true;
        var days_split = days.split("");
        for(var j=0; j<days_split.length; j++) {
            if(work_days[days_split[j]]) { // radome bent viena diena, kuri yra uzhselectinta
                hide = false;
                fits.push(data_rows[i]);
                break;
            }
        }

        if(hide) {
            data_rows[i].style.display = "none";
        }

        /*
         if(days.indexOf(filter_value) != -1) {
         fits.push(data_rows[i]);
         }
         else {
         data_rows[i].style.display = "none";
         }
         */
    }

    if(day_header) {
        day_header.innerHTML = parseDaysAbb(day_join);
    }

    return fits;
}


function viastop_filter(data_rows, input, cell_index) {
//jeigu suvede visa "Tartu" tarkime pavadinima tai reiketu, kad parodzius autossuggesta iskarto butu irasutas hidden inputas
    var input_el = document.getElementById(input);
    if(input_el.className == "tlp-input-empty") return data_rows;

    if(input_el.value.length < 3) return data_rows;

    var filter_value = document.getElementById(pc._current_page_name + "_viastop_filter").value.split(",");

    //console.log(filter_value);
    //alert(data_rows[0].firstChild);

    var fits = [];
    for(var i=0; i<data_rows.length; i++) {
        var cell = data_rows[i].cells[cell_index];
        //var stops = data_rows[i].firstChild.value.split(",");
        var stops = cell.lastChild.value.split(",");

        var common_stops = stops.intersection(filter_value);
        if(common_stops.length) {
            fits.push(data_rows[i]);
        }
        else {
            data_rows[i].style.display = "none";
        }
    }
    return fits;

}


/* filtrai grazhina neprafiltruotas eilutes */   //"destination_filter_input", 2
function destination_filter(data_rows, input, cell_index) {
    var input_el = document.getElementById(input);
    if(input_el.className == "tlp-input-empty") return data_rows;

    var filter_value = input_el.value;
    if (!filter_value) return data_rows;

    var fits = [];
    for(var i=0; i<data_rows.length; i++) {
        var cell = data_rows[i].cells[cell_index];
        var cell_content = toascii(cell.textContent || cell.innerText);

        //console.log(cell, cell_content);

        var firstword_end = cell_content.indexOf(" "); // indexas arba 0
        if(firstword_end != -1) {
            cell_content = cell_content.substr(firstword_end);
        }

        var indexOf = cell_content.indexOf(toascii(filter_value)); // reiketu praleisti numeri...

        var separators = " -.\"()";
        if((indexOf == -1) || (separators.indexOf(cell_content.charAt(indexOf-1))==-1)) {
            data_rows[i].style.display = "none";
        }
        else {
            fits.push(data_rows[i]);
        }
    }
    return fits;
}


function title_filter(data_rows, input) {
    var input_el = document.getElementById(input);
    if(input_el.className == "tlp-input-empty") return data_rows;

    var filter_value = input_el.value;
    if (!filter_value) return data_rows;

    var intValue = parseInt(filter_value, 10);
    var isNumber = !(isNaN(intValue)) && (intValue.toString().length == filter_value.length);
    var separators = " -.\"()";

    var filter_ascii = toascii(filter_value);
    var filter_lower = filter_value.toLowerCase();


    var fits = [];
    var odd = 1;
    for(var i=0; i<data_rows.length; i++) {
        var title = toascii(data_rows[i].title);
        var number = title.split(":")[0];

        var indexOf = title.indexOf(filter_ascii); // reiketu praleisti numeri...
        var lastIndexOf = title.lastIndexOf(filter_ascii);
        var repeats = (indexOf != lastIndexOf);

        if((isNumber && (readNumber(number) == intValue)) ||
            (!isNumber && (indexOf != -1) && (separators.indexOf(title.charAt(indexOf-1))!=-1)
            && (filter_ascii == filter_lower || data_rows[i].title.toLowerCase().indexOf(filter_lower) != -1)
            ) ||
            (repeats && !isNumber && (lastIndexOf != -1) && (separators.indexOf(title.charAt(lastIndexOf-1))!=-1)
            && (filter_ascii == filter_lower || data_rows[i].title.toLowerCase().indexOf(filter_lower) != -1)
            )
        ) {
            data_rows[i].className = (odd % 2) ? "odd data_row" : "data_row";
            fits.push(data_rows[i]);
            odd += 1;
        }
        else {
            data_rows[i].style.display = "none";
        }

        /*
         if((indexOf == -1) || ((digits.indexOf(filter_value.charAt(0)) == -1) && (separators.indexOf(title.charAt(indexOf-1))==-1))) {
         data_rows[i].style.display = "none";
         }
         else {
         data_rows[i].className = (odd % 2) ? "odd data_row" : "data_row";
         fits.push(data_rows[i]);
         odd += 1;
         }
         */

    }
    return fits;
}

function filter_routes(filters) {
    var node = document.getElementById("schedule_result");
    var data_rows = getElementsByClassName("data_row", node);

    //console.log(data_rows);

    var odd = 1;
    for(var i=0; i<data_rows.length; i++) {
        data_rows[i].className = (odd % 2) ? "odd data_row" : "data_row";
        data_rows[i].style.display = ""; // visus padarome matomais priesh filtruojant;
        odd += 1;
    }

    for(var i=0; i<filters.length; i++) {
        var f = filters[i];
        data_rows = f.filter(data_rows, f.input);
        //console.log("filter: ", f.input, " returns: ", data_rows);
    }
}


// filters [{ filter: destination_filter, cell: 2, input: 'destination_filter_input'}]
function filter(filters, hide_transport) {
    //console.log("filter: ", filters);

    var node = document.getElementById(pc._current_page_name + "_rcontent");
    var data_rows = getElementsByClassName("data_row", node);

    for(var i=0; i<data_rows.length; i++) {
        data_rows[i].style.display = ""; // visus padarome matomais priesh filtruojant;
    }


    if(hide_transport) {
        data_rows = transport_filter(data_rows); // pirma paslepiam pagal transporta
    }

    //console.log(pc._current_page_name);
    //console.log(data_rows);


    for(var i=0; i<filters.length; i++) {
        var f = filters[i];
        data_rows = f.filter(data_rows, f.input, f.cell);
        //console.log("filter: ", f.input, " returns: ", data_rows);
    }

    function show_how_headers(table, drows) {
        if(!table) return;

        var header_rows = getElementsByClassName("header_row", table);
        for(var i=0; i<header_rows.length; i++) {
            header_rows[i].style.display = "none"; // paslepiame lyg joje nebutu nei vienos matomos eilutes
        }

        var show = false;
        for(var j=0; j<drows.length; j++) {
            if (drows[j].offsetParent == table) {
                show = true;
                break;
            }
        }
        if(show) {
            for(var i=0; i<header_rows.length; i++) {
                header_rows[i].style.display = ""; // paslepiame lyg joje nebutu nei vienos matomos eilutes
            }
        }
    }

    var table_67 = document.getElementById(pc._current_page_name + "_table_67");
    var table_12345 = document.getElementById(pc._current_page_name + "_table_12345");
    // paslepiame lenteles jeigu jos tuscios
    show_how_headers(table_67, data_rows);
    show_how_headers(table_12345, data_rows);
}

function showall(hide_transport) {
    filter([], hide_transport);

    var day_header = document.getElementById(pc._current_page_name + "_days_header");
    if(day_header) {
        day_header.innerHTML = parseDaysAbb("1234567");
    }

}

function pt_report_timetable(data) {
    //var weekdays = ["12345", "67"]; // default
    var weekdays = ["1234567"]; // default

    /*
     if(!("67" in data.stop_schedule_byhour_all)) { // visishkai skirtingi 6 ir 7 marshrutai
     weekdays = ["12345", "6", "7"];
     }
     else { // "67" yra
     var size67 = sizeOfArrObj(data.stop_schedule_byhour_all["67"]);
     // bet minuchiu, kai vazhiuoja tik sestadieniais arba tik sekmadieniais yra daugiau nei tokiu kai vaziuoja biem dienomis
     console.log("67:", size67, " 6:", sizeOfArrObj(data.stop_schedule_byhour_all["6"]), " 7:",sizeOfArrObj(data.stop_schedule_byhour_all["7"]));


     if((sizeOfArrObj(data.stop_schedule_byhour_all["6"]) > size67) ||
     (sizeOfArrObj(data.stop_schedule_byhour_all["7"]) > size67))
     {
     weekdays = ["12345", "6", "7"];
     }
     }
     */

    var hours = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30];

    /*
     var show_end_stop = false;
     if(pc._temp.expand_data) {
     var stops = pc.getSI().getRouteData(pc._temp.expand_data.direction_id).stops;
     if(stops[stops.length-1] == pc._temp.expand_data.stop_id) {
     show_end_stop = true;
     }
     }
     */

    var reports_html = [];
    for(var index=0; index<weekdays.length; index++) {
        var days = weekdays[index];

        var thead = ["<thead>"]
        if(index == 0) {
            var daybox = ["<input id=\"", pc._current_page_name, "_day_filter_input\" type=\"hidden\" value=\"{1:1,2:1,3:1,4:1,5:1,6:1,7:1}\"/><input id=\"i31052\" checked=\"true\" type=\"checkbox\" onclick=\"check_days_all(this,event)\" onmouseover=\"show_dayfilter(this, event);\" style=\"margin: 0px;\"/>"];

            /*
             var daybox = ["<select name=\"day_filter\" id=\"", pc._current_page_name, "_day_filter_input\" size=\"1\"><option value=\"1234567\">*</option>"]
             for(var i=1; i<8; i++) {
             daybox.push(["<option value=\"", i, "\">", pc.i18n("lng" + i + "DayAbb"), "</option>"].join(""));
             }
             daybox.push("</select>");
             */

            thead.push(["<tr class=\"filter_row\"><td style=\"width: 5px;\" onmouseover=\"show_dayfilter(this, event);\">", daybox.join(""), "</td><td style=\"width: 30px;\">",
                "<input id=\"", pc._current_page_name, "_departure_filter_input\" type=\"text\" name=\"departure filter\" onblur=\"inputtest(this);\" onfocus=\"firstinput(this);\" class=\"tlp-input-empty\" title=\"", pc.i18n("lngDepart"), "\" value=\"", pc.i18n("lngDepart"), "\" size=\"12\" title=\"\"/>",
                "</td><td style=\"width: 90%;\"><input id=\"", pc._current_page_name, "_viastop_filter_input\" type=\"text\" name=\"destination filter\" class=\"tlp-input-empty\" title=\"", pc.i18n("lngMiddleStop"), "\" value=\"", pc.i18n("lngMiddleStop"), "\" size=\"20\" title=\"\"/>",
                "<input id=\"", pc._current_page_name, "_viastop_filter\" type=\"hidden\" value=\"\"/>",

                //"<input onclick=\"filter([{filter:destination_filter, cell:2, input:'", pc._current_page_name, "_destination_filter_input'}, {filter:time_filter, cell:1, input:'", pc._current_page_name, "_departure_filter_input'}, {filter:day_filter, cell:0, input:'", pc._current_page_name, "_day_filter_input'}], ", ((pc._current_page_name != "schedule")?"true":"false"), ");\" type=\"submit\" value=\"", pc.i18n("lngFilter"), "\" name=\"filter\"/>",
                "<input onclick=\"filter([{filter:viastop_filter, cell:0, input:'", pc._current_page_name, "_viastop_filter_input'}, {filter:time_filter, cell:1, input:'", pc._current_page_name, "_departure_filter_input'}, {filter:day_filter, cell:0, input:'", pc._current_page_name, "_day_filter_input'}], ", ((pc._current_page_name != "schedule")?"true":"false"), ");\" type=\"submit\" value=\"", pc.i18n("lngFilter"), "\" name=\"filter\"/>",
                "<input onclick=\"showall(", ((pc._current_page_name != "schedule")?"true":"false"), ");\" type=\"submit\" value=\"", pc.i18n("lngShowAll"), "\" name=\"showall\"/>",
                "</td></tr>"].join(""));
        }

        thead.push(["<tr class=\"header_row\"><td colspan=\"4\" id=\"", pc._current_page_name, "_days_header\">", parseDaysAbb(days), "</td></tr>",
            "<tr class=\"header_row\"><th style=\"width:5px;\"></th><th style=\"width:20px;\">",
            pc.i18n("lngTime"), "</th><th style=\"width:90%;\">",
            pc.i18n("lngRoute"), "</th></tr></thead>"].join(""));


        var hours_html = [];

        var tbody = ["<tbody>"];
        for (var i=0; i<hours.length; i++) { // kiekviena valanda
            var hour = hours[i];

            var race_times = [];
            var unique_keys = {};

            var day_list = days.split("");
            for(var k=0; k<day_list.length;k++) {
                var day_schedule = data.stop_schedule_byhour[day_list[k]];
                if (hour in day_schedule) {
                    for(var m=0; m<day_schedule[hour].length;m++) {
                        if((hour==0) && (day_schedule[hour][m].minute==0)) continue;

                        var race_obj = day_schedule[hour][m];
                        var ukey = [race_obj.direction_id, race_obj.race_index, race_obj.minute].join(":");

                        if(!(ukey in unique_keys)) {
                            race_times.push(race_obj);
                            unique_keys[ukey] = true;
                        }
                    }
                }
            }

            //race_times = race_times.unique(function (a,b) {
            //   if((a.direction_id == b.direction_id) && (a.race_index == b.race_index) && (a.minute == b.minute)) return true;
            //	 return false;
            //}); // pashaliname identishkus duomenis...

            if(race_times.length) {
                race_times.sort(function(x,y) {return x.minute - y.minute});

                for(var j=0; j<race_times.length; j++) { // per kiekviena minute
                    var x = race_times[j];

                    if(x.end && pc._temp.expand_data) {
                        var expand_route = pc.getSI().getRouteData(pc._temp.expand_data.direction_id);
                        if(expand_route.stops[expand_route.stops.length-1] != x.stop_id) continue;
                    }
                    else if(x.end && (pc._current_page_name == "search")) { continue; }

                    var minute_html = ["<span title=\"", (x.end)?pc.i18n("lngTheLastStop"):"" ,"\"><a style=\"cursor:pointer;\" onclick=\"load_racetime(", x.direction_id, ",",
                        x.stop_id, ",", x.race_index, ");\">", (hour%24), ":", ((x.minute<10)?("0" + x.minute):x.minute),
                        "</a>", ((x.end)?("<span style=\"cursor:default;color:red\">*</span>"):""), "</span>"].join("");

                    var route_data = pc.getSI().getRouteData(x.direction_id);
                    var color = pc.getTransportData(route_data.transport).color;

                    var nmb_class = (route_data.route_nmb.length==1)?"number1":((route_data.route_nmb.length==2)?"number":"number3");
                    var hour_change = (j==0)?("border-top:4px solid #DDD;"):("");

                    /* isrenkam tik reikiamas dienu raides */
                    var my_days = [];
                    if(x.days.indexOf(days) != -1)  {
                        var x_days = "";
                    }
                    else {
                        for(var z=0; z<day_list.length; z++) {
                            var d = day_list[z];
                            if(x.days.indexOf(d) != -1) {
                                my_days.push(d);
                            }
                        }
                        var x_days = parseDaysAbb(my_days.join(""), true);
                        //console.log(route_data.route_nmb, "> ", x.days, " : ", parseDaysAbb(x.days, true), " : ", days, " my: ", my_days.join(""), " ", parseDaysAbb(my_days.join(""), true));
                    }
                    /* isrenkam tik reikiamas dienu raides */


                    tbody.push(["<tr title=\"", route_data.transport, ":", route_data.route_nmb, "\" class=\"data_row\" style=\"", hour_change, "\">",
                        //"<input type=\"hidden\" value=\"", route_data.stops.join(","), "\"\>",
                        "<td class=\"d", (x_days?my_days.join(""):days) ,"\" style=\"text-align:center;font-size:10px;\">",
                        x_days || parseDaysAbb("1234567", true), //("<span style=\"color:grey;\">" + pc.i18n("lngDaily") + "</span>"),
                        "<input type=\"hidden\" value=\"", route_data.stops.join(","), "\"\>",
                        "</td>",
                        //"<th style=\"text-align:right;\">", hour_html, "</th>",
                        "<td style=\"text-align:center;\">", minute_html, "</td><td style=\"white-space:nowrap;\" class=\"link\">",
                        "<img onclick=\"showRouteInfo('",x.direction_id, "','right',this,event)\" style=\"cursor:pointer; margin:2px 2px -3px 2px;\" class=\"img_info\" src=\"common/images/info.gif\" width=\"13\" height=\"15\"/>",

                        "<span><a href=\"", pc.url({page:"schedule", args:[route_data.transport, x.direction_id, x.stop_id]}), "\" class=\"number_link\" style=\"cursor:pointer;padding-left:3px;padding-right:0px;\">",
                        "<b class=\"", nmb_class, "\" style='line-height:1.5;background-color:", color, "'>", route_data.route_nmb, "</b>",
                        "</a></span>",

                        "<a href=\"", pc.url({page:"schedule", args:[route_data.transport, x.direction_id, x.stop_id]}), "\" style=\"padding:0 0px;cursor:pointer;color:", color, ";\">", route_data.direction_name,
                        "</a></td></tr>"].join(""));
                }
            }
        }


        tbody.push("</tbody>");

        if(tbody.length>2) {
            reports_html.push("<table id=\"" + pc._current_page_name + "_table_" + days + "\" style=\"width:100%;\" class=\"timetable\">" + thead.join("") + tbody.join("") + "</table>");
        }
    }

    reports_html.push(pc.i18n("lngInfo") + "<br/>" + pc.i18n("lngClickTime"));
    return reports_html.join("<br/>");
}


function pt_timetable(data, interval) {

    var si = pc.getSI();

    if (data.direction_id) {
        var main_directions = si.get_maindirections(data.direction_id);
    }

    var table = [];
    var show_ground_msg = false;
    var sn = "";

    for(var i=0; i<data.directions.length;i++) {
        var route_data = pc.getSI().getRouteData(data.directions[i]);

        var nmb_class = (route_data.route_nmb.length == 1) ? "number_small1" : ((route_data.route_nmb.length == 2) ? "number_small" : "number_small3");

        var color = pc.getTransportData(route_data.transport).color;

        var is_opposite = (data.opposite_directions.indexOf(data.directions[i]) != -1);
        table.push(["<div", ((i==data.directions.length-1)?" style='margin-bottom:4px;'":""), "><a class='number_link' onclick=\"pc.expectedHash = '';\" href=\"",
            pc.url({page:"schedule", args:[route_data.transport, data.directions[i], data.stop_id]}), "\">", "<span class='", nmb_class, "' style=\"background-color:", color, "\">",
            route_data.route_nmb, ((is_opposite)?"&nbsp;&laquo;":""), "</span></a>",
            "<a style='color:", color, ";' onclick=\"pc.expectedHash = '';\" href='",
            pc.url({page:"schedule", args:[route_data.transport, data.directions[i], data.stop_id]}), "'>",
            route_data.direction_name, "</a></div>"].join(""));


        /* ------------------------------------------------- */





    }




    table.push("<table class=\"timetable\"><tbody><tr>");
    for (var i=0; i<data.all_workdays.length; i++) { // kad islaikyti eiliskuma lenteleje...
        table.push("<th></th><th>" + parseDaysAbb(data.all_workdays[i]) + "</th>");
    }
    table.push("</tr>");

    var hours = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]; // galima bus perslinkti...
    var show_hours = {}; // for HTML formatting 
    var ashow_hours = {}; // for HTML formatting 

    var last_minute = {}; // minskui - fiksuojame paskutine ishvykimo minute , [diena]{hour:23, minute:12}
    // last_minute[days] = {hour:hour, minute: minute}

    var hours_html = [];
    for (var i=0; i<hours.length; i++) { // kiekviena valanda

        var days_html = [];


        for(var j=0; j<data.all_workdays.length; j++) {
            var days = data.all_workdays[j];

            if(days.length == 1) { // "1", "3", etc
                var schedule = data.stop_schedule_byhour[days];
                //arg.stop_schedule_byhour[wd][hour].sort(function(x,y) {return x.minute - y.minute});

            }
            else { // "123", "67", etc
                var schedule = data.stop_schedule_byhour_all[days];
            }

            // Chia dabar reikia sutinkti nauja valandos grafika  <- pagal tai kokios dienos gautos
            //pvz: all_days = [123, 456], tai reikia apjungti [1,2,3] ir [4,5,6]

            var hour = hours[i];

            var race_times = [];
            var unique_keys = {};

            var day_list = days.split("");
            for(var k=0; k<day_list.length;k++) {
                var day_schedule = data.stop_schedule_byhour[day_list[k]];
                if (hour in day_schedule) {
                    for(var m=0; m<day_schedule[hour].length;m++) {
                        var race_obj = day_schedule[hour][m];

                        if((hour==0) && (race_obj.minute==0)) continue;
                        var ukey = [race_obj.direction_id, race_obj.race_index, race_obj.minute].join(":");

                        if(!(ukey in unique_keys)) {
                            race_times.push(race_obj);
                            unique_keys[ukey] = true;
                        }

                        //console.log(interval, hour, race_obj.minute, race_obj)

                        // race_times.push(day_schedule[hour][m]);
                    }
                }
            }

            //race_times = race_times.unique(); // pashaliname identishkus duomenis...
            //race_times = race_times.unique(function (a,b) {
            //    if((a.direction_id == b.direction_id) && (a.race_index == b.race_index) && (a.minute == b.minute)) return true;
            //	return false;
            //}); // pashaliname identishkus duomenis...


            if(race_times.length) {
                race_times.sort(function(x,y) {return x.minute - y.minute});
                // schedules = {"1-5": { 23:[ {race_index:7, min: 15},  {race_index:5, min: 23}]}, }


                var time_html = [];

                //interval = false;
                if(interval) { // minsk metro
                    interval_html = [];

                    if(race_times.length == 1) {
                        var minute = race_times[0].minute;
                        if(!(days in last_minute)) {
                            time_html.push([pc.i18n("lngFromTime"), " ", printTime({hour:hour, minute:minute}), " "].join(""));
                        }
                        else if(!((hour+1) in data.stop_schedule_byhour_all[days])) {
                            time_html.push([" ", pc.i18n("lngUntilTime"), " ", printTime({hour:hour, minute:minute})].join(""));
                        }
                        else {
                            time_html.push(((minute<10)?("0" + minute):minute));
                        }
                        last_minute[days] = {hour:hour, minute: minute}

                        //time_html.push(((minute<10)?("0" + minute):minute));
                    }
                    else {
                        var min = Number.MAX_VALUE;
                        var max = Number.MIN_VALUE;
                        var previous_min = -1;

                        for(var r=0; r<race_times.length; r++) { // per kiekviena minute
                            var minute = race_times[r].minute;

                            if(!(days in last_minute)) {
                                interval_html.push([pc.i18n("lngFromTime"), " ", printTime({hour:hour, minute:minute}), " "].join(""));
                            }

                            last_minute[days] = {hour:hour, minute: minute}

                            if(previous_min != -1) {
                                var wait = minute - previous_min;
                                if(wait != 0) {
                                    if(wait < min) min = wait;
                                    if(wait > max) max = wait;
                                }
                            }
                            previous_min = minute;
                        }
                        if(interval_html.length) {
                            //interval_html.push([" ", pc.i18n("lngInterval"), " "].join(""));
                            interval_html.push(printInterval(min, max, true));
                        }
                        else {
                            interval_html.push(printInterval(min, max, true));
                        }

                        if(!((hour+1) in data.stop_schedule_byhour_all[days])) {
                            interval_html.push([" ", pc.i18n("lngUntilTime"), " ", printTime({hour:hour, minute:minute}), " "].join(""))
                        }
                        time_html.push(interval_html.join(""));
                    }
                }

                else {
                    for(var r=0; r<race_times.length; r++) { // per kiekviena minute
                        var x = race_times[r];

                        //if(x.end) continue; // vienodi visishkai linkai abiem atvejais... kai pirma ir paskutine stotele vienodos
                        if(x.end && pc._temp.expand_data) {
                            var expand_route = pc.getSI().getRouteData(pc._temp.expand_data.direction_id);
                            if(expand_route.stops[expand_route.stops.length-1] != data.stop_id) continue;
                        }

                        var xroute_data = pc.getSI().getRouteData(x.direction_id);
                        var route_type = xroute_data.route_type;
                        var route_nmb = xroute_data.route_nmb;
                        var color = pc.getTransportData(xroute_data.transport).color;

                        show_ground_msg = show_ground_msg || x.ground;

                        if (!data.direction_id) {
                            //Tokio varianto kai rodomi numeriai /12A jau nebeliko

                            time_html.push(["<span title=\"", (x.end) ? pc.i18n("lngTheLastStop"):"", "\"><a class=\"", x.ground?"ground":"" ,"\" style=\"cursor:pointer;\" onclick=\"load_racetime(", x.direction_id, ",", data.stop_id, ",", x.race_index, ");\">", ((x.minute<10)?("0" + x.minute):x.minute),
                                "</a>", ((x.end)?("<span style=\"cursor:default;color:red\">*</span>"):""),
                                "<span class='number_small_white' style=\"color:", color, "\">/", route_nmb, "</span>",
                                "</span>"].join(""));


                        }
                        else if(route_type in main_directions) {
                            time_html.push(["<span title=\"", (x.end) ? pc.i18n("lngTheLastStop"):"", "\"><a class=\"", x.ground?"ground":"" ,"\" style=\"cursor:pointer;\" onclick=\"load_racetime(", x.direction_id, ",", data.stop_id, ",", x.race_index, ");\">", ((x.minute<10)?("0" + x.minute):x.minute),
                                "</a>", ((x.end)?("<span style=\"cursor:default;color:red\">*</span>"):""), "</span>"].join(""));
                        }
                        else {
                            //console.log(x.direction_id);

                            var color = "blue";
                            var type_split = route_type.split(">");
                            var to = type_split[type_split.length-1];

                            if(to.charAt(0) == "D") {
                                color = "red";
                            }
                            else if(to.indexOf("2") != -1) {
                                color = "green";
                            }
                            time_html.push(["<span title=\"", (x.end) ? pc.i18n("lngTheLastStop"):"", "\"><<a class=\"", x.ground?"ground":"" ,"\" style=\"cursor:pointer;\" onclick=\"load_racetime(",  x.direction_id, ",",
                                data.stop_id, ",", x.race_index, ");\"", "\" title=\"",
                                xroute_data.direction_name, "\"><i style=\"color:", color, "; font-style: italic;\">",
                                ((x.minute<10)?("0" + x.minute):x.minute), "</i></a>",
                                ((x.end)?("<span style=\"cursor:default;color:red\">*</span>"):""), "</span>"].join(""));
                        }


                    }
                } // interval

                if(time_html.length) {
                    days_html.push("<th>" + (hour%24) + "</th><td>");
                    days_html.push(time_html.join("<span class=\"wbr\"> </span>"));
                    days_html.push("</td>");
                }
            }
            else {
                days_html.push("<th>&nbsp;</th><td></td>");
            }

        }

        hours_html.push(["<tr>", days_html.join(""), "</tr>"].join(""));
    }

    table.push(filter_empty_hours(hours_html, data.all_workdays.length).join(""));
    table.push("</tbody></table><br/>");


    table.push(pc.i18n("lngInfo") + "<br/>" + (show_ground_msg ? pc.i18n("lngDeparturesLowGround")+"<br/>":"") + pc.i18n("lngClickTime"));

    return table.join("");
}


// pashalina tas valandas kuriomis niekas nevazhiuoja - iki vtarkaraschio pradzhios ir po jo pabaigos
function filter_empty_hours(hours_html, cells) {
    var row = ["<tr>"];
    for(var i=0; i<cells; i++) {
        row.push("<th>&nbsp;</th><td></td>");
    }
    row.push("</tr>");
    var empty_row = row.join("");

    var start = 0;
    for(var i=0; i<hours_html.length; i++) {
        if (hours_html[i] != empty_row) {
            start = i;
            break;
        }
    }

    var finish = hours_html.length;
    for(var j=finish; j>=0; j--) {
        if (hours_html[j-1] != empty_row) {
            finish = j;
            break;
        }
    }
    return hours_html.slice(start, finish);
}


function pt_schedule_routeid_stopid(args) {
    var si = args.si;
    var direction_id = args.direction_id;
    var route_data = pc.getSI().getRouteData(direction_id);

    var report = pc.getTransportData(args.transport).report || false;

    var stop_id = args.stop_id;
    var stop_data = pc.getSI().getStopData(stop_id);

    var area_street = [];
    if (stop_data.area != "0") area_street.push(stop_data.area);
    if (stop_data.street != "0") area_street.push(stop_data.street);

    var transport_html = ["<table><tr><td style=\"vertical-align:top;white-space:nowrap;padding-right:20px;\"><b>", stop_data.name,
        "</b><br/>", (area_street.length?("<span style='font-size:10px;'>(" + area_street.join(", ") + ")</span><br/>"):""),
        "</td><td>"];


    if(!report) {
        var onmouseover_html = "report_choices(this, event, 'schedule_selected_routes', true);";
    }
    else {
        var onmouseover_html = "report_choices(this, event, 'schedule_selected_routes', false);";
    }

    //console.log(stop_data.temp);
    var html_value = {}; //
    for (var tt in stop_data.temp) {
        //console.log(tt);
        var routes_html = {};
        for(var route_nmb in stop_data.temp[tt]) { routes_html[route_nmb] = (route_data.route_nmb == route_nmb)?1:0; }
        html_value[tt] = ObjectToString(routes_html);

        var transport_data = pc.getTransportData(tt);

        transport_html.push(["<div class=\"transport_choice\" style=\"cursor:default;\" onclick=\"", onmouseover_html, "\" onmouseover=\"", onmouseover_html, "\"><input style='margin:0px;' onclick=\"showLoadingTimeTable(); checkReportTableAll(this,'", tt, "',", !report, ");\" name='cb' type='checkbox' value='", tt, "'/> <span style=\"color:", transport_data.color,
            ";font-weight: bold;\">", pc.i18n(transport_data.title), "</span></div>"].join(""));
    }

    //transport_html.push("</td></tr><tr><td></td><td><input type=\"checkbox\" id=\"showfilter\" name=\"showfilter\" onclick=\"filter_toggle(this);\" style=\"margin: 0px;\"/>"+
    //                                    "<span class=\"show_filter\" style=\"line-height: 14pt; margin-left: 4px; font-size: 8pt;\">" + pc.i18n("lngShowFilter") + "</span></td></tr></table>")
    //{1:1,2:1,3:1,4:1,5:1,6:1,7:1}
    transport_html.push(["<input id=\"", pc._current_page_name, "_transport_filter\" type=\"hidden\" value=\"",  ObjectToString(html_value), "\"/>"].join(""))
    transport_html.push("</td></tr></table>")

    pc.frame(0);
    return "<div id=\"schedule_list\" class=\"content\">" + transport_html.join("") + "</div>";
}


/*
 "<input id=\"search_transport_input\" type=\"hidden\" value=\"", ObjectToString(transport), "\"/>",
 "<input id=\"search_change_input\" type=\"hidden\" value=\"5\"/>",
 */

function check_transport(input) {
    var transport_input = document.getElementById("search_transport_input");
    var transport_obj = eval("(" + transport_input.value + ")");
    transport_obj[input.name] = input.checked;
    transport_input.value = ObjectToString(transport_obj);
}

function show_options(obj, event) {
    var transport_html = [pc.i18n("lngUseTransportation"), "<br/>"];

    var transport_types = pc.getAllTransportTypes();
    var transport_value = eval("(" + document.getElementById("search_transport_input").value + ")");

    for (var tt in transport_types) {
        var color = transport_types[tt].color;
        var title = pc.i18n(transport_types[tt].title);

        transport_html.push(["<input onclick=\"check_transport(this)\" type=\"checkbox\" ", (transport_value[tt])?"checked=\"checked\" ":"", "id=\"Opt", tt, "\" name=\"", tt, "\"/><span style=\"color:", color,"\">", title, "</span><br/>"].join(""));
    }

    var change_value = document.getElementById("search_change_input").value;
    var change_options = [1,3,5,10];
    change_options = change_options.map(function(x) { return ["<option ", (x==change_value)?"selected=\"selected\" ":"", "value=\"", x, "\">", x, "</option>"].join("")});

    transport_html.push(["<hr/>", pc.i18n("lngTimeToChange"), " <select onchange=\"document.getElementById('search_change_input').value=this.value;\">",
        change_options.join(""),
        "</select> min"].join(""));

    var maxwalk_value = document.getElementById("search_maxwalk_input").value;
    var maxwalk_options = [50, 200, 500, 1000, 2000, 5000];

    maxwalk_options = maxwalk_options.map(function(x) { return ["<option ", (x==maxwalk_value)?"selected=\"selected\" ":"", "value=\"", x, "\">", x, "</option>"].join("")});
    transport_html.push("<hr/>", pc.i18n("lngMaxWalk"), " <select onchange=\"document.getElementById('search_maxwalk_input').value=this.value;\">",
        maxwalk_options.join(""), "</select> m");


    var walkspeed_value = document.getElementById("search_walkspeed_input").value;
    var walkspeed_options = [2, 3, 4, 5, 6];
    walkspeed_options = walkspeed_options.map(function(x) { return ["<option ", (x==walkspeed_value)?"selected=\"selected\" ":"", "value=\"", x, "\">", x, "</option>"].join("")});

    transport_html.push("<br/>", pc.i18n("lngWalkingSpeed"), " <select onchange=\"document.getElementById('search_walkspeed_input').value=this.value;\">", walkspeed_options.join("") ,"</select> km/h");

    var html = transport_html.join("");

    fixedtooltip_static({width:330, html:html, scroll_div_id:pc._current_page_name + "_left", obj:obj, event:event, offset:[0,-1], cancel:true}, 250);
}

function report_choices(obj, event, selected_routes_temp, html_data) {
    var tt = obj.firstChild.value;
    var transport_types = pc.getAllTransportTypes();
    var color = transport_types[tt].color;

    if(html_data) {
        var routes_filter = document.getElementById(pc._current_page_name + "_transport_filter");
        //var routes_obj = eval("(" + routes_filter.value + ")");
        var selected_routes = eval("(" + routes_filter.value + ")");
    }
    else {
        if (selected_routes_temp == "schedule_selected_routes") {
            //var stop_id = pc._temp.expand_data.stop_id;
            //var tdata = pc.getSI()._stops_data[stop_id].temp[tt];
            var selected_routes = pc._temp[selected_routes_temp];
        }
        else {
            var selected_routes = pc._temp[window.location.hash].selected_routes;
        }
    }

    //console.log(selected_routes);

    var transport_html = [];
    for(var route_nmb in selected_routes[tt]) {
        var check = selected_routes[tt][route_nmb];
        if(selected_routes_temp) {
            var onclick_html = ["showLoadingTimeTable(); checkReportTableItem(this,'", tt, "','", route_nmb, "',", html_data, ")"].join("");
        }
        else {
            var onclick_html = ["checkReportTableItemFilter(this,'", tt, "','", route_nmb, "')"].join("");
        }
        transport_html.push(["<span style=\"white-space:nowrap;\"><input style='margin:0px;' onclick=\"", onclick_html, "\" name='cb' ", (check?"checked='checked'":""),
            " type='checkbox' value='", tt, "'/><span class='number_white' style='color:", color,
            "'>", route_nmb, "</span></span> "].join(""));
    }

    //console.log(tdata);
    var html = transport_html.join("");

    fixedtooltip({width:250, scroll_div_id:pc._current_page_name + "_right", html:html, obj:obj, event:event, offset:[0,-2]}, 0);
}

function showLoadingTimeTable() {
    var timetable = document.getElementById("timetable");
    timetable.innerHTML = pc.i18n("lngLoading");
}

function checkReportTableAll(obj, tt, html_input) {
    hidetip();

    //if(pc.getTransportData(tt).report) {
    if(!html_input) {
        var selected_routes = pc._temp.schedule_selected_routes[tt];
        for(var route_nmb in selected_routes) {
            selected_routes[route_nmb] = (obj.checked) ? true : false;
        }
        updateReportTable();
    }
    else { // miesto transportas
        var routes_filter = document.getElementById(pc._current_page_name + "_transport_filter");
        var routes_obj = eval("(" + routes_filter.value + ")");
        //console.log(routes_obj);

        html_value = {};
        for(var transport in routes_obj) {
            if(transport == tt) {
                for(var route_nmb in routes_obj[tt]) {
                    routes_obj[tt][route_nmb] = (obj.checked) ? 1 : 0;
                }
            }
            html_value[transport] = ObjectToString(routes_obj[transport]);
        }
        routes_filter.setAttribute("value", ObjectToString(html_value));

        var stop_id = pc._temp.expand_data.stop_id;
        updateTimeTable(stop_id);
    }

    //var showfilter_el = document.getElementById("showfilter"); // kadangi perkrauname lentele, tai reikia nuzymeti filtro checkboxa
    //if(showfilter_el) {
    //    showfilter_el.checked = false;
    //}

}


function checkReportTableAllFilter(obj, tt) {
    hidetip();
    var selected_routes = pc._temp[window.location.hash].selected_routes[tt];

    for(var route_nmb in selected_routes) {
        selected_routes[route_nmb] = (obj.checked) ? true : false;
    }
    filter([], true);
}


// naudojas search/routesearch puslapiuose vietoje checkReportTableItem
function checkReportTableItemFilter(obj, tt, route_nmb) {

    //console.log("FILTER!");
    //var hash = window.location.hash;
    var selected_routes = pc._temp[window.location.hash].selected_routes;
    selected_routes[tt][route_nmb] = (obj.checked) ? true : false;

    //filter();
    // filters [{ filter: destination_filter, cell: 2, input: 'destination_filter_input'}]
    filter([], true);
}

function checkReportTableItem(obj, tt, route_nmb, html_input) {
    //if(pc.getTransportData(tt).report) {
    if(!html_input) {
        pc._temp.schedule_selected_routes[tt][route_nmb] = (obj.checked) ? true : false;

        var timetable = document.getElementById("timetable");
        timetable.innerHTML = pc.i18n("lngLoading");

        updateReportTable();
    }
    else { // miesto transportas
        var routes_filter = document.getElementById(pc._current_page_name + "_transport_filter");
        var routes_obj = eval("(" + routes_filter.value + ")");

        html_value = {};
        routes_obj[tt][route_nmb] = (obj.checked) ? 1 : 0;

        for(var transport in routes_obj) {
            html_value[transport] = ObjectToString(routes_obj[transport]);
        }
        routes_filter.setAttribute("value", ObjectToString(html_value));

        var stop_id = pc._temp.expand_data.stop_id;
        //console.log("WTF!!");
        updateTimeTable(stop_id);
    }

    //var showfilter_el = document.getElementById("showfilter"); // kadangi perkrauname lentele, tai reikia nuzymeti filtro checkboxa
    //if(showfilter_el) {
    //    showfilter_el.checked = false;
    //}

}

function pt_schedule_time(args) {
    var si = args.si;
    var transport = args.transport; //args[0];
    var direction_id_arg = args.direction_id; //args[1];
    var stop_id_arg = args.stop_id; //args[2];

    var route_data = pc.getSI().getRouteData(direction_id_arg);
    var city = pc.chooseCity(route_data);

    var new_args = [transport,direction_id_arg];
    if(stop_id_arg) { new_args.push(stop_id_arg); }

    var schedule_html = ["<div id=\"schedule_list\" class=\"inner content routes\">"];
    schedule_html.push("<span class=\"print_hide\"><a id=\"show_time_a\" href=\"" + pc.url({city:city, page:"schedule", args:new_args}) + "\" class=\"command\" style=\"font-size:8pt;cursor:pointer;\">" + pc.i18n("lngClickOnStop") + "</a></span><br/>");
    schedule_html.push(["<input type=\"checkbox\" name=\"routenum\"", (pc.show_transport?" checked='true'":'') ," onclick=\"loadRouteNumbers('", transport, "','", direction_id_arg, "','", stop_id_arg, "', this, true);\" style=\"margin: 0px; margin-right:4px;\"/><span class=\"print_hide\" style=\"line-height: 14pt;font-size: 8pt;\">",
        pc.i18n("lngShowRouteNumbers"), "</span>"].join(""));

    schedule_html.push("<br/>");

    var directions = si.get_directions(direction_id_arg);
    var main_directions_html = [];

    if(directions.main) {
        //var direction_html = ["<div><b>Main direction: </b><br/>"];
        var direction_html = ["<div>"];
        direction_html.push(other_directions_html({directions:[directions.main], direction_id:direction_id_arg, main:true, stop_id:stop_id_arg, template:"time"}));
        direction_html.push("</div>");
        main_directions_html.push(direction_html.join(""));
    }
    if(directions.main_opposite) {
        //var direction_html = ["<div><b>Main direction (backward): </b><br/>"];
        var direction_html = ["<div>"];
        direction_html.push(other_directions_html({directions:[directions.main_opposite], direction_id:direction_id_arg, main:true, stop_id:stop_id_arg, template:"time"}));
        direction_html.push("<br/></div>");
        main_directions_html.push(direction_html.join(""));
    }

    schedule_html.push("<div>" + main_directions_html.join("") + "<br/></div>");

    var side_directions_html = [];

    if (directions.similar.length != 0) {
        side_directions_html.push("<div><b>" + pc.i18n("lngDirectionsForwards") + "</b><br/>");
        side_directions_html.push(other_directions_html({directions:directions.similar, direction_id:direction_id_arg, stop_id:stop_id_arg, template:"time"}));
        side_directions_html.push("<br/></div>");
    }

    if (directions.opposite.length != 0) {
        side_directions_html.push("<div><b>" + pc.i18n("lngOtherDirections") + "</b><br/>");
        side_directions_html.push(other_directions_html({directions:directions.opposite, direction_id:direction_id_arg, stop_id:stop_id_arg, template:"time"}));
        side_directions_html.push("<br/></div>");
    }
    if(directions.similar.length || directions.opposite.length) {
        schedule_html.push("<div>" + side_directions_html.join("") + "<br/></div>");
    }

    schedule_html.push("<br/><br/><br/><br/><br/></div>");

    return schedule_html.join("");
}



function pt_schedule_change(args) {
    var si = args.si;
    var transport = args.transport; //args[0];
    var direction_id_arg = args.direction_id; //args[1];
    var stop_id_arg = args.stop_id; //args[2];

    var show_main = args.collapse_main?false:true; // jeigu rodome laikus tai reikia sukolapsinti pagrindines kryptis

    var route_data = pc.getSI().getRouteData(direction_id_arg);
    var city = pc.chooseCity(route_data);

    var schedule_html = ["<div id=\"schedule_list\" class=\"inner content routes\">"];

    if(!stop_id_arg) { // dabar visad rodome pirma stotele (jei nenurodyta), todel nera reikalingas...
        var fstop = si._routes_data[direction_id_arg].stops[0];
        schedule_html.push("<div id='click_stop_tip'><a href=\"" + pc.url({city:city, page:"schedule", args:[transport, direction_id_arg, fstop]}) + "\"><span style=\"background-color:LightYellow; border:1px solid grey;\">" + pc.i18n("lngClickOnStop") + "</span></a></div>");
    }
    else if (transport != "metro") { // MINSK FIX
        schedule_html.push("<div class=\"print_hide\" style=\"line-height: 14pt;\"><a id=\"show_time_a\" href=\"" + pc.url({city:city, page:"schedule", args:[transport,direction_id_arg,stop_id_arg,"time"]}) + "\" class=\"command\" style=\"font-size:8pt;cursor:pointer;\">" + pc.i18n("lngShowDepartures") + "</a></div>");
    }
    schedule_html.push(["<input type=\"checkbox\" name=\"routenum\"", (pc.show_transport?" checked='true'":'') ," onclick=\"loadRouteNumbers('", transport, "','", direction_id_arg, "','", stop_id_arg, "', this);\" style=\"margin: 0px; margin-right:4px;\"/><span class=\"print_hide\" style=\"line-height: 14pt;font-size: 8pt;\">",
        pc.i18n("lngShowRouteNumbers"), "</span>"].join(""));

    schedule_html.push("<br/>");

    var directions = si.get_directions(direction_id_arg);
    var main_directions_html = [];

    if(directions.main) {
        //var direction_html = ["<div style=\"float:left;display:inline-block;width:465px;\"><b>Main direction: </b><br/>"];
        var direction_html = ["<div style=\"float:left;display:inline-block;width:465px;\">"];
        direction_html.push(other_directions_html({directions:[directions.main], direction_id:direction_id_arg, main:show_main, stop_id:stop_id_arg, template:"change"}));
        direction_html.push("</div>");
        main_directions_html.push(direction_html.join(""));
    }
    if(directions.main_opposite) {
        //var direction_html = ["<div style=\"float:left;display:inline-block;width:465px;\"><b>Main direction (backward): </b><br/>"];
        var direction_html = ["<div style=\"float:left;display:inline-block;width:465px;\">"];
        direction_html.push(other_directions_html({directions:[directions.main_opposite], direction_id:direction_id_arg, main:show_main, stop_id:stop_id_arg, template:"change"}));
        direction_html.push("<br/></div>");
        main_directions_html.push(direction_html.join(""));
    }

    schedule_html.push("<div style=\"display:float:left;width:90%;\">" + main_directions_html.join("") + "<br/></div>");

    var side_directions_html = [];

    if (directions.similar.length != 0) {
        side_directions_html.push("<div style=\"float:left;display:inline-block;width:465px;\"><b>" + pc.i18n("lngDirectionsForwards") + "</b><br/>");
        side_directions_html.push(other_directions_html({directions:directions.similar, direction_id:direction_id_arg, stop_id:stop_id_arg, template:"change"}));
        side_directions_html.push("<br/></div>");
    }

    if (directions.opposite.length != 0) {
        side_directions_html.push("<div style=\"float:left;display:inline-block;width:465px;\"><b>" + pc.i18n("lngOtherDirections") + "</b><br/>");
        side_directions_html.push(other_directions_html({directions:directions.opposite, direction_id:direction_id_arg, stop_id:stop_id_arg, template:"change"}));
        side_directions_html.push("<br/></div>");
    }
    if(directions.similar.length || directions.opposite.length) {
        schedule_html.push("<div style=\"float:left;display:block;width:90%;\">" + side_directions_html.join("") + "<br/></div>");
    }

    schedule_html.push("<br/><br/><br/><br/><br/></div>");

    return schedule_html.join("");
}


function other_directions_html(cfg) {
    var schedule_html = "";
    for(var i=0; i<cfg.directions.length; i++) {
        var direction_id = cfg.directions[i];

        //console.log(direction_id, cfg.expand);

        if ((direction_id == cfg.direction_id) || cfg.main || cfg.expand) {
            var cfg2 = {direction_id:direction_id, stop_id: cfg.stop_id, main:cfg.main};
            if(cfg.template) { cfg2.template = cfg.template; }
            var show_html = expand_html(cfg2);
        }
        else {
            var show_html = collapse_html({direction_id:direction_id, template:cfg.template});
        }
        schedule_html += "<div style='margin-top:4px;' id=\'dir_" + direction_id + "\'>" + show_html + "</div>";
    }
    return schedule_html;
}


function collapse(cfg) {
    document.getElementById("dir_" + cfg.direction_id).innerHTML = collapse_html(cfg);
}

function collapse_html(cfg) {
    var route_data = pc.getSI().getRouteData(cfg.direction_id);
    var color = pc.getTransportData(route_data.transport).color;

    var nmb_class = (route_data.route_nmb.length == 1) ? "number1" : ((route_data.route_nmb.length == 2) ? "number" : "number3");

    var schedule_html = ["<div style=\"width:450px;cursor:default\" onclick=\"expand({direction_id:", cfg.direction_id, (cfg.template?(",template:'"+cfg.template+"'"):""), "})\"><img src=\"", _sub_dir, "common/images/expand.gif\"/> <b class='", nmb_class, "' style=\"background-color:", color, "\">", route_data.route_nmb, "</b>",
        "<span style='color:" + color + ";'>", route_data.direction_name, "</span></div>"].join("");

    return schedule_html;
}

function expand(cfg) {
    document.getElementById("dir_" + cfg.direction_id).innerHTML = expand_html(cfg);
}


function expand_html(cfg) {

    var route_data = pc.getSI().getRouteData(cfg.direction_id);

    var color = pc.getTransportData(route_data.transport).color;

    var nmb_class = (route_data.route_nmb.length == 1) ? "number1" : ((route_data.route_nmb.length == 2) ? "number" : "number3");

    var direction_html = [];
    if(!cfg.main) {
        direction_html.push(["<div style=\"width:450px;cursor:default;margin-bottom:4px;\" onclick=\"collapse({direction_id:", cfg.direction_id, ((cfg.stop_id)?(",stop_id:"+cfg.stop_id):""), (cfg.template?(",template:'"+cfg.template+"'"):""), "})\"><img src=\"", _sub_dir, "common/images/collapse.gif\"/> "].join(""));
    }

    direction_html.push(["<b class='", nmb_class, "' style=\"background-color:", color, "\">", route_data.route_nmb, "</b>",
        "<span style='color:", color, ";'>", route_data.direction_name, "</span>"].join(""));

    if(!cfg.main) {
        direction_html.push("</div>");
    }

    if(cfg.template == "change") {
        direction_html.push(pt_left_shedule_change(cfg));
    }
    else if(cfg.template == "time") {
        direction_html.push(pt_left_shedule_time(cfg));
    }
    else {
    }
    return direction_html.join("");
}

function load_racetime(direction_id, stop_id, race_index, layer) {
    if(!pc._temp.expand_data) pc._temp.expand_data = {};
    pc._temp.expand_data["direction_id"] = direction_id;
    pc._temp.expand_data["stop_id"] = stop_id;
    pc._temp.expand_data["race_index"] = race_index;

    var transport = pc.getSI()._routes_data[direction_id].transport;

    //if(pc._current_page_name == "search") {
    //    pc.hash(pc.url({city:pc.city, page:"schedule", args:[transport, direction_id, stop_id, race_index, pc._current_page_name]}));
    //}

    if(pc._current_page_name == "route_search" || pc._current_page_name == "search") {
        var result_el = (pc._current_page_name == "route_search")?document.getElementById("search_start_result"):document.getElementById("search_result");

        var cfg = {
            direction_id:direction_id,
            stop_id:stop_id,
            template:"change",
            race_index:race_index,
            main:true,
            layer:(pc._current_page_name == "route_search")?"search_start_result":"search_result"
        }
        result_el.innerHTML = "<div style=\"padding:0px;\" class=\"content\">" + expand_html(cfg) + "</div>";
    }
    else {
        pc.show_content("schedule_result", pt_schedule_change({transport:transport, direction_id:direction_id, stop_id:stop_id, si:pc.getSI(), template:pt_left_shedule_change, collapse_main:true}), "transport_menu_tag");
        //pc.show_left_content({transport:transport, direction_id:direction_id, stop_id:stop_id, si:pc.getSI(), template:pt_left_shedule_change}, pt_schedule_change);
    }
}

function removeById(el_id) {
    var el = document.getElementById(el_id);
    if(el) {
        var parent = el.parentNode;
        parent.removeChild(el);
        //intro(parent);
    }
}
function changeStopTip(el_id, new_tip) {
    var el = document.getElementById(el_id);
    if(el) {
        el.innerHTML = new_tip;
    }
}

// nera prasmes daugiau?
function highlight_stopname(direction_id, stop_id, el) {
    if(pc._current_page_name != "schedule") return; // kad nespaudinetu ant laiku lenteles, kuria rodome search ir route_search puslapiuose

    var show_time_a = document.getElementById("show_time_a");
    if(show_time_a) {
        show_time_a.href = pc.url({page:"schedule", args:[transport, direction_id, stop_id, "time"]});
    }

    pc.suspend_left(true); // kad neperpieshinetu

    var results_html = document.getElementById("schedule_result");
    var trs = getElementsByClassName("gold", results_html);
    for(var i=0; i<trs.length; i++) {
        var class_name = trs[i].className;
        var split = class_name.split(" ");
        var index = split.indexOf("gold");
        split.splice(index, 1);
        trs[i].className = split.join(" ");
    }

    el.parentNode.className += " gold";
}

function remove_other_stops_tr(el) {
    //console.log(el.parentNode.parentNode);

    var tbody = el.parentNode.parentNode;
    tbody.innerHTML = "";

    // el.parentNode.style.backgroundColor = 'gold';
    // el.parentNode.style.fontWeight = 'bold';
    el.parentNode.className = "even";
    tbody.appendChild(el.parentNode);

    //for(var i=0; i<tbody.childNodes.length; i++) {
    //if(tbody.childNodes[i] != el.parentNode) {
    //        tbody.removeChild(tbody.childNodes[i]);
    //delete tbody.childNodes[i];
    //}
//	}
}

function highlight_stopname_2(el) {

    /*
     pc.show_right_content(pt_loading(), function(x) {return x;});
     var transport = pc.getSI()._routes_data[direction_id].transport;

     var show_time_a = document.getElementById("show_time_a");
     if(show_time_a) {
     show_time_a.href = pc.url({page:"schedule", args:[transport, direction_id, stop_id, "time"]});
     }
     else {
     changeStopTip("click_stop_tip", "<a id=\"show_time_a\" href=\"" + pc.url({page:"schedule", args:[transport,direction_id,stop_id,"time"]}) + "\" style=\"font-size:8pt;cursor:pointer;\">" + pc.i18n("lngShowDepartures") + "</a>");
     }

     pc.suspend_left(true); // kad neperpieshinetu
     */

    // TODO: pakeisti i class
    //console.log(el.parentNode.offsetParent.getElementsByTagName("tr"));
    //var trs = el.parentNode.offsetParent.getElementsByTagName("tr");
    //console.log(trs);
    pc.as_stopsearch.suspend_focus = true; // kad nepanakintu pageltoninimo sarashe, kai atvertus nauja hash'a ishkvieciamas focus().

    var trs = document.getElementsByTagName("tr");
    for(var i=0; i<trs.length; i++) {
        var color = trs[i].style.backgroundColor;

        if(color && (color=="rgb(255, 215, 0)" || color=="gold")) {
            trs[i].setAttribute("style", "");
            trs[i].style.backgroundColor = '';
            trs[i].style.fontWeight = '';
        }
    }

    // gal shita reikia padaryti tik ish pirmojo psulapio su dviem lygiagrechiais lapais! - negerai ,es (tvarkarashtyje tada bedos)
    // TODO: pakeisti i class

    /*
     var divs = document.getElementsByTagName("div");
     for(var i=0; i<divs.length; i++) {
     var div_id = divs[i].id;
     if(div_id && (div_id.substr(0,4) == 'dir_')) {
     var direction_html_id = div_id.substr(4);
     if (direction_html_id != direction_id) {
     var template = "change";

     divs[i].innerHTML = collapse_html({direction_id:direction_html_id, template:template});
     }
     }
     }
     */

    //intro(el);
    el.parentNode.style.backgroundColor = 'gold';
    el.parentNode.style.fontWeight = 'bold';

}

// TODO: ishkelti i gmaps sriti:
function gmap_loadstops(stops_ids) {
    pc.map_show(function(){
        map = pc.getGMap();
        map.checkResize();

        if (!pc.get_maptoggle()) { // ishjungiame jeigu ijungtas
            toggleStops(); // OldStopsInfo.init iskviecia kuris butinai reikalingas .show.
        }
        //map.clearOverlays(); // pashaliname kitas stoteles, trasas

        var stop_data = pc.getSI().getStopData(stops_ids[0]);
        map.setCenter(new GLatLng(stop_data.lat, stop_data.lng), 14);
        //map.addOverlay(new GMarker(map.getCenter()));

        OldStopsInfo.init2(map);
        OldStopsInfo.show(new GLatLng(stop_data.lat, stop_data.lng), stops_ids);
    });

}


//function gmap_loadoptimal(stops, routes, time) {
//	show_route_parts(stops, routes, time);
//}

function gmap_loadroute(direction_id, single) {

    pc.map_show(function(){
        map = pc.getGMap();
        map.checkResize();

        if (pc.get_maptoggle()) { // ishjungiame jeigu ijungtas
            toggleStops();
        }

        // pashaliname sena trasa
        while (arrayStopsAndRoute.length > 0) {
            map.removeOverlay(arrayStopsAndRoute.pop());
        }
        //map.clearOverlays(); // pashaliname kitas trasas

        if(single) {
            var stop_list = pc.getSI().getRouteData(direction_id).stops;
            var stops = [];

            for(var i=0; i<stop_list.length; i++) {
                stops.push({data:pc.getSI().getStopData(stop_list[i]), direction: "main", direction_id: direction_id});
            }
            var data = {stops:stops, color:"#ff0000", opacity:{main:0.7}, directions:{main:direction_id}};
        }
        else {
            var directions = pc.getSI().get_directions(direction_id);

            var stop_list = pc.getSI().getRouteData(directions.main).stops;
            var stops = [];

            for(var i=0; i<stop_list.length; i++) {
                stops.push({data:pc.getSI().getStopData(stop_list[i]), direction: "main", direction_id: directions.main});
            }

            if(directions.main_opposite) {
                var stop_list = pc.getSI().getRouteData(directions.main_opposite).stops;
                for(var i=0; i<stop_list.length; i++) {
                    stops.push({data:pc.getSI().getStopData(stop_list[i]), direction: "opposite", direction_id:directions.main_opposite});
                }

            }
            var data = {stops:stops, color:"#ff0000", opacity:{main:0.7, opposite:0.4}, directions:{main:directions.main, opposite:directions.main_opposite}};
        }

        // Pagrindineje trasoje reikia nupieshti visas stoteles ishskyrus starto!
        // Shalutineje trasoje visos stotelss pilkos, bet reikia dar nupieshti ir pagrindines trasos starta (o finisho nereikia)

        NewDrawMap(data);
    });
}

function loadIcons() {
    if (!iconYellow) // create GIcons
    {
        iconYellow = new GIcon(); iconYellow.image = _sub_dir + 'common/images/MarkerYellow.gif';
        iconYellow.shadow=''; iconYellow.iconSize = new GSize(12,20); iconYellow.shadowSize = new GSize(0,0);
        iconYellow.iconAnchor = new GPoint(6,20); iconYellow.infoWindowAnchor = new GPoint(5,1);

        iconGray=new GIcon(); iconGray.image = _sub_dir + 'common/images/MarkerGray.gif';
        iconGray.shadow=''; iconGray.iconSize = new GSize(12,20); iconGray.shadowSize = new GSize(0,0);
        iconGray.iconAnchor = new GPoint(6,20); iconGray.infoWindowAnchor = new GPoint(5,1);

        iconRed=new GIcon(); iconRed.image = _sub_dir + 'common/images/MarkerRed.gif';
        iconRed.shadow=''; iconRed.iconSize = new GSize(12,20); iconRed.shadowSize = new GSize(0,0);
        iconRed.iconAnchor = new GPoint(6,20); iconRed.infoWindowAnchor = new GPoint(5,1);

        iconStart=new GIcon(); iconStart.image = _sub_dir + 'common/images/MarkerStart.gif';
        iconStart.shadow=''; iconStart.iconSize = new GSize(20,34); iconStart.shadowSize = new GSize(0,0);
        iconStart.iconAnchor = new GPoint(9,34); iconStart.infoWindowAnchor = new GPoint(9,2);
        iconStart.dragCrossImage = _sub_dir + 'common/images/empty.gif'; // norime nieko nerodyti kaip dragCross //undocumented String: indicates an image to be used as the drag cross. If you set it to the null string, you get the default drag_cross_67_16.png image.
        iconStart.dragCrossSize = GSize(1, 1); //undocumented GSize(): indicates the size of the drag cross.
        iconStart.maxHeight = 1; //undocumented integer: The maximum difference in height between the marker anchor and the drag cross anchor during dragging. Setting the maxHeight to zero causes it to use the default 13.

        iconEnd=new GIcon(); iconEnd.image = _sub_dir + 'common/images/MarkerEnd.gif';
        iconEnd.shadow=''; iconEnd.iconSize = new GSize(20,34); iconEnd.shadowSize=new GSize(0,0);
        iconEnd.iconAnchor = new GPoint(9,34); iconEnd.infoWindowAnchor=new GPoint(9,2);

        iconStop = new GIcon();	iconStop.image = _sub_dir +'common/images/Stop.gif';
        iconStop.shadow = ''; iconStop.iconSize = new GSize(8,8); iconStop.shadowSize = new GSize(0,0);
        iconStop.iconAnchor = new GPoint(4,4); iconStop.infoWindowAnchor = new GPoint(4,4);

        iconStopHover = new GIcon(); iconStopHover.image = _sub_dir +'common/images/StopActive.gif';
        iconStopHover.shadow = ''; iconStopHover.iconSize = new GSize(8,8);	iconStopHover.shadowSize = new GSize(0,0);
        iconStopHover.iconAnchor = new GPoint(4,4);	iconStopHover.infoWindowAnchor = new GPoint(4,4);

        iconNode = new GIcon();	iconNode.image = _sub_dir +'common/images/node.gif';
        iconNode.shadow = ''; iconNode.iconSize = new GSize(8,8); iconNode.shadowSize = new GSize(0,0);
        iconNode.iconAnchor = new GPoint(4,4); iconNode.infoWindowAnchor = new GPoint(4,4);
    }
}

function NewDrawMap(data) { // žemėlapyje parodoma maršruto idTv trasa
    loadIcons();
    arrayStopsAndRoute = [];

    var center, bounds;

    // Parse the JSON document
    bounds = new GLatLngBounds();

    var tag = 0;

    var finish = false;
    var finish_stop = null;

    for (var i=data.stops.length-1; i>=0; i--) { //vėliausiai įdėtos stotelės bus viršuje
        var icon;
        var iconover = iconRed;

        if (i == 0) {
            icon = iconStart;
            iconover = iconStart;
        }
        else if (tag == 0 && i>1) {
            if(data.stops[i].direction == "main") {
                tag = 1;

                //del ZIndexo pirmiau parodysim pagrindines krypties pabaigos dideli markeri, kad jis butu po apacia pirmo nepagrindines krypties mazo pilko markerio
                //var temp = jsonData.stops[i];
                //jsonData.stops[i] = jsonData.stops[i-1];
                //jsonData.stops[i-1] = temp;
                finish = true;

                icon = iconEnd;
                iconover = iconEnd;
            }
            else
                icon = iconGray;
        }
        else if (tag == 1) {
            icon = iconGray;
            tag = 2;
        }
        else
            icon = iconYellow;

        //console.log(data.stops);
        var point = new GLatLng(data.stops[i].data.lat, data.stops[i].data.lng)
        bounds.extend(point);

        var stop;

        if (true) {
            stop = new GMarker(point, {icon:icon, title:data.stops[i].data.name, zIndexProcess:function() {return 1;}});
            GEvent.addListener(stop, "mouseover",
                function(x) {
                    return function() {x.setImage(iconRed.image); }
                }(stop)
            );
            GEvent.addListener(stop, "mouseout",
                function(x) {
                    return function() {x.setImage(x.getIcon().image); }
                }(stop)
            );
            GEvent.addListener(stop, "click",
                function(direction_id, stop_id) {
                    return function() {
                        pc.open_schedule({direction_id:direction_id, stop_id:stop_id});
                    }
                }(data.stops[i].direction_id, data.stops[i].data.id)
            );
        }
        else {
            stop = new IconOverlay(point, icon, {title:data.stops[i].data.name, mouseoverIcon:iconover});
        }
        stop.id = data.stops[i].data.id;
        //stop.idDir = jsonData.stops[i].idDir;

        //stoteles tik įsimename, piešime vėliau, nes pirmąkart žemėlapis bus nesukonstruotas, kad veltui nedarytų setCenter (privaloma prieš naudojant žemėlapį!) su kažkokia pradine sritimi

        if (finish) {
            finish_stop = stop;
            finish = false;
        }
        else {
            arrayStopsAndRoute.push(stop);
        }
    }
    arrayStopsAndRoute.splice(0, 0, finish_stop); // kad finisha uzhdengtu shalutinio kelio ikonkes


    if (bounds && map) {
        map.setCenter(bounds.getCenter(), map.getBoundsZoomLevel(bounds));
    }
    else if (map) {
        //map.clearOverlays(); // nerodyti senų paišymų perpiešiant žemėlapį
    }

    for (i=0; i<arrayStopsAndRoute.length; i++) {
        map.addOverlay(arrayStopsAndRoute[i]);
    }


    var main_points = [];
    var opposite_points = [];
    for (var i=0; i<data.stops.length; i++) {
        if (data.stops[i].direction == "main") {
            main_points.push(new GLatLng(data.stops[i].data.lat, data.stops[i].data.lng));
        }
        else {
            opposite_points.push(new GLatLng(data.stops[i].data.lat, data.stops[i].data.lng));
        }
    }

    /*
     var main_line = new GPolyline(main_points, data.color, 5, data.opacity.main);
     arrayStopsAndRoute.push(main_line);
     map.addOverlay(main_line);

     var opposite_line = new GPolyline(opposite_points, data.color, 5, data.opacity.opposite);
     arrayStopsAndRoute.push(opposite_line);
     map.addOverlay(opposite_line);
     */

    data.map = map;
    pc.getSI().executeOnPolylines(data, function(data) {
        var main_line2 = GPolyline.fromEncoded({
            color: data.color,
            weight: 5,
            opacity: data.opacity.main,
            points: data.polylines.main.line,
            levels: data.polylines.main.levels,
            zoomFactor: 32,
            numLevels: 4
        });
        arrayStopsAndRoute.push(main_line2);
        data.map.addOverlay(main_line2);

        if(data.polylines.opposite) {
            var opposite_line2 = GPolyline.fromEncoded({
                color: data.color,
                weight: 5,
                opacity: data.opacity.opposite,
                points: data.polylines.opposite.line,
                levels: data.polylines.opposite.levels,
                zoomFactor: 32,
                numLevels: 4
            });
            arrayStopsAndRoute.push(opposite_line2);
            data.map.addOverlay(opposite_line2);
        }
    });

    /*
     var main_line2 = GPolyline.fromEncoded({
     color: data.color,
     weight: 5,
     opacity: data.opacity.main,
     points: pointString,
     levels: levelString,
     zoomFactor: 4,
     numLevels: 9
     });
     */


}
