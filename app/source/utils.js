var national_chars={
    "\u0105":"a",
    "\u00E4":"a",
    "\u0101":"a",
    "\u010D":"c",
    "\u0119":"e",
    "\u0117":"e",
    "\u012F":"i",
    "\u0173":"u",
    "\u016B":"u",
    "\u00FC":"u",
    "\u017E":"z",
    "\u0113":"e",
    "\u0123":"g",
    "\u012B":"i",
    "\u0137":"k",
    "\u013C":"l",
    "\u0146":"n",
    "\u00F6":"o",
    "\u00F5":"o",
    "\u0161":"s"
};

function toascii(str) {
    var s = str.toLowerCase().split("");
    var s2;

    for (var i=s.length; --i >=0;) {
        if (s2 = national_chars[s[i]]) {
            s[i] = s2;
        }
    }

    return s.join("");
}

function create_routeclass_single(route_data) {
    var classes = [toascii(route_data.authority.replace(/ /g, "_")), route_data.transport, toascii(route_data.route_nmb.replace(/ /g, "_"))];
    return classes.join("_");
}


function create_routeclass(route_data) {
    var route_class = create_routeclass_single(route_data);
    var all = [route_class, "subscription"];
    if(pc.login_data && pc.login_data.subscriptions_hash) {
        if(route_class in pc.login_data.subscriptions_hash) all.push("subscribed");
    }
    return  all.join(" ");
}

/*
 a. B>B1

 b. A>B
 B>A
 B1>A
 */

function str_int_sort(a, b) {
    a_int = parseInt(a, 10);
    b_int = parseInt(b, 10);

    //alert(a_int + ":" + b_int);
    if(!isNaN(a_int) && !isNaN(b_int)) {
        if(a_int == b_int) {
            if(a == b) return 0;
            else if(a < b) return -1;
            return 1;
        }
        else return a_int - b_int;
    }
    else if(isNaN(a_int) && !isNaN(b_int)) {
        return 1;
    }
    else if(isNaN(b_int) && !isNaN(a_int)) {
        return -1;
    }
    else if (a.length == b.length) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    }
    else {
        return a.length - b.length;
    }
}

function parseDaysAbb(str, abb) {
    //console.log("parseDaysAbb: ", str, " abb: ", abb);
    if(!str) return "";

    var day_list = str.split("");

    if(day_list.length > 4) {
        var days_int_list = day_list.map(function(day) { return parseInt(day, 10);});
        var is_sequence = true;
        for(i=days_int_list.length-1; i>0; i--) {
            if(days_int_list[i] != days_int_list[i-1]+1) {
                is_sequence = false;
                break;
            }
        }
        if(is_sequence) {
            return pc.i18n("lng" + day_list[0] + "Day" + (abb?"Abb":"")) + "-" + pc.i18n("lng" + day_list[day_list.length-1] + "Day" + (abb?"Abb":""));
        }
        else {
            var day_names = day_list.map(function(day) { return pc.i18n("lng" + day + "Day" + (abb?"Abb":""));});
            return abb ? day_names.join("&nbsp;") : day_names.join(",");
        }
    }
    else {
        var day_names = day_list.map(function(day) { return pc.i18n("lng" + day + "Day" + (abb?"Abb":""));});
        return abb ? day_names.join("&nbsp;") : day_names.join(",");
    }
    //console.log("results", );
}

function isForward(type) {
    var split = type.split(">");

    var a_from = split[0];
    var a_to = split[split.length-1];

    if(a_from.charCodeAt(0)>a_to.charCodeAt(0)) return false;
    return true;
}


function isSimilar(a, b) {
    //console.log("2", a, b);

    var a_from = a[0];
    var a_to = a[a.length-1];

    var b_from = b[0];
    var b_to = b[b.length-1];

    //console.log("1");
    if(a_from == b_from) return true;

    //console.log("2", a_to, b_to);
    if(a_to == b_to) return true;

    // A1>B2, A>B1
    //console.log("3");
    if((a_from.charAt(0) == b_from.charAt(0)) && (a_to.charAt(0) == b_to.charAt(0))) return true;
    // B1>A, B>B1 - nepraeis
    //console.log("4");
    return false;

    /*
     // A1>B1 ir A>C o nepraeitu A1>B1 ir A1>A
     // A1>B ; A2>C	- toks praeitu:)  o toks jau ne:     // A1>B ; A2>A
     if ((a_from.charAt(0) == b_from.charAt(0))
     && ((a_to.charAt(0) != b_to.charAt(0))
     && (b_from.charAt(0) != b_to.charAt(0)))
     && (b_to.charAt(0) != a_from.charAt(0))
     ) return true;

     // A1>B1; C1>B2
     if((a_to.charAt(0) == b_to.charAt(0)) && ((a_from.charAt(0) != b_from.charAt(0))&&(b_to.charAt(0) != b_from.charAt(0)))) return true;

     // A2>B1 ; A1>A2
     if((b_from.charAt(0) == a_from.charAt(0)) && ((b_to.charAt(0) != a_to.charAt(0))&&(a_from.charAt(0) != a_to.charAt(0)))) return true;

     //
     if((b_to.charAt(0) == a_to.charAt(0)) && ((b_from.charAt(0) != a_from.charAt(0))&&(a_to.charAt(0) != a_from.charAt(0)))) return true;
     */

}

function parse_filedata(str, key, changemap) {
    return {key:data};
}

function create_div(id, className, style) {
    var div = document.createElement("div");
    div.className = className;
    div.id = id;

    if (style) {
        for (var key in style) {
            div.style[key] = style[key];
        }
    }
    return div;
}

function format_dirname(str) {
    return str; // estijos " - " bruksniai tvarkyngi

    //console.log(str, str.indexOf(" "), str.length);
    str_split = str.split(" вЂ” ");
    if (str_split.length == 1) { // visokiu zhiuriu brksniu buna:)
        return str;
    }
    else {
        return str_split.join(" вЂ“ ");
    }

//  if ((str.length > 25) && ((str.indexOf(" ") == -1) || (str.indexOf(" ") > 25))) {
//	  console.log(str_split);
//	  if (str_split.length > 2) {
//	       str_split[2] = " " + str_split[2];
//	  }
//  }

}

/*
 method : POST/GET
 url    : Call url
 func   : custom function which is used to process returned data,
 take only one parameter
 */



function CDownloadUrl(method, url, func) {
    var httpObj;

    try {
        if (window["XMLHttpRequest"]) {
            httpObj = new XMLHttpRequest();
        }
        else if (typeof ActiveXObject != 'undefined') {
            httpObj = new ActiveXObject('Microsoft.XMLHTTP');
        }
        else {
            return;
        }
    }
    catch (e) {
        changeStatus(e);
    }

    httpObj.open(method, url, true);
    httpObj.onreadystatechange = function() {
        if(httpObj.readyState == 4) {
            if (httpObj.status == 200 || httpObj.status == 0) {
                var contenttype = httpObj.getResponseHeader('Content-Type');
                if (contenttype.indexOf('xml') > -1) {
                    func(httpObj.responseXML);
                }
                else {
                    func(httpObj.responseText);
                }
            } else {
                func('Error: '+httpObj.status);
            }
        }
    };
    httpObj.send(null);
}


// IE has no Array.map
if (typeof Array.prototype.map == "undefined") {
    Array.prototype.map = function(fn, thisObj) {
        var scope = thisObj || window;
        var a = [];
        for ( var i=0, j=this.length; i < j; ++i ) {
            a.push(fn.call(scope, this[i], i, this));
        }
        return a;
    };
}

if (typeof Array.prototype.filter == "undefined") {
    Array.prototype.filter = function(fn, thisObj) {
        var scope = thisObj || window;
        var a = [];
        for ( var i=0, j=this.length; i < j; ++i ) {
            if ( !fn.call(scope, this[i], i, this) ) {
                continue;
            }
            a.push(this[i]);
        }
        return a;
    };
}


if (typeof Array.prototype.indexOf == "undefined") {
    Array.prototype.indexOf = function(el, start) {
        var start = start || 0;

        for ( var i = start - 1, j = this.length; ++i < j;) {
            if (this[i] === el) {
                return i;
            }
        }
        return -1;
    };
}

if (typeof Array.prototype.lastIndexOf == "undefined") {
    Array.prototype.lastIndexOf = function(el, start) {
        var start = start || this.length;
        if ( start >= this.length ) {
            start = this.length;
        }
        if ( start < 0 ) {
            start = this.length + start;
        }
        for ( var i=start; i >= 0; --i ) {
            if ( this[i] === el ) {
                return i;
            }
        }
        return -1;
    };
}


function fireEvent(element,event){
    if(document.createEvent){
        // dispatch for firefox + others
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, true, true ); // event type,bubbling,cancelable
        return !element.dispatchEvent(evt);
    }
    else{
        // dispatch for IE
        var evt = document.createEventObject();
        return element.fireEvent('on'+event,evt)
    }
}


function inputtest(input) {
    //console.log(">", input.value, "<:",input.initial_value);
    if(input.value == "") {
        input.className = "tlp-input-empty";
        input.clean = false;
        input.value = input.initial_value; //pc.i18n("lngTypeNumberCity");
    }
}

function inputfocus(input, init_value) {
    //console.log(pc.i18n(init_value), input.value, pc.i18n(init_value) == input.value);

    if(input.value == pc.i18n(init_value)) {
        input.value = "";
    }
}
function inputblur(input, init_value) {
    if(input.value == "") {
        input.value = pc.i18n(init_value);
    }
}


function firstinput(input, init_value) {
    //console.log("1:", input.value, " 2:", input.initial_value);
    if(!(input.clean)) {
        input.initial_value = init_value ? pc.i18n(init_value) : input.value;

        if(!init_value) {
            input.value = "";
        }

        input.className = "";
        input.clean = true;
    }
    if(input.value == input.initial_value) {
        input.value = "";
    }
}

function addZero(number) {
    return (number < 10) ? ("0" + number) : number;
}

function getElementsByClassName(searchClass,node) {
    if(!node) return [];
    if(typeof node.getElementsByClassName != "undefined") {
        return node.getElementsByClassName(searchClass);
    }
    else {
        return document.getElementsByClassName(searchClass, node);
    }
}

if (typeof document.getElementsByClassName == "undefined") {
    document.getElementsByClassName = function(searchClass,node,tag) {
        var classElements = new Array();
        if ( node == null )
            node = document;
        if ( tag == null )
            tag = '*';
        var els = node.getElementsByTagName(tag);
        var elsLen = els.length;
        var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
        for (i = 0, j = 0; i < elsLen; i++) {
            if ( pattern.test(els[i].className) ) {
                classElements[j] = els[i];
                j++;
            }
        }
        return classElements;
    }
}



/*
 Array.prototype.rstrip = function() {
 for (var i=this.length-1; i>=0; --i) {
 if(this[i] != "") {
 break;
 }
 else {
 this.splice(i,1);
 }
 }
 }
 */

//DEBUG introspection
function intro(obj) {
    property_list = [];
    for (prop in obj) {
        property_list.push(prop);
    }
    property_list.sort();

    alert(property_list.join(" - "));
}

function checkTime(value) {
    var errorMsg = ""; // regular expression to match required time format 
    re = /^(\d{1,2})(\d{2})$/;

    if(value != '') {
        if(regs = value.match(re)) {
            // 24-hour time format
            if(regs[1] > 23) {
                errorMsg = "Invalid value for hours: " + regs[1];
            }

            if(!errorMsg && regs[2] > 59) {
                errorMsg = "Invalid value for minutes: " + regs[2];
            }
        } else {
            errorMsg = "Invalid time format! Example: 1905 (19h 05min)";
        }
    }
    if(errorMsg != "") {
        return errorMsg; //false;
    }
    return true;
}

/*
 Su estais sutarД—m, kad laiko ДЇvedimД… nedarom dropdownnais, bet reikia
 leisti ДЇvesti kaip h , hmm, h:mm, h.mm, h mm ir pan. Laikykime, kad
 jei iЕЎ pradЕѕiЕі eina 3-4 skaitmenys, tai interpretuojam kaip hhmm, t.y.
 paskutiniai 2 skaitmenys yra minutД—s. Jei pradЕѕioje eina 1-2
 skaitmenys, tai jie verДЌiami ДЇ valandas, tada  ieЕЎkome skaitmenЕі po
 simboliЕі sekos ir jei randame, verДЌiame juos ДЇ minutes.
 */


function test1() {
    for (var i=0; i<10000;i++) {
        parseTime2("1:12");
    }
}

function test2() {
    for (var i=0; i<10000;i++) {
        parseTime("1:12");
    }
}


function parseTime2(value) {
    var rvalue = {hour:-1, minute:-1};
    if(!value && value!="0") return rvalue;

    if(value.length <= 2) {// 5, 12 ir pan h
        rvalue = {hour:parseInt(value,10), minute:0};
    }
    else if(value.length == 3) { //123 == 1:23, 5:5 == 5:05,
        if(!(isDigit(value.charAt(1)))) { // 5:5
            rvalue = {hour:parseInt(value.charAt(0),10), minute:parseInt(value.charAt(2),10)};
        }
        else { // 123 == 1:23
            rvalue = {hour:parseInt(value.charAt(0),10), minute:parseInt(value.substr(1,2),10)};
        }
    }
    else if(value.length == 4) { // 1:12, 12:5, 1224
        if(!(isDigit(value.charAt(1)))) { // 1:12
            rvalue = {hour:parseInt(value.charAt(0),10), minute:parseInt(value.substr(2,2),10)};
        }
        else if(!(isDigit(value.charAt(2)))) { // 12:5
            rvalue = {hour:parseInt(value.substr(0,2),10), minute:parseInt(value.charAt(3),10)};
        }
        else { // //1224
            rvalue = {hour:parseInt(value.substr(0,2),10), minute:parseInt(value.substr(2,2),10)};
        }
    }
    else { //5 simboliai;	12:25
        rvalue = {hour:parseInt(value.substr(0,2),10), minute:parseInt(value.substr(3,2),10)};
    }

    //return rvalue;
    return (isNaN(rvalue.hour) || isNaN(rvalue.minute)) ? {hour:-1, minute:-1} : rvalue;
}


// Jeighu duodamas kazhkoks sh, tai grazhina 0:0 web ouslapio atveju tai tinka, nes chia buna 24:XX valandos o ne 0:xx
function parseTime(value) {
    var rvalue = {hour:-1, minute:-1};
    if(!value) return rvalue;

    re = /^(\d{1,2})(\d{2})$/;
    value = ("0000" + value).slice(-4);

    //console.log(value);
//    if(value != '') { 
    if(regs = value.match(re)) {
        rvalue["hour"] = parseInt(regs[1],10);
        rvalue["minute"] = parseInt(regs[2],10);
    }
//	}
    //console.log(rvalue);
    return rvalue;
}

function subTime(val1, val2) {
    var hour1 = val1["hour"];
    var hour2 = val2["hour"];
    var minute1 = val1["minute"];
    var minute2 = val2["minute"];

    var my_hour = (hour1 < hour2) ? (24 + hour1 - hour2) : (hour1 - hour2);
    var my_minutes = 0;
    if (minute1 < minute2) {
        my_minutes = 60 + minute1 - minute2;
        var my_hour = ((my_hour - 1) < 0) ? 23 : (my_hour - 1);
    }
    else {
        my_minutes = minute1 - minute2;
    }
    return {hour:my_hour, minute:my_minutes};
}

function diffTime(val1, val2) {
    var diff = subTime(val1, val2);
    if (diff.hour >= 12) {
        var diff = subTime(val2, val1);
    }
    return diff;
}

function compareTime(val1, val2) {
    var hour1 = val1["hour"];
    var hour2 = val2["hour"];
    var minute1 = val1["minute"];
    var minute2 = val2["minute"];
    return (hour1 == hour2) ? (minute1 - minute2) : (hour1 - hour2);
}

function addTime(val1, val2) {
    var hour1 = val1["hour"];
    var hour2 = val2["hour"];
    var minute1 = val1["minute"];
    var minute2 = val2["minute"];

    var my_hour = (hour1 + hour2) % 24;
    //if (my_hour >= 24) { my_hour = my_hour-24; }

    var my_minutes = minute1 + minute2;
    while (my_minutes >= 60) {
        my_minutes = my_minutes-60;
        my_hour += 1;
    }
    my_hour = my_hour % 24;
    //if (my_hour >= 24) { my_hour = my_hour-24; } //check for owerflow again

    return {hour: my_hour, minute: my_minutes};
}

function minutesToTime(minutes) {
    return {hour: Math.floor(minutes/60), minute: minutes % 60};
}
function timeToMinutes(time) {
    //if(!time || !time.hour) {
    //    console.log("TIMe: ", time);
    //}
    return time.hour * 60 + time.minute;;
}

function printInterval(min, max, show_interval) {
    if(min == max) return  [show_interval?(pc.i18n("lngInterval")+" "):"" , min, " ", pc.i18n("lngMin")].join("");
    return [show_interval?(pc.i18n("lngInterval")+" "):"", min , "-", max, " ", pc.i18n("lngMin")].join("");
}

function printTime(time) {
    var hour = time["hour"];// || 24;
    if (hour == -1) return "";
    var minutes = time["minute"];
    return (hour%24) + ":" + ("00" + minutes).slice(-2);
}

function printTimeHM(time) {
    var hour = time["hour"]; // || 24;
    if (hour == -1) return "";
    var minutes = time["minute"];
    return hour + "h&nbsp;" + ("00" + minutes).slice(-2) + "min";
}


function toStringTime(time) {
    var hour = time["hour"]; // || 24;
    var minutes = time["minute"];
    if (hour == -1) return "";
    return hour + ("00" + minutes).slice(-2);
}

function isDigit(n){
    if( (n < "0") || ("9" < n) ){
        return false
    } else {
        return true
    }
}

function readNumber(str) {
    if(isDigit(str.charAt(0))) {
        return parseInt(str, 10);
    }
    else {
        var length = str.length;
        if (length == 1) return -1; // "x" - vienas simbolis duotas
        var start = 1;
        while(!isDigit(str.charAt(start)) && start<length) {
            start += 1;
        }
        if (start == length) return -1; // "xxx" - simboliu eilute

        return parseInt(str.substr(start), 10);
    }
}

function sizeOf(obj) {
    return (function() {var s=0; for(var i in this){s++;} return s;}).call(obj)
}

function sizeOfArrObj(obj) {
    if(!obj) return 0;

    var s=0;
    for(var i in obj) {
        //if (typeof(obj[i]) == "object") {
        s += obj[i].length;
        //}
        //s++;
    }
    return s;

    //return (function() {var s=0; for(var i in this){s++;} return s;}).call(obj)
}


String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); }


function ObjectCopy(obj) {
    var copy = {};

    for(var property in obj) {
        var value = obj[property];
        copy[property] = (typeof(value) == "object") ? ObjectCopy(value) : value;
    }

    return copy;
}

/*
 function ObjectToString(obj) {
 var rvalue = [];
 for(var key in obj) {
 if(typeof(obj[key]) == "function") { continue; }
 rvalue.push(key + ":" + obj[key]);
 }
 return rvalue.join(";")
 }
 */

function ObjectToString(obj) {
    var rvalue = [];
    for(var key in obj) {
        if(typeof(obj[key]) == "function") { continue; }
        rvalue.push("'" + key + "':" + obj[key]);
    }
    return "{" + rvalue.join(",") + "}";
}

// Return new array with duplicate values removed

Array.prototype.unique =
    function(equal_fnc) {
        var a = [];
        var l = this.length;
        for(var i=0; i<l; i++) {
            for(var j=i+1; j<l; j++) {
                // If this[i] is found later in the array
                if (typeof(this[i]) == "object") {
                    if(equal_fnc) {
                        if (equal_fnc(this[i], this[j]))
                            j = ++i;
                    }
                    else if (ObjectToString(this[i]) === ObjectToString(this[j]))
                        j = ++i;
                }
                else {
                    if (this[i] === this[j])
                        j = ++i;
                }
            }
            a.push(this[i]);
        }
        return a;
    };


function split_days(arr) { // arr: ["1234567", "12345"]
    if(arr.length <= 1) return arr;
    var day_list = arr.map(function(x) {return x.split("")}); // suskaidom i dienu masyvus

    // pridedame visu variantu susikirtimus; pvz: "12345", "567" turi prideti "5"
    var length = day_list.length;
    for(var i=0; i<length-1; i++) {
        var item = day_list[i];
        for(var j=i+1; j<length; j++) {
            var next = day_list[j];
            var inter = item.intersection(next); // sankirta reikalinga tik jeigu ji yra mazene uzh trumpesni dienu rinkini
            if(inter.length && inter.length < Math.min(next.length, item.length)) day_list.push(inter);
        }
    }

    var lengthsort = function(x,y) {return y.length-x.length};
    day_list.sort(lengthsort); // isrusiuojame, kad pop() grazintu trumpiausia dienu rinkini

    var rvalue = [];
    while(day_list.length) {
        var smallest = day_list.pop(); // gaunam maziausia dienu rinkini
        if(!smallest.length) continue;
        rvalue.push(smallest); // butina ishskaidyti pagal maziausia

        for(var i=0; i<day_list.length; i++) {
            day_list[i] = day_list[i].complement(smallest); // visus dienu rinkinius ishskaidom pagal maziausia
        }
        day_list.sort(lengthsort);
    }

    rvalue.sort(function(a,b) {return (a[0] - b[0]) || (b.length - a.length)});
    return rvalue.map(function(x) {return x.join("")});
}

/*This will return an array of everything that is in the current array but not in the array you pass in.*/
Array.prototype.complement = function( setB ) {
    var setA = this;

    var setA_seen = {};
    var setB_seen = {};

    for ( var i = 0; i < setB.length; i++ ) {
        setB_seen[ setB[i] ] = true;
    }

    var complement = [];
    for ( var i = 0; i < setA.length; i++ ) {
        if ( !setA_seen[ setA[i] ] ) {
            setA_seen[ setA[i] ] = true;
            if ( !setB_seen[ setA[i] ] ) {
                complement.push( setA[i] );
            }
        }
    }
    return complement;
};


Array.prototype.intersection = function( setB ) {
    var setA = this;

    var setA_seen = {};
    var setB_seen = {};
    for ( var i = 0; i < setB.length; i++ ) {
        setB_seen[ setB[i] ] = true;
    }

    var intersection = [];
    for ( var i = 0; i < setA.length; i++ ) {
        if ( !setA_seen[ setA[i] ] ) {
            setA_seen[ setA[i] ] = true;
            if ( setB_seen[ setA[i] ] ) {
                intersection.push( setA[i] );
            }
        }
    }
    return intersection;
};

/**
 * Method to calculate the size of the content
 * and return back the object which contains the width and the height of the content
 * @param {Object} node - html string or dom element
 * @return {object} width/height of content
 */
var tmpDiv = null;
function calculateContentSize(node) {
    var realWidth, realHeight, previousDisplay;

    if(!tmpDiv) {
        tmpDiv = document.createElement("div");
        tmpDiv.style.visibility = "hidden";
    }

    //dom object
    if(typeof node == "object" && node != null) {
        previousDisplay = node.style.display;
        //must set node to inline
        node.style.display = "inline";
        tmpDiv.appendChild(node);
        //must set node container to inline too
        tmpDiv.style.display = "inline";
        document.body.appendChild(tmpDiv);
        realWidth = tmpDiv.offsetWidth;
        realHeight = tmpDiv.offsetHeight;

        node.style.display = previousDisplay;
        document.body.removeChild(tmpDiv);
    }

    //html string
    else if(typeof node == "string" && node != "") {
        previousDisplay = tmpDiv.style.display;
        tmpDiv.innerHTML = node;
        tmpDiv.style.display = "inline";
        document.body.appendChild(tmpDiv);
        realWidth = tmpDiv.offsetWidth;
        realHeight = tmpDiv.offsetHeight;
        tmpDiv.style.display = previousDisplay;
        document.body.removeChild(tmpDiv);
    }
    else {
        realWidth = 0;
        realHeight = 0;
    }
    return {width: realWidth, height: realHeight};
}


/*
 * Use Haversine formula to Calculate distance (in km) between two points specified by 
 * latitude/longitude (in numeric degrees)
 *
 * example usage from form:
 *   result.value = LatLon.distHaversine(lat1.value.parseDeg(), long1.value.parseDeg(), 
 *                                       lat2.value.parseDeg(), long2.value.parseDeg());
 * where lat1, long1, lat2, long2, and result are form fields
 */

// extend Number object with methods for converting degrees/radians
Number.prototype.toRad = function() {  // convert degrees to radians
    return this * Math.PI / 180;
}

function distHaversine(lat1, lon1, lat2, lon2) {
    var R = 6371; // earth's mean radius in km
    var dLat = (lat2-lat1).toRad();
    var dLon = (lon2-lon1).toRad();
    lat1 = lat1.toRad(), lat2 = lat2.toRad();

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1) * Math.cos(lat2) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d;
}

function distPythagor(lat1, lon1, lat2, lon2) {
    var dLat = 111.2 * (lat2 - lat1);
    var dLon = 58.1 * (lon2 - lon1);
    return Math.sqrt(dLat*dLat + dLon*dLon);
}

function testAlias(stops) {
    if(stops.length == 1) {
        var stop_data = pc.getSI().getStopData(stops[0]);
        //console.log(stops[0], ":", stop_data.neighbours);
        if(stop_data.is_alias) {
            return stop_data.neighbours; //.split(",");
        }
    }
    return stops;
}


/* 
 CSSHttpRequest
 Copyright 2008 nb.io - http://nb.io/
 Licensed under Apache License, Version 2.0 - http://www.apache.org/licenses/LICENSE-2.0.html
 */
//var response = "";

var r = null;
(function(){
    var chr = window.CSSHttpRequest = {};


    chr.id = 0;
    chr.requests = {};
    chr.MATCH_ORDINAL = /#c(\d+)/;
    chr.MATCH_URL = /url\("?data\:[^,]*,([^")]+)"?\)/;

    function callback_wait() {
        window.setTimeout(function () {
            var response = r.document.body.innerHTML;
            //console.log("response: ", response);
            if(!response) {
                // Cia galime isppausdinti progresso informacija!
                callback_wait();
            }
            else {
                r.callback(response);
            }
        }, 10);
    }


    chr.get = function(url, callback) {
        var id = ++chr.id;
        var iframe = document.createElement( "iframe" );
        iframe.style.position = "absolute";
        iframe.style.left = iframe.style.top = "-1000px";
        iframe.style.width = iframe.style.height = 0;
        iframe.src = url; // su CSS reik uzkomentuoti
        document.documentElement.appendChild(iframe);

        //var
        r = chr.requests[id] = {
            id: id,
            iframe: iframe,
            document: iframe.contentDocument || iframe.contentWindow.document,
            callback: callback
        };

        //callback_wait();
        //console.log("response: ", r.document.body.innerHTML);

        /*
         r.document.open("text/html", false);
         r.document.write("<html><head>");
         r.document.write("<link rel='stylesheet' type='text/css' media='print, csshttprequest' href='" + chr.escapeHTML(url) + "' />");
         r.document.write("</head><body>");
         r.document.write("<script type='text/javascript'>");
         r.document.write("(function(){var w = window; var p = w.parent; p.CSSHttpRequest.sandbox(w); w.onload = function(){p.CSSHttpRequest.callback('" + id + "');};})();");
         r.document.write("</script>");
         r.document.write("</body></html>");
         r.document.close();
         */
    };


    chr.sandbox = function(w) {
    };


    chr.callback = function(id) {
        var r = chr.requests[id];
        var data = chr.parse(r);
        r.callback(data);
        window.setTimeout(function() {
            var r = chr.requests[id];
            try { r.iframe.parentElement.removeChild(r.iframe); } catch(e) {};
            delete chr.requests[id];
        }, 0);
    };

    chr.parse = function(r) {
        var data = [];

        // Safari, IE and same-domain Firefox
        try {
            var rules = r.document.styleSheets[0].cssRules || r.document.styleSheets[0].rules;
            for(var i = 0; i < rules.length; i++) {
                try {
                    var r = rules.item ? rules.item(i) : rules[i];
                    var ord = r.selectorText.match(chr.MATCH_ORDINAL)[1];
                    var val = r.style.backgroundImage.match(chr.MATCH_URL)[1];
                    data[ord] = val;
                } catch(e) {}
            }
        }

            // catch same-domain exception
        catch(e) {
            r.document.getElementsByTagName("link")[0].setAttribute("media", "screen");
            var x = r.document.createElement("div");
            x.innerHTML = "foo";
            r.document.body.appendChild(x);
            var ord = 0;
            try {
                while(1) {
                    x.id = "c" + ord;
                    var style = r.document.defaultView.getComputedStyle(x, null);
                    var bg = style["background-image"] || style.backgroundImage || style.getPropertyValue("background-image");
                    var val = bg.match(chr.MATCH_URL)[1];
                    data[ord] = val;
                    ord++;
                }
            } catch(e) {}
        }

        return decodeURIComponent(data.join(""));
    };


    chr.escapeHTML = function(s) {
        return s.replace(/([<>&""''])/g,
            function(m, c) {
                switch(c) {
                    case "<": return "&lt;";
                    case ">": return "&gt;";
                    case "&": return "&amp;";
                    case '"': return "&quot;";
                    case "'": return "&apos;";
                }
                return c;
            });
    };
})();

function setCookie(c_name,value,expiredays) {
    var exdate=new Date() + 14;
    exdate.setDate(exdate.getDate()+expiredays);
    document.cookie=c_name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
}

function getCookie(c_name) {
    if (document.cookie.length>0) {
        c_start=document.cookie.indexOf(c_name + "=");
        if (c_start!=-1) {
            c_start=c_start + c_name.length+1;
            c_end=document.cookie.indexOf(";",c_start);
            if (c_end==-1) c_end=document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

// this deletes the cookie when called
function deleteCookie(c_name) {
    if (getCookie(c_name)) document.cookie = c_name + "='';expires=Thu, 01-Jan-1970 00:00:01 GMT";
}

function togglebold(obj, id) {
    if(obj.className != "togglebold") {
        obj.className = "togglebold";
        filter([{filter:commercial_filter, data:{cell_index:1}}], id);
    }
    else {
        obj.className = "tabcommand";
        showall(id);
    }
}

//var content_id =  pc._current_page_name + "_tagCon" + i;
//id=\"tab", i, "_", pc._current_page_name, "_middle\"
function small_tab_schedule(content_id, commercial) {
    return ["<span class=\"tab_grey\">",
        commercial ? ["<span class=\"tab_grey_ico_bg\">A</span><span style=\"cursor:pointer\" onclick=\"togglebold(this, '", content_id, "')\">", pc.i18n("lngOnlyPublic"),"</span>&nbsp;&nbsp;&nbsp;"].join(""):"",
        "<span class=\"tabcommand\" style=\"cursor:pointer\" onclick=\"window.print()\">", pc.i18n("lngPrint"),"</span></span>"].join("");
}

/*
 var optimal_tab = document.getElementById("tab0_middle");
 optimal_tab.innerHTML = ["<span class=\"tab_grey\"><span class=\"tabcommand\" onclick=\"sortedoptions_print('Depart')\" style=\"cursor:pointer\">", pc.i18n("lngOrderByDeparture"), "</span>&nbsp;&nbsp;&nbsp;<span class=\"tabcommand\" onclick=\"sortedoptions_print('Trip')\" style=\"cursor:pointer\">", pc.i18n("lngOrderByDuration"), "</span>&nbsp;&nbsp;&nbsp;<span class=\"tab_grey_ico_bg\"> A </span><span id=\"optimal_routes_commercial\" class=\"tabcommand\" style=\"cursor:pointer\" onclick=\"togglebold(this, 'tagCon0')\">", pc.i18n("lngOnlyPublic"),"</span>&nbsp;&nbsp;&nbsp;<span class=\"tabcommand\" style=\"cursor:pointer\" onclick=\"window.print()\">", pc.i18n("lngPrint"),"</span></span>"].join("");

 var direct_tab = document.getElementById("tab1_middle");
 direct_tab.innerHTML = ["<span class=\"tab_grey\"><span class=\"tabcommand\" onclick=\"sorteddirect_print('Depart')\" style=\"cursor:pointer\">", pc.i18n("lngOrderByDeparture"), "</span>&nbsp;&nbsp;&nbsp;<span class=\"tabcommand\" onclick=\"sorteddirect_print('Trip')\" style=\"cursor:pointer\">", pc.i18n("lngOrderByDuration"), "</span>&nbsp;&nbsp;&nbsp;<span class=\"tab_grey_ico_bg\"> A </span><span id=\"direct_routes_commercial\" class=\"tabcommand\" style=\"cursor:pointer\" onclick=\"togglebold(this, 'tagCon1')\">", pc.i18n("lngOnlyPublic"),"</span>&nbsp;&nbsp;&nbsp;<span class=\"tabcommand\" style=\"cursor:pointer\" onclick=\"window.print()\">", pc.i18n("lngPrint"),"</span></span>"].join("");
 */

function array_equal(arr1, arr2) {
    if(arr1.length != arr2.length) return false;
    else {
        for(var i=0;i<arr1.length; i++) {
            if(arr1[i] != arr2[i]) return false;
        }
    }
    return true;
}

function dayToDate(day) {
    var d = new Date();
    d.setTime(day * 24 * 60 * 60 * 1000);
    return d;
}

function toDay() {
    var today = new Date();
    return Math.floor(today.getTime() / (1000 * 60 * 60 * 24));
}


function format_streetarea(stop_data) {
    var start_area_street = [];
    if (stop_data.area != "0") start_area_street.push(stop_data.area);
    if (stop_data.street != "0") start_area_street.push(stop_data.street);

    //return (start_area_street.length ? " ("+start_area_street.join(", ")+")" : "")
    return (start_area_street.length ? (" <span style='font-size:10px;white-space:nowrap;'>(" + start_area_street.join(", ") + ")</span>"):"")
}
