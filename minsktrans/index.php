<?php
error_reporting(-1) ; // включить все виды ошибок, включая  E_STRICT
ini_set('display_errors', 'On');  // вывести на экран помимо логов
class MinskTrans {
    private static $token = null;
    private static $appCookie = 'nqT3KBHze3Jd9u8f9cjC-mtz0CCcY5NqsCKNBkWmwT_wav6hCnA0Y2EsSXgixeSNxcJlVxgnZ8LNTrW5v2kuo2xdol9qEsMDGzZgMLFWJBYn_35B7GyUydLHsht-sQ7qEqwYb8yl-0TJNm1EfwSWL-8BbBI5dAAmy6WL4uUR-28bdkZglozzbb2UBV339C-0QuKqJX8JJeHpsboByLdSsjTLjNf0L5RtaAjZU6Z2L9Tw9OfEN3oeY-xjAVDoQ3617J1XptZ0toMjWDsEIdbueF5B8QAl5S1i5t7sauxioneaZHFZC8OYOODihXrfvDocH5JzqjdgSBViaYoKDVqTk9yjMTwBLDx0ZxXyCkFDE1rwzwn3Pf0gw3Uu6NS8PLmZZKrMlVbOG3UtOYdJKKtzELcdBy4ylfuhjAm2CcuSH8e16GGMeyoaL1Y2FzIyGUGqLC2cQPJxRbfUKgKpj9f1Lr53O5ApoEXEnoMXLx5pBA39Tfixm_CralvVmGp0O3Rw';

    private static function getToken() {
        if (!empty(self::$token)) return self::$token;

        $page = self::curl('http://www.minsktrans.by/lookout_yard/Home/Index/minsk', [
            CURLOPT_COOKIESESSION => true
        ]);
        preg_match('#<input.*name="__RequestVerificationToken".*value=\"(.*?)\"#ui', $page, $matches);
        self::$token = $matches[1];
        return self::$token;
    }

    private static function curl($url, $options = []) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt' );
        curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt' );

        curl_setopt_array($ch, $options);
        return curl_exec($ch);
    }

    private static function request($url, $data) {
        $defaultData = ['__RequestVerificationToken' => self::getToken()];
        return self::curl($url, [
            CURLOPT_HTTPHEADER => [
                "X-Requested-With: XMLHttpRequest",
                "Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
                'User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:45.0) Gecko/20100101 Firefox/45.0',
                'Referer: http://www.minsktrans.by/lookout_yard/',
                'Host: www.minsktrans.by'
            ],
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query(array_merge($defaultData, $data)),

        ]);
    }

    public static function track() {
        $_REQUEST['tt'] = str_ireplace('-', '', $_REQUEST['tt']);
        $out = [];
        $vehicles = self::request('http://www.minsktrans.by/lookout_yard/Data/Vehicles', [
            'v' => $_REQUEST['v'],
            'tt' => $_REQUEST['tt'],
            'r' => $_REQUEST['r'],
            'p' => 'minsk'
        ]);
        $stopsTemp = json_decode(self::request('http://www.minsktrans.by/lookout_yard/Data/Route', [
            'tt' => $_REQUEST['tt'],
            'r' => $_REQUEST['r']
        ]), true)['Trips'];
        $stops = [
            array_shift($stopsTemp['StopsA'])['Id'] => $stopsTemp['EndStopB'],
            array_shift($stopsTemp['StopsB'])['Id'] => $stopsTemp['EndStopA']
        ];

        $vehiclesArray = json_decode($vehicles, true);
        foreach ($vehiclesArray['Vehicles'] as $index => &$v) {
            if (isset($stops[$v['IdEndStop']]))
                $v['EndStop'] = $stops[$v['IdEndStop']];
            else
                unset($vehiclesArray['Vehicles'][$index]);
        }
        return json_encode($vehiclesArray);
    }

    public static function routeSearch() {
        $out = [];
        $result = self::request('http://www.minsktrans.by/lookout_yard/Data/RouteSearch/minsk', [
            'lat1' => $_REQUEST['from']['lat'],
            'lon1' => $_REQUEST['from']['lng'],
            'lat2' => $_REQUEST['to']['lat'],
            'lon2' => $_REQUEST['to']['lng']
        ]);

        return $result;
    }
}
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
echo (isset($_GET['track']) ? MinskTrans::track() : MinskTrans::routeSearch());